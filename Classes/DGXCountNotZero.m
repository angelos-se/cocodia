//
//  DGXCountNotZero.m
//  CocoTrain
//
//  Created by KITAHARA Motohiko on 06-11-11.
//  Copyright 2006 horazaka.net. All rights reserved.
//

#import "DGXCountNotZero.h"


@implementation DGXCountNotZero
+ (Class)transformedValueClass
{
	return [ NSString class ];
}

+ (BOOL)allowsReverseTransformation
{
	return NO;
}

- (id)transformedValue:(id)beforeObject
{
//	NSLog( @"[ beforeObject count ] = %d", [ beforeObject count ]);
	return [ NSNumber numberWithBool:( 0 < [ beforeObject count ])];
}

@end
