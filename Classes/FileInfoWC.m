//
//  FileInfoWC.m
//  CocoDia
//
//  Created by 北原 基彦 on 08-08-30.
//  Copyright 2008 horazaka.net. All rights reserved.
//

#import "FileInfoWC.h"


NSString*	DGXMainWindowDidChangeNotification = @"DGXMainWindowDidChange";
NSString*	DGXDocumentWindowWillCloseNotification = @"DGXDocumentWindowWillClose";
static CGFloat	gRightMergin = 40.0;
static CGFloat	gUpperMergin = 40.0;

@implementation FileInfoWC
// =============================================================================
//	initWithWindowNibName:
// =============================================================================
- (id)initWithWindowNibName:(NSString *)windowNibName
{
	self = [super initWithWindowNibName:windowNibName];
	if (self != nil) {
		NSRect	theVisibleFrame = [[[NSScreen screens] objectAtIndex:0] visibleFrame];
//		NSLog(@"theVisibleFrame=%@",NSStringFromRect(theVisibleFrame));
		CGFloat x, y;
		x = NSMaxX(theVisibleFrame) - NSWidth([[self window] frame]) - gRightMergin;
		y = NSMaxY(theVisibleFrame) - NSHeight([[self window] frame]) - gUpperMergin;
//		NSLog(@"x=%f,y=%f",x,y); 
		[[self window] setFrameOrigin:NSMakePoint(x, y)];

		_fileInfo = [NSEntityDescription insertNewObjectForEntityForName:@"FileInfo" inManagedObjectContext:[self managedObjectContext]];
	}
	return self;
}
// =============================================================================
//	dealloc
// =============================================================================
- (void) dealloc
{
	[_fileInfo release], _fileInfo = nil;
	[_managedObjectContext release], _managedObjectContext = nil;
	[_persistentStoreCoodinator release], _persistentStoreCoodinator = nil;
	[_managedObjectModel release], _managedObjectModel = nil;

	[super dealloc];
}
#pragma mark -
// =============================================================================
//	managedObjectModel
// =============================================================================
- (NSManagedObjectModel*)managedObjectModel
{
	if(_managedObjectModel)
		return _managedObjectModel;

	NSMutableArray*	theBundles = [NSMutableArray array];
	[theBundles addObject:[NSBundle mainBundle]];
	[theBundles addObjectsFromArray:[NSBundle allFrameworks]];
	_managedObjectModel = [[NSManagedObjectModel mergedModelFromBundles:[NSArray arrayWithArray:theBundles]] retain];
	return _managedObjectModel;
}
// =============================================================================
//	persistentStoreCoodinator
// =============================================================================
- (NSPersistentStoreCoordinator*)persistentStoreCoodinator
{
	if(_persistentStoreCoodinator)
		return _persistentStoreCoodinator;

	_persistentStoreCoodinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
	NSError*	theError = nil;
	id	theObject = [_persistentStoreCoodinator addPersistentStoreWithType:NSInMemoryStoreType 
		configuration:nil 
		URL:nil 
		options:nil 
		error:&theError];
	if(!theObject)
		[[NSApplication sharedApplication] presentError:theError];
	return _persistentStoreCoodinator;
}
// =============================================================================
//	managedObjectContext
// =============================================================================
- (NSManagedObjectContext*)managedObjectContext
{
	if(_managedObjectContext)
		return _managedObjectContext;

	NSPersistentStoreCoordinator*	theCoordinator = [self persistentStoreCoodinator];
	if(theCoordinator)
	{
		_managedObjectContext = [[NSManagedObjectContext alloc] init];
		[_managedObjectContext setPersistentStoreCoordinator:theCoordinator];
	}
	return _managedObjectContext;
}
#pragma mark -
// =============================================================================
//	validDocumentWindows
//		cut out windows without title( they may be rubbish window ) and NSPanels
// =============================================================================
- (NSArray*) validDocumentWindows
{
//	for( id theWindow in [NSApp windows])
//	{
//		NSLog(@"class=%@,title=%@,isKey(Main)=%ld(%ld)",[theWindow class],[theWindow title],[theWindow isKeyWindow],[theWindow isMainWindow]);
//	}
	NSPredicate*	thePredicate = [ NSPredicate predicateWithFormat:@"title != '' && class != %@",[ NSPanel class ]];
	return [[ NSApp windows ] filteredArrayUsingPredicate:thePredicate ];
}
#pragma mark -
// =============================================================================
//	mainWindowDidChange:
// =============================================================================
- (void) mainWindowDidChange:(NSNotification*)aNotification
{
//	NSLog(@"aNotification=%@",aNotification);
	if(![[aNotification name] isEqualTo:DGXMainWindowDidChangeNotification])
		return;
	id	theDocument = [aNotification object];
	if(![theDocument isKindOfClass:[NSDocument class]])	// object != subclass of NSDocument -> return
		return;
	id	theProperties = [theDocument valueForKey:@"_properties"];
//	NSLog(@"theProperties=%@",[theProperties valueForKey:@"info"]);
	[_fileInfo setValue:[theDocument displayName] forKey:@"displayName"];
	if(theProperties)
	{
		[_fileInfo setValuesForKeysWithDictionary:[theProperties valueForKey:@"info"]];
		NSString*	thePath = [[theDocument fileURL] path];
		NSDictionary*	theAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:thePath error:nil];
		[_fileInfo setValue:thePath forKey:@"filePath"];
		[_fileInfo setValue:[theAttributes valueForKey:@"NSFileCreationDate"] forKey:@"creationDate"];
		[_fileInfo setValue:[theAttributes valueForKey:@"NSFileModificationDate"] forKey:@"modificationDate"];
	}
	else
	{
		[_fileInfo setValue:@"" forKey:@"line"];
		[_fileInfo setValue:nil forKey:@"date"];
		[_fileInfo setValue:@"" forKey:@"creator"];
		[_fileInfo setValue:@"" forKey:@"comment"];
		[_fileInfo setValue:@"" forKey:@"filePath"];
		[_fileInfo setValue:nil forKey:@"creationDate"];
		[_fileInfo setValue:nil forKey:@"modificationDate"];
	}
}
// =============================================================================
//	documentWindowWillClose:
// =============================================================================
- (void) documentWindowWillClose:(NSNotification*)aNotification
{
//	NSLog(@"aNotification=%@",aNotification);
	if(![[ aNotification name ] isEqualTo:DGXDocumentWindowWillCloseNotification ])
		return;
	id	theDocument = [ aNotification object ];
	if(![ theDocument isKindOfClass:[ NSDocument class ]])	// object != subclass of NSDocument -> return
		return;
//	NSLog(@"widows=%@",[NSApp windows]);
	NSArray*	theValidWidows = [ self validDocumentWindows ];
//	NSLog(@"theValidWidows=%@",theValidWidows);
	if( 1 == [ theValidWidows count ])	// if there is only one Document
		[[ self window ] orderOut:self ];
}

@end
