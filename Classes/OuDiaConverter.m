//
//  OuDiaConverter.m
//  CoreDia
//
//  Created by KITAHARA Motohiko on 07-12-09.
//  Copyright 2007-10 horazaka.net. All rights reserved.
//

#import "OuDiaConverter.h"

#import	"DGXKeys.h"
#import "DGXUtilities.h"
#import "NSColor_Category.h"


@implementation OuDiaConverter
// =============================================================================
//	init
// =============================================================================
- (id)init
{
	self = [super init];
	if( self )
	{
		NSString*	thePath = [[NSBundle mainBundle] pathForResource:@"OuDiaConverter" ofType:@"plist"];
		_converterInfo = [[NSDictionary dictionaryWithContentsOfFile:thePath] retain];
	}
	return self;
}
// =============================================================================
//	dealloc:
// =============================================================================
- (void) dealloc
{
	[_stack release], _stack = nil;
	[_ouDiaRoot release], _ouDiaRoot = nil;

	[_rosen release], _rosen = nil;
	[_dispProp release], _dispProp = nil;
	[_eki release], _eki = nil;
	[_ressyasyubetsu release], _ressyasyubetsu = nil;
	[_dia release], _dia = nil;
	[_kudari release], _kudari = nil;
	[_nobori release], _nobori = nil;
	[_ressya release], _ressya = nil;

	[_converterInfo release], _converterInfo = nil;

	[super dealloc];
}
#pragma mark -
// -----------------------------------------------------------------------------
//	createInfo:fromOuDia:auxiliary:
// -----------------------------------------------------------------------------
- (void)createInfo:(NSMutableDictionary*)newObject fromOuDia:(NSDictionary*)object index:(NSInteger)index
{
	id	theValue;
	// line
	theValue = [NSString stringWithFormat:@"%@ (%@)",[object valueForKey:@"Rosenmei"], [[[object valueForKey:@"Dia"] objectAtIndex:index] valueForKey:@"DiaName"]];
	[newObject setValue:(( theValue ) ? theValue : @"" ) forKey:(NSString*)DGXLine];
	// comment
	theValue = [object valueForKey:@"Comment"];
	[newObject setValue:(( theValue ) ? theValue : @"" ) forKey:(NSString*)DGXComment];
	// creator
	NSString*	theFormat = NSLocalizedString( @"converted", @"Converted by %@ ver %@" );
	theValue = [NSString stringWithFormat:theFormat, 
			[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleExecutable"], 
			[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
	[newObject setValue:theValue forKey:(NSString*)DGXCreator];
	// date
	[newObject setValue:[NSDate date] forKey:(NSString*)DGXDate];
}
// -----------------------------------------------------------------------------
//	createStation:fromOuDia
// -----------------------------------------------------------------------------
- (void)createStation:(NSMutableArray*)newObjects fromOuDia:(NSArray*)objects
{
	NSArray*	theInfo = [_converterInfo valueForKey:@"DGXOuDiaStateValues"];
	for( id theObject in objects )
	{
		NSMutableDictionary*	theNewObject = [NSMutableDictionary dictionary];
		// name
		[theNewObject setValue:[theObject valueForKey:@"Ekimei"] forKey:(NSString*)DGXName];
		// state
		NSInteger	i = [theInfo indexOfObject:[theObject valueForKey:@"Ekikibo"]];
		[theNewObject setValue:[NSNumber numberWithInteger:(( NSNotFound != i ) ? i : 1 )] forKey:(NSString*)DGXState];
		// distance
		[theNewObject setValue:[NSNumber numberWithFloat:0.0] forKey:(NSString*)DGXDistance];
		// addObject
		[newObjects addObject:theNewObject];
	}
}
// -----------------------------------------------------------------------------
//	createClassification:fromOuDia
// -----------------------------------------------------------------------------
- (void)createClassification:(NSMutableArray*)newObjects fromOuDia:(NSArray*)objects
{
	NSArray*	theInfo = [_converterInfo valueForKey:@"DGXOuDiaLineStyleValues"];
	for( id theObject in objects )
	{
		NSMutableDictionary*	theNewObject = [NSMutableDictionary dictionary];
		id	theValue;
		// longName
		theValue = [theObject valueForKey:@"Syubetsumei"];
		[theNewObject setValue:(( theValue ) ? theValue : @"" ) forKey:(NSString*)DGXLongName];
		// shortName
		theValue = [theObject valueForKey:@"Ryakusyou"];
		[theNewObject setValue:(( theValue ) ? theValue : @"" ) forKey:(NSString*)DGXShortName];
		// lineColor
		unsigned	i;
		NSScanner*	theScanner = [NSScanner scannerWithString:[theObject valueForKey:@"DiagramSenColor"]];
		[theScanner scanHexInt:&i];
		CGFloat	theAlpha = 1 - (( i >> 24 ) & 0xFF ) / 255.0;
		CGFloat	theBlue = (( i >> 16 ) & 0xFF ) / 255.0;
		CGFloat	theGreen = (( i >> 8 ) & 0xFF ) / 255.0;
		CGFloat	theRed = ( i & 0xFF ) / 255.0;
//		NSLog(@"theAlpha=%f,theRed=%f,theGreen=%f,theBlue=%f",theAlpha, theRed, theGreen, theBlue);
		NSColor*	theColor = [NSColor colorWithCalibratedRed:theRed green:theGreen blue:theBlue alpha:theAlpha];
		[theNewObject setValue:[NSColor stringFromColor:theColor] forKey:(NSString*)DGXLineColor];
		// lineStyle
		NSInteger	j = [theInfo indexOfObject:[theObject valueForKey:@"DiagramSenStyle"]];
		[theNewObject setValue:[NSNumber numberWithInteger:(( NSNotFound != j ) ? j : 0 )] forKey:(NSString*)DGXLineStyle];
		// lineThickness
		[theNewObject setValue:[NSNumber numberWithBool:(([theObject valueForKey:@"DiagramSenIsBold"]) ? YES : NO )] forKey:(NSString*)DGXLineThickness];
		// addObject
		[newObjects addObject:theNewObject];
	}
}
// -----------------------------------------------------------------------------
//	createTrain:fromOuDia:withStationCount:direction
// -----------------------------------------------------------------------------
- (void)createTrain:(NSMutableArray*)newObjects fromOuDia:(NSArray*)objects withStationCount:(NSInteger)stationCount direction:(BOOL)direction
{
	for( id theObject in objects )
	{
		NSMutableDictionary*	theNewObject = [NSMutableDictionary dictionary];
		id	theValue;
		// trainID
		theValue = [theObject valueForKey:@"Ressyabangou"];
		[theNewObject setValue:(( theValue ) ? theValue : @"" ) forKey:(NSString*)DGXTrainID];
		// visible
		[theNewObject setValue:[NSNumber numberWithBool:YES] forKey:(NSString*)DGXVisible];
		// classificationID
		theValue = [theObject valueForKey:@"Syubetsu"];
		[theNewObject setValue:(( theValue ) ? theValue : @"" ) forKey:(NSString*)DGXClassificationID];
		// nickname
		theValue = [theObject valueForKey:@"Ressyamei"];
		[theNewObject setValue:(( theValue ) ? theValue : @"" ) forKey:(NSString*)DGXNickname];
		// number
		theValue = [theObject valueForKey:@"Gousuu"];
		NSInteger	theIntegerValue = ( theValue ) ? [theValue integerValue] : 0;
		[theNewObject setValue:[NSNumber numberWithInteger:theIntegerValue] forKey:(NSString*)DGXNumber];
		// note
		theValue = [theObject valueForKey:@"Bikou"];
		[theNewObject setValue:(( theValue ) ? theValue : @"" ) forKey:(NSString*)DGXNote];
		// timetable
		NSMutableArray*	theTimetable = [NSMutableArray arrayWithArray:[[theObject valueForKey:@"EkiJikoku"] componentsSeparatedByString:@","]];
		NSUInteger i, j = [theTimetable count];
		for( i = 0; i < j; i++ )
		{
			NSArray*	theToken = [[theTimetable objectAtIndex:i] componentsSeparatedByString:@";"];
			if( 2 == [theToken count])
			{
				if( 1 == [[theToken objectAtIndex:0] integerValue])
					[theTimetable replaceObjectAtIndex:i withObject:[theToken lastObject]];
				else
				{
					NSArray*	theTime = [[theToken lastObject] componentsSeparatedByString:@"/"];
					NSString*	theArrival = nil;
					NSString*	theNewArrival = nil;
					NSString*	theDeparture = [theTime lastObject];
					NSString*	theNewDeparture = (0 == [theDeparture length]) ? theDeparture : [theDeparture stringByAppendingString:@"?"];
					if (2 == [theTime count])
					{
						theArrival = [theTime objectAtIndex:0];
						theNewArrival = (0 == [theArrival length]) ? theArrival : [theArrival stringByAppendingString:@"?"];
						[theTimetable replaceObjectAtIndex:i withObject:[NSString stringWithFormat:@"%@/%@", theNewArrival, theNewDeparture]];
					}
					else
						[theTimetable replaceObjectAtIndex:i withObject:theNewDeparture];
				}
			}
			else
			{
				if( 2 <= [[theToken lastObject] integerValue])	// 2="-", 3="|"
					[theTimetable replaceObjectAtIndex:i withObject:@"-"];
				else
					;	// do nothing
			}
		}
		for( i = j; i < stationCount; i++ )
			[theTimetable addObject:@""];
		if( !direction )	// upTrain -> reverse timetable
			for( i = 0; i < stationCount / 2; i++ )
				[theTimetable exchangeObjectAtIndex:i withObjectAtIndex:stationCount - i - 1];
		[theNewObject setValue:theTimetable forKey:(NSString*)DGXTimetable];
		[newObjects addObject:theNewObject];
	}
}
#pragma mark -
// -----------------------------------------------------------------------------
//	pop
// -----------------------------------------------------------------------------
- (SEL)pop
{
	[_stack removeLastObject];
	return NSSelectorFromString([NSString stringWithFormat:@"create%@:",[_stack lastObject]]);
}
// -----------------------------------------------------------------------------
//	push:
// -----------------------------------------------------------------------------
- (SEL)push:(NSInteger)anIndex
{
	NSString*	theObjectName = [[_converterInfo valueForKeyPath:@"DGXOuDiaLabels.Name"] objectAtIndex:anIndex];
	[_stack addObject:theObjectName];
	return NSSelectorFromString([NSString stringWithFormat:@"create%@:", theObjectName]);
}
// -----------------------------------------------------------------------------
//	setValueFromLine:toObject:
// -----------------------------------------------------------------------------
- (void)setValueFromLine:(id)aLine toObject:(id)anObject
{
	if([aLine rangeOfString:@"="].location != NSNotFound )
	{
		NSArray*	theWords = [aLine componentsSeparatedByString:@"="];
		[anObject setValue:[theWords lastObject] forKey:[theWords objectAtIndex:0]];
	}
}
// -----------------------------------------------------------------------------
//	createRessya:
// -----------------------------------------------------------------------------
- (SEL)createRessya:(NSString*)aLine
{
	if([aLine isEqualTo:@"."])
	{
		NSString*	theKey = [_stack lastObject];
		BOOL	theDirection = ( NSNotFound != [_stack indexOfObject:@"Kudari"]);	// YES=Kudari NO=Nobori
		id	theTarget = theDirection ? _kudari : _nobori;
		NSMutableArray*	theObjects = [NSMutableArray arrayWithArray:[theTarget valueForKey:theKey]];
		[theObjects addObject:_ressya];
		[theTarget setValue:theObjects forKey:theKey];
		[_ressya release], _ressya = [[NSMutableDictionary dictionary] retain];
		return [self pop];
	}
//	if( goDeep ){}
	[self setValueFromLine:aLine toObject:_ressya];
	return @selector( createRessya: );
}
// -----------------------------------------------------------------------------
//	createNobori:
// -----------------------------------------------------------------------------
- (SEL)createNobori:(NSString*)aLine
{
	if([aLine isEqualTo:@"."])
	{
		NSString*	theKey = [_stack lastObject];
		[_dia setValue:_nobori forKey:theKey];
		[_nobori release], _nobori = [[NSMutableDictionary dictionary] retain];
		return [self pop];
	}
	NSInteger	i;
	if( 3 != _version )
		i = [aLine isEqualTo:@"Ressya."] ? 7 : NSNotFound;
	else
		i = ([aLine hasPrefix:@"RessyaCont["] && [aLine hasSuffix:@"]."]) ? 7 : NSNotFound;
	if( NSNotFound != i )	// -> "Ressya."
	{
		return [self push:i];
	}
//	[self setValueFromLine:aLine toObject:_nobori];
	return @selector( createNobori: );
}
// -----------------------------------------------------------------------------
//	createKudari:
// -----------------------------------------------------------------------------
- (SEL)createKudari:(NSString*)aLine
{
	if([aLine isEqualTo:@"."])
	{
		NSString*	theKey = [_stack lastObject];
		[_dia setValue:_kudari forKey:theKey];
		[_kudari release], _kudari = [[NSMutableDictionary dictionary] retain];
		return [self pop];
	}
	NSInteger	i;
	if( 3 != _version )
		i = [aLine isEqualTo:@"Ressya."] ? 7 : NSNotFound;
	else
		i = ([aLine hasPrefix:@"RessyaCont["] && [aLine hasSuffix:@"]."]) ? 7 : NSNotFound;
	if( NSNotFound != i )	// -> "Ressya."
	{
		return [self push:i];
	}
//	[self setValueFromLine:aLine toObject:_kudari];
	return @selector( createKudari: );
}
// -----------------------------------------------------------------------------
//	createEki:
// -----------------------------------------------------------------------------
- (SEL)createEki:(NSString*)aLine
{
	if([aLine isEqualTo:@"."])
	{
		NSString*	theKey = [_stack lastObject];
		NSMutableArray*	theObjects = [NSMutableArray arrayWithArray:[_rosen valueForKey:theKey]];
		[theObjects addObject:_eki];
		[_rosen setValue:theObjects forKey:theKey];
		[_eki release], _eki = [[NSMutableDictionary dictionary] retain];
		return [self pop];
	}
//	if( goDeep ){}
	[self setValueFromLine:aLine toObject:_eki];
	return @selector( createEki: );
}
// -----------------------------------------------------------------------------
//	createRessyasyubetsu:
// -----------------------------------------------------------------------------
- (SEL)createRessyasyubetsu:(NSString*)aLine
{
	if([aLine isEqualTo:@"."])
	{
		NSString*	theKey = [_stack lastObject];
		NSMutableArray*	theObjects = [NSMutableArray arrayWithArray:[_rosen valueForKey:theKey]];
		[theObjects addObject:_ressyasyubetsu];
		[_rosen setValue:theObjects forKey:theKey];
		[_ressyasyubetsu release], _ressyasyubetsu = [[NSMutableDictionary dictionary] retain];
		return [self pop];
	}
//	if( goDeep ){}
	[self setValueFromLine:aLine toObject:_ressyasyubetsu];
	return @selector( createRessyasyubetsu: );
}
// -----------------------------------------------------------------------------
//	createDia:
// -----------------------------------------------------------------------------
- (SEL)createDia:(NSString*)aLine
{
	if([aLine isEqualTo:@"."])
	{
		NSString*	theKey = [_stack lastObject];
		NSMutableArray*	theObjects = [NSMutableArray arrayWithArray:[_rosen valueForKey:theKey]];
		[theObjects addObject:_dia];
		[_rosen setValue:theObjects forKey:theKey];
		[_dia release], _dia = [[NSMutableDictionary dictionary] retain];
		return [self pop];
	}
	NSInteger	i;
	if( 3 != _version )
	{
		NSArray*	theLabels = [_converterInfo valueForKeyPath:@"DGXOuDiaLabels.Ver5"];
		i = [[theLabels subarrayWithRange:NSMakeRange( 5, 2 )] indexOfObject:aLine];
		if( NSNotFound != i )
			i += 5;
	}
	else
	{
		if( ![aLine hasSuffix:@"]."])
			i = NSNotFound;
		else if([aLine hasPrefix:@"Ressya[Kudari"])
			i = 5;
		else if([aLine hasPrefix:@"Ressya[Nobori"])
			i = 6;
		else
			i = NSNotFound;
	}
	if( NSNotFound != i )
	{
		return [self push:i];
	}
	[self setValueFromLine:aLine toObject:_dia];
	return @selector( createDia: );
}
// -----------------------------------------------------------------------------
//	createDispProp:
// -----------------------------------------------------------------------------
- (SEL)createDispProp:(NSString*)aLine
{
	if([aLine isEqualTo:@"."])
	{
		NSString*	theKey = [_stack lastObject];
		[_ouDiaRoot setObject:_dispProp forKey:theKey];
		[_dispProp release], _dispProp = [[NSMutableDictionary dictionary] retain];
		return [self pop];
	}
//	if( goDeep ){}
	[self setValueFromLine:aLine toObject:_dispProp];
	return @selector( createDispProp: );
}
// -----------------------------------------------------------------------------
//	createRosen:
// -----------------------------------------------------------------------------
- (SEL)createRosen:(NSString*)aLine
{
	if([aLine isEqualTo:@"."])
	{
		NSString*	theKey = [_stack lastObject];
		[_ouDiaRoot setValue:_rosen forKey:theKey];
		[_rosen release], _rosen = [[NSMutableDictionary dictionary] retain];
		return [self pop];
	}
	NSInteger	i;
	if( 3 != _version )
	{
		NSArray*	theLabels = [_converterInfo valueForKeyPath:@"DGXOuDiaLabels.Ver5"];
		i = [[theLabels subarrayWithRange:NSMakeRange( 2, 3 )] indexOfObject:aLine];
		if( NSNotFound != i )
			i += 2;
	}
	else
	{
		if( ![aLine hasSuffix:@"]."])
			i = NSNotFound;
		else if([aLine hasPrefix:@"Eki["])
			i = 2;
		else if([aLine hasPrefix:@"Ressyasyubetsu["])
			i = 3;
		else if([aLine hasPrefix:@"Dia["])
			i = 4;
		else
			i = NSNotFound;
	}
	if( NSNotFound != i )	// line -> "Eki.","Ressyasyubetsu.","Dia."
	{
		return [self push:i];
	}
	[self setValueFromLine:aLine toObject:_rosen];
	return @selector( createRosen: );
}
// -----------------------------------------------------------------------------
//	createRoot:
// -----------------------------------------------------------------------------
- (SEL)createRoot:(NSString*)aLine
{
	if([aLine isEqualTo:@"."])
	{
		// not reached
	}
	if([aLine isEqualTo:@"Rosen."] || [aLine isEqualTo:@"DispProp."])	// ->"Rosen.","DispProp."
	{
		NSInteger	i = [aLine isEqualTo:@"Rosen."] ? 0 : 1;
		return [self push:i];
	}
	[self setValueFromLine:aLine toObject:_ouDiaRoot];
	return @selector( createRoot: );
}
#pragma mark -
// -----------------------------------------------------------------------------
//	judgeFileVersion:
// -----------------------------------------------------------------------------
- (NSInteger)judgeFileVersion:(NSString*)aLine
{
	NSInteger	theResult = 0;
	NSArray*	theComponents = [aLine componentsSeparatedByString:@"="];
	if(![[theComponents objectAtIndex:0] isEqualTo:@"FileType"])
		return theResult;
	theResult = [[[[theComponents lastObject] componentsSeparatedByString:@"."] lastObject] integerValue];
//	NSLog(@"theResult=%ld",theResult);
	return theResult;
}
// -----------------------------------------------------------------------------
//	parse:
//		return array for DiaName if parse is successful, or nil
// -----------------------------------------------------------------------------
- (NSArray*)parse:(NSArray*)lines
{
	[_stack release], _stack = [[NSMutableArray array] retain];
	[_ouDiaRoot release], _ouDiaRoot = [[NSMutableDictionary dictionary] retain];

	[_rosen release], _rosen = [[NSMutableDictionary dictionary] retain];
	[_dispProp release], _dispProp = [[NSMutableDictionary dictionary] retain];
	[_eki release], _eki = [[NSMutableDictionary dictionary] retain];
	[_ressyasyubetsu release], _ressyasyubetsu = [[NSMutableDictionary dictionary] retain];
	[_dia release], _dia = [[NSMutableDictionary dictionary] retain];
	[_kudari release], _kudari = [[NSMutableDictionary dictionary] retain];
	[_nobori release], _nobori = [[NSMutableDictionary dictionary] retain];
	[_ressya release], _ressya = [[NSMutableDictionary dictionary] retain];

	// judge file version
	if( 0 == ( _version = [self judgeFileVersion:[lines objectAtIndex:0]]))
		return nil;

	// 1st pass
	SEL	theSelector = @selector( createRoot: );
	[_stack addObject:@"Root"];
	for( id theLine in lines )
	{
		NSInteger	theLength = [theLine length];
		BOOL	hasDot = [theLine hasSuffix:@"."];
		BOOL	hasEqual = ([theLine rangeOfString:@"="].location != NSNotFound );
		if(( !hasEqual && !hasDot ) || 0 == theLength )
			continue;	// do nothing
		theSelector = (SEL)[self performSelector:theSelector withObject:theLine];
//		SEL	theResult = (SEL)[self performSelector:theSelector withObject:theLine];
//		NSLog(@"theResult=%@",NSStringFromSelector(theResult));
//		theSelector = theResult;
//		NSLog(@"_stack=%@",_stack);
	}
//	NSLog(@"_ouDiaRoot=%@",_ouDiaRoot);

	return [_ouDiaRoot valueForKeyPath:@"Rosen.Dia.DiaName"];
}
// -----------------------------------------------------------------------------
//	convertWithDiaIndex:
// -----------------------------------------------------------------------------
- (NSDictionary*)convertWithDiaIndex:(NSInteger)index
{
	// 2nd pass
	NSMutableString*	theVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"DGXDocumentVersion"];
	NSMutableDictionary*	theInfo = [NSMutableDictionary dictionary];
	NSMutableArray*	theStations = [NSMutableArray array];
	NSMutableArray*	theClassifications = [NSMutableArray array];
	NSMutableArray*	theDownTrains = [NSMutableArray array];
	NSMutableArray*	theUpTrains = [NSMutableArray array];
	// info
	[self createInfo:theInfo fromOuDia:[_ouDiaRoot valueForKeyPath:@"Rosen"] index:index];
	// station
	[self createStation:theStations fromOuDia:[_ouDiaRoot valueForKeyPath:@"Rosen.Eki"]];
	// classification
	[self createClassification:theClassifications fromOuDia:[_ouDiaRoot valueForKeyPath:@"Rosen.Ressyasyubetsu"]];
	// downTrains and upTrains
	NSDictionary*	theDia = [[_ouDiaRoot valueForKeyPath:@"Rosen.Dia"] objectAtIndex:index];
	NSArray*	theTrains;
	theTrains = [theDia valueForKeyPath:@"Kudari.Ressya"];
	[self createTrain:theDownTrains fromOuDia:theTrains withStationCount:[theStations count] direction:YES];
	theTrains = [theDia valueForKeyPath:@"Nobori.Ressya"];
	[self createTrain:theUpTrains fromOuDia:theTrains withStationCount:[theStations count] direction:NO];

	NSDictionary*	theResult = [NSDictionary dictionaryWithObjectsAndKeys:
		theVersion, DGXVersion, 
		theInfo, DGXInfo, 
		theStations, DGXStation, 
		theClassifications, DGXClassification, 
		theDownTrains, DGXDownTrain, 
		theUpTrains, DGXUpTrain, nil];
//	return _ouDiaRoot;
	return theResult;
}
#pragma mark -
// -----------------------------------------------------------------------------
//	linesWithContentsOfURL:
// -----------------------------------------------------------------------------
- (NSArray*)linesWithContentsOfURL:(NSURL*)fileURL
{
	// first try SJIS encoding
	NSError*	theError = nil;
	NSString*	theContents = [NSString stringWithContentsOfURL:fileURL encoding:NSShiftJISStringEncoding error:&theError];
	if( theError )
	{
		// if unsuccessful -> try auto mode
		NSStringEncoding	theEncoding = 0;
		theContents = [NSString stringWithContentsOfURL:fileURL usedEncoding:&theEncoding error:&theError];
		if( theError )
			return nil;
	}

	// first try CR+LF
	NSArray*	theLines = [theContents componentsSeparatedByString:@"\r\n"];
	if([theLines count] > 1)
		return theLines;

	// if unsuccessful -> try other EOLs
	NSUInteger	length = [theContents length];
	NSUInteger	paraStart = 0, paraEnd = 0, contentsEnd = 0;
	NSMutableArray*	theTempLines = [NSMutableArray array];
	NSRange currentRange;
	while( paraEnd < length )
	{
		[theContents getParagraphStart:&paraStart end:&paraEnd
			contentsEnd:&contentsEnd forRange:NSMakeRange( paraEnd, 0 )];
		currentRange = NSMakeRange( paraStart, contentsEnd - paraStart );
		[theTempLines addObject:[theContents substringWithRange:currentRange]];
	}
	return ([theTempLines count] > 1) ? [NSArray arrayWithArray:theTempLines] : nil;
}
// =============================================================================
//	convert:
// =============================================================================
- (NSDictionary*)convert:(NSURL*)fileURL
{
	NSArray*	theLines = [self linesWithContentsOfURL:fileURL];
	if( !theLines )
		return nil;

	NSInteger	theIndex = 0;
	NSArray*	theItems = [self parse:theLines];
	if( !theItems )
		return nil;	// if parse error -> return nil;

//	if([theItems count] > 0 )	// for debug
	if([theItems count] > 1 )
	{
		NSString*	theFilename = [[[fileURL path] lastPathComponent] stringByDeletingPathExtension];
		// Let user select one within maltiple dia data, by NSAlert panel + PopUpButton in accessory view
		NSPopUpButton*	thePUB = [[[NSPopUpButton alloc] initWithFrame:NSMakeRect( 0.0, 0.0, 160.0, 26.0 )] autorelease];
		[thePUB addItemsWithTitles:theItems];
		NSAlert*	theAlert = [[[NSAlert alloc] init] autorelease];
		NSString*	theFormat = NSLocalizedString( @"multipleData", @"\"%@\" may have multiple Dia data." );
		[theAlert setMessageText:[NSString stringWithFormat:theFormat, theFilename]];
		[theAlert setInformativeText:NSLocalizedString( @"selectPopUpButton", @"Select one with the popup button." )];
		[theAlert setAccessoryView:thePUB];
		[theAlert runModal];

		theIndex = [thePUB indexOfSelectedItem];
	}
	return [self convertWithDiaIndex:theIndex];
}

@end
