//
//  DGXUpdater.h
//  CoreDia
//
//  Created by KITAHARA Motohiko on 06-09-26.
//  Copyright 2006-10 horazaka.net. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface DGXUpdater : NSObject
{
}

- (NSDictionary*)convert:(NSURL*)fileURL;

@end
