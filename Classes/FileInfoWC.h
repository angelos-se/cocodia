//
//  FileInfoWC.h
//  CocoDia
//
//  Created by 北原 基彦 on 08-08-30.
//  Copyright 2008 horazaka.net. All rights reserved.
//

#import <Cocoa/Cocoa.h>


extern NSString*	DGXMainWindowDidChangeNotification;
extern NSString*	DGXDocumentWindowWillCloseNotification;

@interface FileInfoWC : NSWindowController
{
	NSManagedObjectModel*	_managedObjectModel;
	NSPersistentStoreCoordinator*	_persistentStoreCoodinator;
	NSManagedObjectContext*	_managedObjectContext;

	id	_fileInfo;
}
@property (readonly) NSManagedObjectModel*	managedObjectModel;
@property (readonly) NSPersistentStoreCoordinator*	persistentStoreCoodinator;
@property (readonly) NSManagedObjectContext*	managedObjectContext;

- (NSArray*) validDocumentWindows;

// methods invoked with notification
- (void) mainWindowDidChange:(NSNotification*)aNotification;
- (void) documentWindowWillClose:(NSNotification*)aNotification;

@end
