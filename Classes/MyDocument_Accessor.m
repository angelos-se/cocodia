//
//  MyDocument_Accessor.m
//  CoreDia
//
//  Created by 北原 基彦 on 09-10-19.
//  Copyright 2009-10 horazaka.net. All rights reserved.
//

#import "MyDocument_Accessor.h"

#import "Train.h"
#import "DGXKeys.h"


@implementation MyDocument (Accessor)
// =============================================================================
//	info
// =============================================================================
- (NSDictionary*)info
{
	NSManagedObjectContext*	theContext = [self managedObjectContext];
	NSEntityDescription*	theEntity = [NSEntityDescription entityForName:@"Info" inManagedObjectContext:theContext];
	NSFetchRequest*	theRequest = [[[NSFetchRequest alloc] init] autorelease];
	[theRequest setEntity:theEntity];
	NSArray*	theObjects = [theContext executeFetchRequest:theRequest error:NULL];
	id	theObject = [theObjects lastObject];

	return [theObject dictionaryRepresentation];
}
// =============================================================================
//	station
// =============================================================================
- (NSArray*)station
{
	NSManagedObjectContext*	theContext = [self managedObjectContext];
	NSEntityDescription*	theEntity = [NSEntityDescription entityForName:@"Station" inManagedObjectContext:theContext];
	NSFetchRequest*	theRequest = [[[NSFetchRequest alloc] init] autorelease];
	[theRequest setEntity:theEntity];
    NSSortDescriptor*   theDescriptor = [[NSSortDescriptor alloc] initWithKey:(NSString*)DGXIndex ascending:YES];
    [theRequest setSortDescriptors:[NSArray arrayWithObject:theDescriptor]];
    [theDescriptor release];
	NSArray*	theObjects = [theContext executeFetchRequest:theRequest error:NULL];

	return [theObjects valueForKey:@"dictionaryRepresentation"];
}
// =============================================================================
//	classification
// =============================================================================
- (NSArray*)classification
{
	NSManagedObjectContext*	theContext = [self managedObjectContext];
	NSEntityDescription*	theEntity = [NSEntityDescription entityForName:@"Classification" inManagedObjectContext:theContext];
	NSFetchRequest*	theRequest = [[[NSFetchRequest alloc] init] autorelease];
	[theRequest setEntity:theEntity];
    NSSortDescriptor*   theDescriptor = [[NSSortDescriptor alloc] initWithKey:(NSString*)DGXIndex ascending:YES];
    [theRequest setSortDescriptors:[NSArray arrayWithObject:theDescriptor]];
    [theDescriptor release];
	NSArray*	theObjects = [theContext executeFetchRequest:theRequest error:NULL];

	return [theObjects valueForKey:@"dictionaryRepresentation"];
}
// -----------------------------------------------------------------------------
//	trainByDirection:
// -----------------------------------------------------------------------------
- (NSArray*)trainByDirection:(BOOL)direction
{
	NSManagedObjectContext*	theContext = [self managedObjectContext];
	NSEntityDescription*	theEntity = [NSEntityDescription entityForName:@"Train" inManagedObjectContext:theContext];
	NSFetchRequest*	theRequest = [[[NSFetchRequest alloc] init] autorelease];
	[theRequest setEntity:theEntity];
    NSSortDescriptor*   theDescriptor = [[NSSortDescriptor alloc] initWithKey:(NSString*)DGXIndex ascending:YES];
    [theRequest setSortDescriptors:[NSArray arrayWithObject:theDescriptor]];
    [theDescriptor release];
	NSPredicate*	thePredicate = [NSPredicate predicateWithFormat:@"direction == %d", direction];
	[theRequest setPredicate:thePredicate];
	NSArray*	theObjects = [theContext executeFetchRequest:theRequest error:NULL];

	NSMutableArray*	theNewObjects = [NSMutableArray array];
	for( id theObject in theObjects)
	{
		[theNewObjects addObject:[theObject dictionaryRepresentation]];
	}
	return [NSArray arrayWithArray:theNewObjects];
}
// =============================================================================
//	downTrain
// =============================================================================
- (NSArray*)downTrain
{
	return [self trainByDirection:YES];
}
// =============================================================================
//	upTrain
// =============================================================================
- (NSArray*)upTrain
{
	return [self trainByDirection:NO];
}
#pragma mark -
// =============================================================================
//	setInfo:
// =============================================================================
- (void)setInfo:(NSDictionary*)aInfo
{
	NSManagedObjectContext*	theContext = [self managedObjectContext];
	id	theNewObject = [NSEntityDescription insertNewObjectForEntityForName:@"Info" inManagedObjectContext:theContext];
	[theNewObject setValuesForKeysWithDictionary:aInfo];
	return;
}
// =============================================================================
//	setStation:
// =============================================================================
- (void)setStation:(NSArray*)stations
{
	NSManagedObjectContext*	theContext = [self managedObjectContext];
	NSInteger	i = 0;
	for( id theObject in stations )
	{
		id	theNewObject = [NSEntityDescription insertNewObjectForEntityForName:@"Station" inManagedObjectContext:theContext];
		[theNewObject setValuesForKeysWithDictionary:theObject];
		[theNewObject setValue:[NSNumber numberWithInteger:i++] forKey:(NSString*)DGXIndex];
	}
	return;
}
// =============================================================================
//	setClassification:
// =============================================================================
- (void)setClassification:(NSArray*)classifications
{
	NSManagedObjectContext*	theContext = [self managedObjectContext];
	NSInteger	i = 0;
	for( id theObject in classifications )
	{
		id	theNewObject = [NSEntityDescription insertNewObjectForEntityForName:@"Classification" inManagedObjectContext:theContext];
		[theNewObject setValuesForKeysWithDictionary:theObject];
		[theNewObject setValue:[NSNumber numberWithInteger:i++] forKey:(NSString*)DGXIndex];
	}
	return;
}
// -----------------------------------------------------------------------------
//	setTrain:withDirection:classification:station:startIndex:
// -----------------------------------------------------------------------------
- (NSUInteger)setTrain:(NSArray*)trains withDirection:(BOOL)direction classification:(NSArray*)classifications station:(NSArray*)stations startIndex:(NSUInteger)startIndex
{
	NSManagedObjectContext*	theContext = [self managedObjectContext];
	NSArray*	theKeys = [Train copykeys];
	NSUInteger i = startIndex;
	for( id theTrain in trains )
	{
		id	theNewTrain = [NSEntityDescription insertNewObjectForEntityForName:@"Train" inManagedObjectContext:theContext];
		// set attributes
		[theNewTrain setValue:[NSNumber numberWithUnsignedInteger:i++] forKey:(NSString*)DGXIndex];
		[theNewTrain setValue:[classifications objectAtIndex:[[theTrain valueForKey:(NSString*)DGXClassificationID] integerValue]] forKey:(NSString*)DGXClassification];
		[theNewTrain setValue:[NSNumber numberWithBool:direction] forKey:(NSString*)DGXDirection];
		[theNewTrain setValuesForKeysWithDictionary:[theTrain dictionaryWithValuesForKeys:theKeys]];
		// set relationship
		NSArray*	theTables = [theTrain valueForKey:(NSString*)DGXTimetable];
		NSMutableSet*	theNewTables = [NSMutableSet set];
		NSUInteger	j = 0;
		for( id theTime in theTables )
		{
			id	theNewTable = [NSEntityDescription insertNewObjectForEntityForName:@"Table" inManagedObjectContext:theContext];
			NSArray*	theItems = [theTime componentsSeparatedByString:@"/"];
			if (1 == [theItems count])
			{
				if (0 != [theTime length])
					[theNewTable setValue:theTime forKey:(NSString*)DGXDeparture];
			}
			else
			{
				NSString*	theItem = [theItems objectAtIndex:0];
				if (0 != [theItem length])
					[theNewTable setValue:theItem forKey:(NSString*)DGXArrival];
				theItem = [theItems lastObject];
				if (0 != [theItem length])
					[theNewTable setValue:theItem forKey:(NSString*)DGXDeparture];
			}
			[theNewTable setValue:[stations objectAtIndex:j++] forKey:(NSString*)DGXStation];
			[theNewTables addObject:theNewTable];
		}
		[theNewTrain setValue:theNewTables forKey:(NSString*)DGXTables];
	}
	return i;
}
// =============================================================================
//	setTrainWithDown:andUp:
// =============================================================================
- (void)setTrainWithDown:(NSArray*)downTrains andUp:(NSArray*)upTrains
{
	NSManagedObjectContext*	theContext = [self managedObjectContext];
	NSFetchRequest*	theRequest = [[[NSFetchRequest alloc] init] autorelease];
	NSSortDescriptor*	theDescriptor = [[NSSortDescriptor alloc] initWithKey:(NSString*)DGXIndex ascending:YES];
	NSArray*	theDescriptors = [NSArray arrayWithObject:theDescriptor];
    [theDescriptor release];

	NSEntityDescription*	theEntity = [NSEntityDescription entityForName:@"Classification" inManagedObjectContext:theContext];
	[theRequest setEntity:theEntity];
	[theRequest setSortDescriptors:theDescriptors];
	NSArray*	theClassifications = [theContext executeFetchRequest:theRequest error:NULL];

	theEntity = [NSEntityDescription entityForName:@"Station" inManagedObjectContext:theContext];
	[theRequest setEntity:theEntity];
//	[theRequest setSortDescriptors:theDescriptors];
	NSArray*	theStations = [theContext executeFetchRequest:theRequest error:NULL];

	NSUInteger	theIndex = 0;
	theIndex = [self setTrain:downTrains withDirection:YES classification:theClassifications station:theStations startIndex:theIndex];
	theIndex = [self setTrain:upTrains withDirection:NO classification:theClassifications station:theStations startIndex:theIndex];	// theIndex is not used anymore
	return;
}

@end
