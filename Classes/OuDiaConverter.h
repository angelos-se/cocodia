//
//  OuDiaConverter.h
//  CoreDia
//
//  Created by KITAHARA Motohiko on 07-12-09.
//  Copyright 2007-10 horazaka.net. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface OuDiaConverter : NSObject
{
	NSInteger	_version;
	NSMutableArray*	_stack;
	NSMutableDictionary*	_ouDiaRoot;

	NSMutableDictionary*	_rosen;
	NSMutableDictionary*	_dispProp;
	NSMutableDictionary*	_eki;
	NSMutableDictionary*	_ressyasyubetsu;
	NSMutableDictionary*	_dia;
	NSMutableDictionary*	_kudari;
	NSMutableDictionary*	_nobori;
	NSMutableDictionary*	_ressya;

	NSDictionary*	_converterInfo;
}
- (NSDictionary*)convert:(NSURL*)fileURL;

@end
