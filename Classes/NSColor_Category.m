//
//  NSColor_Category.m
//  CoreDia
//
//  Created by 北原 基彦 on 10-09-28.
//  Copyright 2010 horazaka.net. All rights reserved.
//

#import "NSColor_Category.h"


@implementation NSColor (Category)
+ (NSColor*)colorFromString:(NSString*)aString
{
	NSArray*	theRGBA = [aString componentsSeparatedByString:@" "];
	CGFloat	theRed = [[theRGBA objectAtIndex:0] floatValue];
	CGFloat	theGreen = [[theRGBA objectAtIndex:1] floatValue];
	CGFloat	theBlue = [[theRGBA objectAtIndex:2] floatValue];
	CGFloat	theAlpha = [[theRGBA objectAtIndex:3] floatValue];
	return [NSColor colorWithDeviceRed:theRed green:theGreen blue:theBlue alpha:theAlpha];
}
+ (NSString*)stringFromColor:(NSColor*)aColor
{
	NSColor*	theColor = [aColor colorUsingColorSpaceName:NSCalibratedRGBColorSpace];
	CGFloat	theRed = [theColor redComponent];
	CGFloat	theGreen = [theColor greenComponent];
	CGFloat theBlue = [theColor blueComponent];
	CGFloat	theAlpha = [theColor alphaComponent];
	return [NSString stringWithFormat:@"%f %f %f %f",theRed, theGreen, theBlue, theAlpha];
}
// -----------------------------------------------------------------------------
//	convertWinDIAColor:
//		aNumber = ( originalNumber >> 2 ) & 0x1F
//		cf. createClassification:fromLine: in WinDIAConverter.m
// -----------------------------------------------------------------------------
+ (NSColor*)convertWinDIAColor:(NSInteger)aNumber
{
	switch( aNumber )
	{
		default:	return [ NSColor blackColor ];	// black
		case 16:	return [ NSColor blueColor ];	// blue
		case 17:	return [ NSColor greenColor ];	// green
		case 18:	return [ NSColor colorWithCalibratedRed : 0.0 green : 0.0 blue : 128 / 255 alpha : 1.0 ];	// navy
		case 19:	return [ NSColor colorWithCalibratedRed : 0.0 green : 100 / 255 blue : 0.0  alpha : 1.0 ];	// darkgreen
		case 20:	return [ NSColor cyanColor ];	// cyan
		case 21:	return [ NSColor redColor ];	// red
		case 22:	return [ NSColor colorWithCalibratedRed : 0.0 green : 139 / 255 blue : 139 / 255  alpha : 1.0 ];	// DarkCyan
		case 23:	return [ NSColor colorWithCalibratedRed : 139 / 255 green : 0.0 blue : 0.0  alpha : 1.0 ];	// DarkRed
		case 24:	return [ NSColor magentaColor ];	// magenta
		case 25:	return [ NSColor yellowColor ];	// yellow
		case 26:	return [ NSColor colorWithCalibratedRed : 139 / 255 green : 0.0 blue : 139 / 255  alpha : 1.0 ];	// DarkMagent
		case 27:	return [ NSColor colorWithCalibratedRed : 1.0 green : 215 / 255 blue : 0.0  alpha : 1.0 ];	// gold
		case 28:	return [ NSColor blackColor ];	// black
		case 29:	return [ NSColor colorWithCalibratedRed : 105 / 255 green : 105 / 255 blue : 105 / 255  alpha : 1.0 ];	// DimGrey
		case 30:	return [ NSColor grayColor ];	// gray
		case 31:	return [ NSColor whiteColor ];	// white
	}
}

@end
