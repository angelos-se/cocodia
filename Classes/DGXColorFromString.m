//
//  DGXColorFromString.m
//  CoreDia
//
//  Created by 北原 基彦 on 10-09-28.
//  Copyright 2010 horazaka.net. All rights reserved.
//

#import "DGXColorFromString.h"
#import "NSColor_Category.h"


@implementation DGXColorFromString
// =============================================================================
//	transformedValueClass
// =============================================================================
+ (Class)transformedValueClass
{
	return [NSColor class];
}
// =============================================================================
//	allowsReverseTransformation
// =============================================================================
+ (BOOL)allowsReverseTransformation
{
	return YES;
}
// =============================================================================
//	transformedValue:
// =============================================================================
- (id)transformedValue:(id)beforeObject
{
//	NSLog(@"%s,beforeObject=%@,%@",__FUNCTION__,[beforeObject class],beforeObject);
	if ([beforeObject respondsToSelector:@selector(length)])	// ie beforeObject is NSCFString
		return [NSColor colorFromString:beforeObject];
	else
		return [NSColor stringFromColor:beforeObject];
}
@end
