//
//  DGXUpdater.m
//  CoreDia
//
//  Created by KITAHARA Motohiko on 06-09-26.
//  Copyright 2006-10 horazaka.net. All rights reserved.
//

#import "DGXUpdater.h"

#import "NSColor_Category.h"
#import "DGXKeys.h"

static NSInteger	DGXCanReadColorData = 41;
static NSInteger	DGXRenameStateValue = 45;
static NSInteger	DGXIntroduceIsShown = 51;
static NSInteger	DGXIntroduceVisible = 60;
static NSInteger	DGXChangeTypeOfNumber = 63;
static NSInteger	DGXChangeTypeOfLineColor = 70;

@implementation DGXUpdater
// -----------------------------------------------------------------------------
//	update:
// -----------------------------------------------------------------------------
- (NSDictionary*)update:(NSDictionary*)anOriginal
{
	NSMutableDictionary*	theNewDictionary = [NSMutableDictionary dictionaryWithDictionary:anOriginal];
	NSInteger	theVersion = [[anOriginal valueForKey:(NSString*)DGXVersion] integerValue];
	if( theVersion < DGXCanReadColorData )	// ver < 41
	{
		for( id theClassification in [anOriginal valueForKey:(NSString*)DGXClassification])
		{
			NSInteger	theOldColor = [[theClassification valueForKey:(NSString*)DGXLineColor] integerValue];
			NSColor*	theNewColor = [NSColor convertWinDIAColor:theOldColor];
			[theClassification setValue:[NSArchiver archivedDataWithRootObject:theNewColor] forKey:(NSString*)DGXLineColor];
		}
	}
	if( theVersion < DGXRenameStateValue )	// ver < 45
	{
		NSArray*	theStations = [anOriginal valueForKey:(NSString*)DGXStation];
		NSUInteger i, count = [theStations count];
		for (i = 0; i < count; i++)
		{
			id theStation = [theStations objectAtIndex:i];
			NSInteger	theOldValue = [[theStation valueForKey:(NSString*)DGXState] integerValue];
			NSInteger	theNewValue = (( i == 0 ) || ( i == count - 1 ) || ( 2 == theOldValue )) ? 2 : 1;
			[theStation setValue:[NSNumber numberWithInteger:theNewValue] forKey:(NSString*)DGXState];
		}
	}
	if( theVersion < DGXIntroduceIsShown )	// ver < 51
	{
		for (id theTrain in [theNewDictionary valueForKey:(NSString*)DGXDownTrain])
		{
			[theTrain setValue:[NSNumber numberWithBool:YES] forKey:(NSString*)DGXIsShown];
		}
		for (id theTrain in [theNewDictionary valueForKey:(NSString*)DGXUpTrain]) {
			[theTrain setValue:[NSNumber numberWithBool:YES] forKey:(NSString*)DGXIsShown];
		}
	}
	if( theVersion < DGXIntroduceVisible )	// ver < 60
	{
		for (id theTrain in [theNewDictionary valueForKey:(NSString*)DGXDownTrain])
		{
			id	theValue = [theTrain valueForKey:(NSString*)DGXIsShown];
			[theTrain setValue:theValue forKey:(NSString*)DGXVisible];
//			[theTrain removeObjectForKey:(NSString*)DGXIsShown];
		}
		for (id theTrain in [theNewDictionary valueForKey:(NSString*)DGXUpTrain] )
		{
			id	theValue = [theTrain valueForKey:(NSString*)DGXIsShown];
			[theTrain setValue:theValue forKey:(NSString*)DGXVisible];
//			[theTrain removeObjectForKey:(NSString*)DGXIsShown];
		}
	}
	if( theVersion < DGXChangeTypeOfNumber )	// ver < 63
	{
		for (id theTrain in [theNewDictionary valueForKey:(NSString*)DGXDownTrain])
		{
			id	theNumber = [theTrain valueForKey:(NSString*)DGXNumber];
			[theTrain setValue:[NSNumber numberWithInteger:[theNumber integerValue]] forKey:(NSString*)DGXNumber];
		}
		for (id theTrain in [theNewDictionary valueForKey:(NSString*)DGXUpTrain])
		{
			id	theNumber = [theTrain valueForKey:(NSString*)DGXNumber];
			[theTrain setValue:[NSNumber numberWithInteger:[theNumber integerValue]] forKey:(NSString*)DGXNumber];
		}
	}
	if( theVersion < DGXChangeTypeOfLineColor )	// ver < 70
	{
		for (id theClassification in [theNewDictionary valueForKey:(NSString*)DGXClassification])
		{
			NSString*	theNewColor = [NSColor stringFromColor:[NSUnarchiver unarchiveObjectWithData:[theClassification valueForKey:(NSString*)DGXLineColor]]];
			[theClassification setValue:theNewColor forKey:(NSString*)DGXLineColor];
		}
	}
//	if( theVersion < ... )
//	{
//		...
//		[theNewDictionary setValue:theNewClassifications forKey:@"..."];
//	}
	return [NSDictionary dictionaryWithDictionary:theNewDictionary];
}
// =============================================================================
//	convert:
// =============================================================================
- (NSDictionary*)convert:(NSURL*)fileURL
{
	NSDictionary*	theDictionary = [NSDictionary dictionaryWithContentsOfURL:fileURL];
	if (!theDictionary)
		return nil;

	return [self update:theDictionary];
}

@end
