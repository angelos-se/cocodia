//
//  WinDIAConverter.m
//  CocoDia
//
//  Created by KITAHARA Motohiko on 06-09-10.
//  Copyright 2006-10 horazaka.net. All rights reserved.
//

#import "WinDIAConverter.h"

#import	"DGXKeys.h"
#import "DGXUtilities.h"
#import "NSColor_Category.h"


@implementation WinDIAConverter
// =============================================================================
//	init
// =============================================================================
- (id)init
{
	self = [super init];
	if( self )
	{
		NSString*	thePath = [[NSBundle mainBundle] pathForResource:@"WinDIAConverter" ofType:@"plist"];
		_converterInfo = [[NSDictionary dictionaryWithContentsOfFile:thePath] retain];
	}
	return self;
}
// =============================================================================
//	dealloc
// =============================================================================
- (void) dealloc
{
	[_converterInfo release], _converterInfo = nil;

	[super dealloc];
}
#pragma mark -
// -----------------------------------------------------------------------------
//	createInfo:fromLine:
// -----------------------------------------------------------------------------
- (void)createInfo:(NSMutableDictionary*)info fromLine:(NSString*)aLine
{
	// line
	[info setValue:aLine forKey:(NSString*)DGXLine];
	// creator
	NSString*	theFormat = NSLocalizedString( @"converted", @"Converted by %@ ver %@" );
	NSString*	theValue = [NSString stringWithFormat:theFormat, 
			[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleExecutable"], 
			[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
	[info setValue:theValue forKey:(NSString*)DGXCreator];
}
// -----------------------------------------------------------------------------
//	createStation:fromLine:
// -----------------------------------------------------------------------------
- (void)createStation:(NSMutableArray*)stations fromLine:(NSString*)aLine
{
	NSArray*	theToken = [aLine componentsSeparatedByString:@","];
	// scan '10.4XYZ' type token
	NSString*	theName = [theToken objectAtIndex:1];
	NSScanner*	theScanner = [NSScanner scannerWithString:theName];
	CGFloat	theDistance = 0.0;
#ifdef __LP64__
	BOOL	theResult = [theScanner scanDouble:&theDistance];	// if no part such as '10.4' -> return NO
#else
	BOOL	theResult = [theScanner scanFloat:&theDistance];	// if no part such as '10.4' -> return NO
#endif
	[stations addObject:[NSDictionary dictionaryWithObjectsAndKeys:
			(( theResult ) ? [theName substringFromIndex:[theScanner scanLocation]] : theName ), DGXName, // if '10.4XYZ' type, 'XYZ' -> name, else whole token -> name
#ifdef __LP64__
			[NSNumber numberWithDouble:theDistance], DGXDistance, 
#else
			[NSNumber numberWithFloat:theDistance], DGXDistance, 
#endif
			[NSNumber numberWithInteger:[[theToken objectAtIndex:0] length]], DGXState, nil]];
}
// -----------------------------------------------------------------------------
//	createClassification:fromLine:
// -----------------------------------------------------------------------------
- (void)createClassification:(NSMutableArray*)classifications fromLine:(NSString*)aLine
{
	NSInteger	i;
	NSDictionary*	theDictionary;
	// convert line with 'LINES='
	if([aLine hasPrefix:@"LINES="])
	{
		NSArray*	theStyle = [[aLine substringFromIndex:6] componentsSeparatedByString:@","];
		NSInteger	j = [classifications count], k = [theStyle count] - 1;
		if( j < k )	// if 'LINES=' has more linestyles than default, then add placeholders
			for( i = j; i < k; i++ )
				[classifications addObject:[_converterInfo valueForKey:@"DGXWinDIAClassificationPlaceholder"]];
		else if( j > k )	// othewise, remove placeholders
			for( i = k; i < j; i++ )
				[classifications removeLastObject];
		else	// j == k
			;	// do nothing
		for( i = 0; i < k; i++ )	// after ajust(add or remove), set linestyles from 'LINES='
		{
			j = [[theStyle objectAtIndex:i] integerValue];
			theDictionary = [classifications objectAtIndex:i];
			NSColor*	theColor = [NSColor convertWinDIAColor:(( j >> 2 ) & 0x1F)];
			[classifications replaceObjectAtIndex:i 
				withObject:[NSDictionary dictionaryWithObjectsAndKeys:
					[theDictionary objectForKey:DGXLongName], DGXLongName, 
					[theDictionary objectForKey:DGXShortName], DGXShortName, 
					[NSNumber numberWithInteger:j & 0x03], DGXLineStyle, 
					[NSColor stringFromColor:theColor], DGXLineColor, 
					[NSNumber numberWithInteger:( j >> 7 ) & 0x01], DGXLineThickness, nil]];
		}
		return;
	}

	// convert lines with 'TRAINxx='
	i = [[aLine substringFromIndex:5] integerValue];
	if([classifications count] <= i )
		return;

	theDictionary = [classifications objectAtIndex:i];
	NSArray*	theName = [[aLine substringFromIndex:8] componentsSeparatedByString:@","];
	[classifications replaceObjectAtIndex:i 
		withObject:[NSDictionary dictionaryWithObjectsAndKeys:
			[theName objectAtIndex:0], DGXLongName, 
			[theName objectAtIndex:1], DGXShortName, 
			[theDictionary objectForKey:DGXLineStyle], DGXLineStyle, 
			[theDictionary objectForKey:DGXLineColor], DGXLineColor, 
			[theDictionary objectForKey:DGXLineThickness], DGXLineThickness, nil]];
	return;
}
// -----------------------------------------------------------------------------
//	createTrain:addClassification:fromLine:
//		create train array
//		add ad-hoc classification to classifications array
// -----------------------------------------------------------------------------
- (void)createTrain:(NSMutableArray*)trains addClassification:(NSMutableArray*)classifications fromLine:(NSString*)aLine
{
	// if theLine has '%' -> there is note
	NSString*	theNote;
	NSArray*	theToken0 = [aLine componentsSeparatedByString:@"%"];
	theNote = (( 1 == [theToken0 count]) ? @"":[theToken0 objectAtIndex:1]);
	// separate the rest of theLine by ','
	NSArray*	theToken = [[theToken0 objectAtIndex:0] componentsSeparatedByString:@","];
	// cut off <tab>s and <cr><lf>s from each token
	NSMutableArray*	theTimetable = [NSMutableArray array];
	// if there is note -> count must be -1
	NSInteger	i, j = [theToken count] + (( 1 == [theToken0 count]) ? 0 : -1 );
	for( i = 4; i < j; i++ )
	{
		NSString*	theTime = [[theToken objectAtIndex:i] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		[theTimetable addObject:theTime];
	}
	NSString*	theClassificationID = nil;
	NSString*	theFirstToken = [theToken objectAtIndex:0];
	if( NSNotFound == [theFirstToken rangeOfString:@"("].location )
		theClassificationID = theFirstToken;
	else
	{
		NSArray*	theWords = [theFirstToken componentsSeparatedByString:@"("];	// ex. 2(128) -> 2, 128)
		NSString*	theFirstWord = [theWords objectAtIndex:0];
		NSString*	theLastWord = [theWords lastObject];
		NSString*	theLongName = [NSString stringWithFormat:@"%@(%@", [[classifications objectAtIndex:[theFirstWord integerValue]] objectForKey:DGXLongName], theLastWord];		// longName -> xxx(128)
		NSString*	theShortName = [theLastWord substringToIndex:[theLastWord length] - 1];	// short -> 128
		i = [theLastWord integerValue];
		NSColor*	theColor = [NSColor convertWinDIAColor:(( i >> 2 ) & 0x1F )];
		NSDictionary*	theClassification = [NSDictionary dictionaryWithObjectsAndKeys: 
			[NSNumber numberWithInteger:i & 0x03], DGXLineStyle, 
			[NSColor stringFromColor:theColor], DGXLineColor, 
			[NSNumber numberWithInteger:( i >> 7 ) & 0x01], DGXLineThickness, 
			theLongName, DGXLongName, 
			theShortName, DGXShortName, nil];
		if( NSNotFound != ( j = [classifications indexOfObject:theClassification]))
		{
			theClassificationID = [NSString stringWithFormat:@"%ld", j];
		}
		else
		{
			theClassificationID = [NSString stringWithFormat:@"%ld", [classifications count]];
			[classifications addObject:theClassification];
		}
	}
	// instantiate Train object and add the instance to down/up train array
	[trains addObject:[NSDictionary dictionaryWithObjectsAndKeys:
			[NSNumber numberWithBool:YES], DGXVisible, 
			[theToken objectAtIndex:1], DGXTrainID,  
			[theToken objectAtIndex:2], DGXNickname,  
			[NSNumber numberWithInteger:[[theToken objectAtIndex:3] integerValue]], DGXNumber, 
			theClassificationID, DGXClassificationID, 
			theTimetable, DGXTimetable,  
			theNote, DGXNote, nil]];
//	NSLog( @"downTrain = %d, upTrain = %d", [trains count]);
	return;
}
// =============================================================================
//	convert:
//		return nil if conversion is not successful
// =============================================================================
- (NSDictionary*)convert:(NSURL*)fileURL
{
	NSArray*	theLabels = [_converterInfo objectForKey:@"DGXWinDIALabels"];

	NSMutableString*	theVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"DGXDocumentVersion"];
	NSMutableDictionary*	theInfo = [NSMutableDictionary dictionary];
	NSMutableArray*	theStations = [NSMutableArray array];
	NSMutableArray*	theClassifications = [NSMutableArray arrayWithArray:[_converterInfo objectForKey:@"DGXWinDIAClassifications"]];
	NSMutableArray*	theDownTrains = [NSMutableArray array];
	NSMutableArray*	theUpTrains = [NSMutableArray array];

	NSError*	theError = nil;
	NSMutableString*	theText = [NSMutableString stringWithContentsOfURL:fileURL 
			encoding:NSShiftJISStringEncoding 
			error:&theError];
	if( theError )
		return nil;
	[theText replaceOccurrencesOfString:@"\x5c\r\n" 
			withString:@"" 
			options:0 
			range:NSMakeRange( 0, [theText length])];
	NSArray*	theLines = [theText componentsSeparatedByString:@"\r\n"];
	if( 1 == [theLines count])
		return nil;
	NSInteger	theIndex, theState = 0;
	for( id theLine in theLines )
	{
		theIndex = [theLabels indexOfObject:theLine];
		if( NSNotFound != theIndex )
			theState = theIndex;
		else
		{
			switch( theState )
			{
				default:
					break;
				case 1:
					[self createInfo:theInfo fromLine:theLine];
					break;
				case 2:
					[self createStation:theStations fromLine:theLine];
					break;
				case 3:
					[self createClassification:theClassifications fromLine:theLine];
					break;
				case 4:
					[self createTrain:theDownTrains addClassification:theClassifications fromLine:theLine];
					break;
				case 5:
					[self createTrain:theUpTrains addClassification:theClassifications fromLine:theLine];
					break;
			}
		}
	}

	NSDictionary*	theResult = [NSDictionary dictionaryWithObjectsAndKeys:
		theVersion, DGXVersion, 
		theInfo, DGXInfo, 
		theStations, DGXStation, 
		theClassifications, DGXClassification, 
		theDownTrains, DGXDownTrain, 
		theUpTrains, DGXUpTrain, nil];
	return theResult;
}

@end
