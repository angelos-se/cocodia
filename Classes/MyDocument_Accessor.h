//
//  MyDocument_Accessor.h
//  CoreDia
//
//  Created by 北原 基彦 on 09-10-19.
//  Copyright 2009-10 horazaka.net. All rights reserved.
//

#import "MyDocument.h"


@interface MyDocument (Accessor)
- (NSDictionary*)info;
- (NSArray*)station;
- (NSArray*)classification;
- (NSArray*)downTrain;
- (NSArray*)upTrain;

- (void)setInfo:(NSDictionary*)aInfo;
- (void)setStation:(NSArray*)stations;
- (void)setClassification:(NSArray*)classifications;
- (void)setTrainWithDown:(NSArray*)downTrains andUp:(NSArray*)upTrains;

@end
