//
//  DGXSortDescriptorOnDirection.m
//  CoreDia
//
//  Created by 北原 基彦 on 09-10-18.
//  Copyright 2009-10 horazaka.net. All rights reserved.
//

#import "DGXSortDescriptorOnDirection.h"


@implementation DGXSortDescriptorOnDirection
// =============================================================================
//	transformedValueClass
// =============================================================================
+ (Class)transformedValueClass
{
	return [NSString class];
}
// =============================================================================
//	allowsReverseTransformation
// =============================================================================
+ (BOOL)allowsReverseTransformation
{
	return NO;
}
// =============================================================================
//	transformedValue:
// =============================================================================
- (id)transformedValue:(id)beforeObject
{
//	NSLog(@"%s,beforeObject=%@,%@",__FUNCTION__,[beforeObject class],beforeObject);
	// if beforeObject comes from TableArrayController, ie beforeObject isKindOfClass NSCFArray
	if([beforeObject respondsToSelector:@selector(count)])
		return nil;
	NSSortDescriptor*	theDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"station.index" ascending:[beforeObject boolValue]] autorelease];
	return [NSArray arrayWithObject:theDescriptor];
}

@end
