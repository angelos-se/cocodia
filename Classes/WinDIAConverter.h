//
//  WinDIAConverter.h
//  CocoDia
//
//  Created by KITAHARA Motohiko on 06-09-10.
//  Copyright 2006-10 horazaka.net. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface WinDIAConverter : NSObject
{
	NSDictionary*	_converterInfo;
}
- (NSDictionary*)convert:(NSURL*)fileURL;

@end
