//
//  NSColor_Category.h
//  CoreDia
//
//  Created by 北原 基彦 on 10-09-28.
//  Copyright 2010 horazaka.net. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface NSColor (Category)
+ (NSColor*)colorFromString:(NSString*)aString;
+ (NSString*)stringFromColor:(NSColor*)aColor;
+ (NSColor*)convertWinDIAColor:(NSInteger)aNumber;
@end
