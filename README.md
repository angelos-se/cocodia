Thanks to Motohiko Kitahara's awesome work, this 10-year-old project builds and works flawlessly on macOS Mojave after just updating the few deprecated build settings.

This repo serves as an archive for the original code hosted at http://www.horazaka.net/cocodia/index.html

The only commit I made to the repo contains only updates to Xcode's project build settings; which hopefully would make it easier for others to build and use this program in the future.