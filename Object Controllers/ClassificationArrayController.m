//
//  ClassificationArrayController.m
//  CocoDia
//
//  Created by KITAHARA Motohiko on 06-08-20.
//  Copyright 2006-10 horazaka.net. All rights reserved.
//

#import "ClassificationArrayController.h"


@implementation ClassificationArrayController
// =============================================================================
//	initWithCoder:
//		initializer from nib decoding
// =============================================================================
- (id)initWithCoder:(NSCoder *)decoder
{
	self = [ super initWithCoder:decoder ];
	{
		_indexKey = [@"index" retain];

		NSSortDescriptor*	theDescriptor =[[NSSortDescriptor alloc] initWithKey:_indexKey ascending:YES];
		[self setSortDescriptors:[NSArray arrayWithObject:theDescriptor]];
		[theDescriptor release];

#if TARGET_VERSION == 1060
		_plistPboardType = [[NSString stringWithFormat:@"net.horazaka.CoreDia.%@",[self entityName]] retain];
#else	// TARGET_VERSION == 1050
		_plistPboardType = [[NSString stringWithFormat:@"%@PlistPboardType", [self class]] retain ];
		_urlPboardType = [[NSString stringWithFormat:@"%@URLPboardType", [self class]] retain];
#endif
		_writablePboardTypes = [[NSArray arrayWithObjects:_plistPboardType, nil] retain];
		_readablePboardTypes = [[NSArray arrayWithObjects:_plistPboardType, nil] retain];
//		readablePboardTypes = [[NSArray arrayWithObjects:_plistPboardType, NSStringPboardType, nil] retain];
	}
	return self;
}

@end
