//
//  TableArrayController.m
//  CocoDia
//
//  Created by KITAHARA Motohiko on 06-07-30.
//  Copyright 2006-10 horazaka.net . All rights reserved.
//

#import	"DGXArrayController.h"


@interface TableArrayController : DGXArrayController
{
    IBOutlet NSStepper *offsetStepper;
}
- (IBAction)shiftTime:(id)sender;

@end
