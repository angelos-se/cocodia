//
//  DGXArrayController.m
//  CocoDia
//
//  Created by KITAHARA Motohiko on 06-07-30.
//  Copyright 2006-10 horazaka.net. All rights reserved.
//

#import "DGXArrayController.h"


@implementation DGXArrayController
// =============================================================================
//	setter/getter
// =============================================================================
@synthesize writablePboardTypes = _writablePboardTypes;
@synthesize readablePboardTypes = _readablePboardTypes;
// =============================================================================
//	dealloc
// =============================================================================
- (void)dealloc
{
	[_indexKey release];
	[_plistPboardType release], _plistPboardType = nil;
	[_urlPboardType release], _urlPboardType = nil;
	[_writablePboardTypes release], _writablePboardTypes = nil;
	[_readablePboardTypes release], _readablePboardTypes = nil;

    [super dealloc];
}
#pragma mark -
// =============================================================================
//	renumberFromIndex
// =============================================================================
- (void)renumberFromIndex:(NSInteger)anIndex
{
	NSArray*	theObjects = [self arrangedObjects];
	NSUInteger	i, j = [theObjects count];
	for(i = anIndex; i < j; i++)
	{
		id	theObject = [theObjects objectAtIndex:i];
		[theObject setValue:[NSNumber numberWithUnsignedInteger:i] forKey:_indexKey];
	}
	[self rearrangeObjects];
}
// =============================================================================
//	scrollRowToVisibleWithFocus:
// =============================================================================
- (void)scrollRowToVisibleWithFocus:(NSUInteger)aRow
{
	[[NSApp keyWindow] makeFirstResponder:_tableView];
	[_tableView scrollRowToVisible:aRow];
}
#pragma mark -
// =============================================================================
//	insertObjects:atArrangedObjectIndexes:
// =============================================================================
- (void)insertObjects:(NSArray *)objects atArrangedObjectIndexes:(NSIndexSet *)indexes
{
	[super insertObjects:objects atArrangedObjectIndexes:indexes];

	// renumber
	[self renumberFromIndex:[indexes firstIndex]];

	// scroll
	[self scrollRowToVisibleWithFocus:[indexes firstIndex]];
}
// =============================================================================
//	removeObjectsAtArrangedObjectIndexes:
// =============================================================================
- (void)removeObjectsAtArrangedObjectIndexes:(NSIndexSet *)indexes
{
	[super removeObjectsAtArrangedObjectIndexes:indexes];

	// renumber
	[self renumberFromIndex:[indexes firstIndex]];
}
#pragma mark -
#pragma mark methods for Pasteboard
// =============================================================================
//	canReadObjectsFromPasteboard:
//		called from self and MyDocument's -validateMenuItem:( cf. Cocoa Programming pp756-758 )
// =============================================================================
- (BOOL)canReadObjectsFromPasteboard:(NSPasteboard*)aPboard
{
	if(!aPboard)
		return NO;
	NSString*	theType = [aPboard availableTypeFromArray:[self readablePboardTypes]];
	return (theType) ? YES : NO;
}
// =============================================================================
//	copyItems:toRow
// =============================================================================
- (BOOL)copyItems:(NSArray *)items toRow:(NSInteger)aRow
{
	NSMutableArray*	theNewObjects = [NSMutableArray array];
	for (id theItem in items)
	{
		id	theNewObject = [self newObject];
#if TARGET_VERSION == 1060
		[theNewObject setValuesForKeysWithDictionary:[theItem propertyListForType:_plistPboardType]];
#else	// TARGET_VERSION == 1050
		[theNewObject setValuesForKeysWithDictionary:theItem];
#endif
		[theNewObjects addObject:theNewObject];
	}
	[self insertObjects:theNewObjects atArrangedObjectIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(aRow, [items count])]];

	return YES;
}
// =============================================================================
//	moveItems:toRow
// =============================================================================
- (BOOL)moveItems:(NSArray *)items toRow:(NSInteger)aRow
{
	NSMutableArray*	theReps = [NSMutableArray array];
	for (id theItem in items)
	{
#if TARGET_VERSION == 1060
		[theReps addObject:[theItem propertyListForType:NSPasteboardTypeString]];
#else	// TARGET_VERSION == 1050
		[theReps addObject:theItem];	// simple copy items to theReps
#endif
	}
//	NSLog(@"theReps=%@",theReps);
	NSArray*	theObjects = [self arrangedObjects];
	NSUInteger	i, j, k = [theObjects count];
	for (i = 0, j = aRow; i < k; i++, j++)	// (i) change IDs for moved objects
	{
		id	theObject = [theObjects objectAtIndex:i];
		if([theReps containsObject:[theObject performSelector:@selector(urlRepresentation)]])
			[theObject setValue:[NSNumber numberWithUnsignedInt:j] forKey:_indexKey];
	}
	for(i = aRow; i < k; i++, j++)	// (ii) change IDs for unmoved objects after inserted point
	{
		id	theObject = [theObjects objectAtIndex:i];
		if(![theReps containsObject:[theObject performSelector:@selector(urlRepresentation)]])
			[theObject setValue:[NSNumber numberWithUnsignedInt:j] forKey:_indexKey];
	}
	[self rearrangeObjects];	// (iii) reorder objects

	for (i = 0; i < k; i++)	// (iv) renumber all IDs
	{
		id	theObject = [theObjects objectAtIndex:i];
		[theObject setValue:[NSNumber numberWithUnsignedInteger:i] forKey:_indexKey];
	}
	return YES;
}
#pragma mark -
// =============================================================================
//	readObjectsFromPasteboard:toRow:copying
// =============================================================================
- (BOOL)readObjectsFromPasteboard:(NSPasteboard *)aPboard toRow:(NSInteger)aRow copying:(BOOL)copyingFlag
{
	if(![self canReadObjectsFromPasteboard:aPboard])
		return NO;

	if(aRow == NSNotFound)	// aRow == NSNotFound ie no selection -> add object
		aRow = [[self arrangedObjects] count];

#if TARGET_VERSION == 1060
	NSDictionary*	theOptions = [NSDictionary dictionary];
	NSArray*	theItems = [aPboard readObjectsForClasses:[NSArray arrayWithObject:[NSPasteboardItem class]] options:theOptions];
	if(!theItems)
		return NO;

//	NSLog(@"theItems=%@,%ld,%ld",theItems,[aPboard canReadItemWithDataConformingToTypes:[NSArray arrayWithObject:_plistPboardType]],[aPboard canReadItemWithDataConformingToTypes:[NSArray arrayWithObject:NSPasteboardTypeString]]);
	if( copyingFlag )	// copy
		return [self copyItems:theItems toRow:aRow];
	else	// move
		return [self moveItems:theItems toRow:aRow];
#else	// TARGET_VERSION == 1050
	if( copyingFlag )	// copy
	{
		NSArray*	theItems = [aPboard propertyListForType:_plistPboardType];
		if(!theItems || ![theItems isKindOfClass:[NSArray class]] || 0 == [theItems count])
			return NO;
		return [self copyItems:theItems toRow:aRow];
	}
	else	// move
	{
		NSArray*	theItems = [aPboard propertyListForType:_urlPboardType];
		if(!theItems || ![theItems isKindOfClass:[NSArray class]] || 0 == [theItems count])
			return NO;
		return [self moveItems:theItems toRow:aRow];
	}
#endif
}
// =============================================================================
//	writeObjectsToPasteboard:
// =============================================================================
- (BOOL)writeObjectsToPasteboard:(NSPasteboard *)aPboard
{
	if(!aPboard)
		return NO;

	NSArray*	theSelections = [self selectedObjects];
	if(!theSelections || 0 == [theSelections count])
		return NO;

#if TARGET_VERSION == 1060
	NSMutableArray*	theItems = [NSMutableArray array];
	for (id theSelection in theSelections)
	{
		NSPasteboardItem*	theItem = [[NSPasteboardItem alloc] initWithPasteboardPropertyList:[theSelection dictionaryRepresentation] ofType:_plistPboardType];
		[theItem setString:[theSelection performSelector:@selector(urlRepresentation)] forType:NSPasteboardTypeString];
		[theItems addObject:theItem];
		[theItem release];
	}
//	NSLog(@"%s,theItems=%@",__FUNCTION__,theItems);

	[aPboard clearContents];
	return [aPboard writeObjects:theItems];
#else	// TARGET_VERSION == 1050
	NSMutableArray*	theDictionaryReps = [NSMutableArray array];
	NSMutableArray*	theURLReps = [NSMutableArray array];
	for (id theObject in theSelections)
	{
		[theDictionaryReps addObject:[theObject dictionaryRepresentation]];
		[theURLReps addObject:[theObject performSelector:@selector(urlRepresentation)]];
	}

	[aPboard declareTypes:[self writablePboardTypes] owner:self];
	BOOL	theResult = [aPboard setPropertyList:theDictionaryReps forType:_plistPboardType];
	if (!theResult)
		return theResult;
	theResult = [aPboard setPropertyList:theURLReps forType:_urlPboardType];
	return theResult;
#endif
}
#pragma mark -
#pragma mark methods for drag & drop (NSTableDataSource Protocol)
// -----------------------------------------------------------------------------
//	tableView:writeRowsWithIndexes:toPasteboard:
// -----------------------------------------------------------------------------
- (BOOL)tableView:(NSTableView*)tableView writeRowsWithIndexes:(NSIndexSet*)rowIndexes toPasteboard:(NSPasteboard*)aPboard
{
    if(NSNotFound == [rowIndexes firstIndex])
		return NO;

	return [self writeObjectsToPasteboard:aPboard];
}
// -----------------------------------------------------------------------------
//	tableView:validateDrop:proposedRow:proposedDropOperation:
// -----------------------------------------------------------------------------
- (NSDragOperation)tableView:(NSTableView*)tableView validateDrop:(id <NSDraggingInfo>)info proposedRow:(NSInteger)row proposedDropOperation:(NSTableViewDropOperation)operation
{
	NSPasteboard*   thePboard = [info draggingPasteboard];	// check thePboard
	if(!thePboard || ![thePboard availableTypeFromArray:[self readablePboardTypes]])
		return NSDragOperationNone;
    
	if( operation == NSTableViewDropOn )	// check operation
        return NSDragOperationNone;

	if( tableView != [info draggingSource]) 	// source and destination is not the same
//		return NSDragOperationNone;
		return NSDragOperationCopy;

	NSDragOperation	theMask = [info draggingSourceOperationMask];
//	NSLog( @"draggingSourceOperationMask = %d", theMask );
	return ( NSDragOperationCopy == theMask ) ? theMask : NSDragOperationMove;
}
// -----------------------------------------------------------------------------
//	tableView:acceptDrop:row:dropOperation:
// -----------------------------------------------------------------------------
- (BOOL)tableView:(NSTableView*)tableView acceptDrop:(id <NSDraggingInfo>)info row:(NSInteger)row dropOperation:(NSTableViewDropOperation)operation
{
	BOOL	theCopying = (NSDragOperationCopy == [info draggingSourceOperationMask]) || (tableView != [info draggingSource]);
	return [self readObjectsFromPasteboard:[info draggingPasteboard] 
				toRow:row 
				copying:theCopying];
}
#pragma mark -
#pragma mark methods for cut, copy, patste
// =============================================================================
//	copy:
// =============================================================================
- (void)copy:(id)sender
{
	[self writeObjectsToPasteboard:[NSPasteboard generalPasteboard]];
//	NSLog(@"copy:sender=%@", sender);
}
// =============================================================================
//	paste:
// =============================================================================
- (void)paste:(id)sender
{
	[self readObjectsFromPasteboard:[NSPasteboard generalPasteboard] 
		toRow:[[self selectionIndexes] firstIndex] 
		copying:YES];
//	NSLog(@"paste:sender=%@", sender);
}
// =============================================================================
//	cut:
// =============================================================================
- (void)cut:(id)sender
{
//	[self copy:sender];
//
//	for(id theObject in [self selectedObjects])
//	{
//		[theObject setValue:nil forKey:@"arrival"];
//		[theObject setValue:nil forKey:@"departure"];
//	}
//	NSLog(@"cut:sender=%@",sender);
}
#pragma mark -
#pragma mark methods for target-action
// =============================================================================
//	add:
//		override to call -insertObjects:atArrangedObjectIndexes: 
//		besides directly invoke -insertObject:atArrangedObjectIndex:
//		since renumber, scroll and notificaton functions are concentrated in -insertObjects:...
//		ie -insertObject:... is not called in DGXArrayController and its derived classes
// =============================================================================
- (void)add:(id)sender
{
	[self insertObjects:[NSArray arrayWithObject:[self newObject]] atArrangedObjectIndexes:[NSIndexSet indexSetWithIndex:[[self arrangedObjects] count]]];
}
// =============================================================================
//	insert:
//		override to call -insertObjects:atArrangedObjectIndexes: 
//		besides directly invoke -insertObject:atArrangedObjectIndex:
//		since renumber, scroll and notificaton functions are concentrated in -insertObjects:...
//		ie -insertObject:... is never implemented in SNDArrayController and its derived classes
// =============================================================================
- (void)insert:(id)sender
{
	NSUInteger	theIndex = [[self selectionIndexes] firstIndex];
	if (NSNotFound == theIndex)
		[self add:sender];
	else
		[self insertObjects:[NSArray arrayWithObject:[self newObject]] atArrangedObjectIndexes:[NSIndexSet indexSetWithIndex:theIndex]];
}

@end
