//
//  DGXArrayController.m
//  CocoDia
//
//  Created by KITAHARA Motohiko on 06-07-30.
//  Copyright 2006-10 horazaka.net . All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DGXViewController.h"


@interface DGXArrayController : NSArrayController
{
	IBOutlet NSTableView*	_tableView;
	IBOutlet DGXViewController*	_viewController;	// refer to File's owner in the same nib

	NSString*	_indexKey;

	NSString*	_plistPboardType;
	NSString*	_urlPboardType;	// use only for 10.5
	NSArray*	_writablePboardTypes;
	NSArray*	_readablePboardTypes;
}
@property (nonatomic, readonly) NSArray*	writablePboardTypes;
@property (nonatomic, readonly) NSArray*	readablePboardTypes;

- (void)renumberFromIndex:(NSInteger)anIndex;
- (void)scrollRowToVisibleWithFocus:(NSUInteger)index;

- (BOOL)canReadObjectsFromPasteboard:(NSPasteboard*)aPboard;
- (BOOL)copyItems:(NSArray *)items toRow:(NSInteger)aRow;
- (BOOL)moveItems:(NSArray *)items toRow:(NSInteger)aRow;
- (BOOL)readObjectsFromPasteboard:(NSPasteboard *)aPboard toRow:(NSInteger)aRow copying:(BOOL)copyingFlag;
- (BOOL)writeObjectsToPasteboard:(NSPasteboard *)aPboard;

- (void)copy:(id)sender;
- (void)paste:(id)sender;
- (void)cut:(id)sender;

@end
