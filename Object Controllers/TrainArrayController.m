//
//  TrainArrayController.m
//  CocoDia
//
//  Created by KITAHARA Motohiko on 06-07-30.
//  Copyright 2006-10 horazaka.net . All rights reserved.
//

#import "TrainArrayController.h"

#import "DGXUtilities.h"


@implementation TrainArrayController
// =============================================================================
//	initWithCoder:
//		initializer from nib decoding
// =============================================================================
- (id)initWithCoder:(NSCoder *)decoder
{
	self = [ super initWithCoder:decoder ];
	{
		_indexKey = [@"index" retain];

		NSSortDescriptor*	theDescriptor =[[NSSortDescriptor alloc] initWithKey:_indexKey ascending:YES];
		[self setSortDescriptors:[NSArray arrayWithObject:theDescriptor]];
		[theDescriptor release];

#if TARGET_VERSION == 1060
		_plistPboardType = [[NSString stringWithFormat:@"net.horazaka.CoreDia.%@",[self entityName]] retain];
#else	// TARGET_VERSION == 1050
		_plistPboardType = [[NSString stringWithFormat:@"%@PlistPboardType", [self class]] retain ];
		_urlPboardType = [[NSString stringWithFormat:@"%@URLPboardType", [self class]] retain];
#endif
		_writablePboardTypes = [[NSArray arrayWithObjects:_plistPboardType, nil] retain];
		_readablePboardTypes = [[NSArray arrayWithObjects:_plistPboardType, nil] retain];
//		readablePboardTypes = [[NSArray arrayWithObjects:_plistPboardType, NSStringPboardType, nil] retain];
	}
	return self;
}
#pragma mark -
// =============================================================================
//	insertObjects:atArrangedObjectIndexes:
// =============================================================================
- (void)insertObjects:(NSArray *)objects atArrangedObjectIndexes:(NSIndexSet *)indexes
{
	[super insertObjects:objects atArrangedObjectIndexes:indexes];

	NSNotificationCenter*	theCenter = [NSNotificationCenter defaultCenter];
	for (id theObject in objects)
	{
		[theCenter postNotificationName:DGXTrainACDidInsertObjectNotification 
			object:self 
			userInfo:[NSDictionary dictionaryWithObject:theObject forKey:@"train"]];
	}
}
#pragma mark -
// =============================================================================
//	copyItems:toRow
// =============================================================================
- (BOOL)copyItems:(NSArray *)items toRow:(NSInteger)aRow
{
	NSManagedObjectContext*	theContext = [self managedObjectContext];
	NSPersistentStoreCoordinator*   theStore = [theContext persistentStoreCoordinator];
	NSMutableArray*	theNewObjects = [NSMutableArray array];
	for( id theItem in items )
	{
#if TARGET_VERSION == 1060
		NSManagedObjectID*	theID = [theStore managedObjectIDForURIRepresentation:[NSURL URLWithString:[theItem propertyListForType:NSPasteboardTypeString]]];
#else	// TARGET_VERSION == 1050
		NSManagedObjectID*	theID = [theStore managedObjectIDForURIRepresentation:[NSURL URLWithString:theItem]];
#endif
		NSManagedObject*    theNewObject = [[theContext objectWithID:theID] copy];
		[theNewObjects addObject:theNewObject];
		[theNewObject release];
	}
	// send message to 'super' not to post notifications
	[super insertObjects:theNewObjects atArrangedObjectIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(aRow, [items count])]];

	return YES;
}
#pragma mark -
#if TARGET_VERSION == 1060
#else	// TARGET_VERSION == 1050
// =============================================================================
//	readObjectsFromPasteboard:toRow:copying
// =============================================================================
- (BOOL)readObjectsFromPasteboard:(NSPasteboard *)aPboard toRow:(NSInteger)aRow copying:(BOOL)copyingFlag
{
	if(![self canReadObjectsFromPasteboard:aPboard])
		return NO;

	if(aRow == NSNotFound)	// aRow == NSNotFound ie no selection -> add object
		aRow = [[self arrangedObjects] count];

	NSArray*	theItems = [aPboard propertyListForType:_urlPboardType];
	if(!theItems || ![theItems isKindOfClass:[NSArray class]] || 0 == [theItems count])
		return NO;
	if( copyingFlag )	// copy
		return [self copyItems:theItems toRow:aRow];
	else	// move
		return [self moveItems:theItems toRow:aRow];
}
#endif
#pragma mark -
#pragma mark methods for drag & drop (NSTableDataSource Protocol)
// -----------------------------------------------------------------------------
//	tableView:validateDrop:proposedRow:proposedDropOperation:
// -----------------------------------------------------------------------------
- (NSDragOperation)tableView:(NSTableView*)tableView validateDrop:(id <NSDraggingInfo>)info proposedRow:(NSInteger)row proposedDropOperation:(NSTableViewDropOperation)operation
{
    NSPasteboard*   thePboard = [info draggingPasteboard];	// check thePboard
    if(!thePboard || ![[thePboard types] containsObject:_plistPboardType])
        return NSDragOperationNone;
    
    if(operation == NSTableViewDropOn)	// drop above -> go, drop on -> no operation 
        return NSDragOperationNone;

	if(tableView != [info draggingSource]) 	// source != destination -> no operation
		return NSDragOperationNone;
//		return NSDragOperationCopy;

	NSDragOperation	theMask = [info draggingSourceOperationMask];
//	NSLog(@"draggingSourceOperationMask = %d", theMask);
	return (NSDragOperationCopy == theMask) ? theMask : NSDragOperationMove;
}

@end
