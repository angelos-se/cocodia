//
//  TableArrayController.m
//  CocoDia
//
//  Created by KITAHARA Motohiko on 06-07-30.
//  Copyright 2006-10 horazaka.net . All rights reserved.
//

#import "TableArrayController.h"

#import "DGXSortDescriptorOnDirection.h"
#import "DGXKeys.h"
#import "DGXUtilities.h"

@interface TableArrayController (PrivateMethods)
- (BOOL)fetchString:(id)anItem toRow:(NSInteger)aRow copying:(BOOL)copyFlag;
- (BOOL)fetchItems:(NSArray*)items toRow:(NSInteger)aRow copying:(BOOL)copyFlag;
@end


@implementation TableArrayController
// =============================================================================
//	initialize
// =============================================================================
+ (void)initialize
{
	DGXSortDescriptorOnDirection*	theTransformer = [[[DGXSortDescriptorOnDirection alloc] init] autorelease];
	[NSValueTransformer setValueTransformer:theTransformer forName:@"DGXSortDescriptorOnDirection"];
}
#pragma mark -
// =============================================================================
//	initWithCoder:
// =============================================================================
- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if(self)
	{
		_indexKey = [@"station.index" retain];

		NSSortDescriptor*	theDescriptor =[[NSSortDescriptor alloc] initWithKey:_indexKey ascending:YES];
		[self setSortDescriptors:[NSArray arrayWithObject:theDescriptor]];
		[theDescriptor release];
		[self setAutomaticallyRearrangesObjects:YES];

#if TARGET_VERSION == 1060
		_plistPboardType = [[NSString stringWithFormat:@"net.horazaka.CoreDia.%@",[self entityName]] retain];
#else	// TARGET_VERSION == 1050
		_plistPboardType = [[NSString stringWithFormat:@"%@PlistPboardType", [self class]] retain];
		_urlPboardType = [[NSString stringWithFormat:@"%@URLPboardType", [self class]] retain];
#endif
		_writablePboardTypes = [[NSArray arrayWithObjects:_plistPboardType, nil] retain];
		_readablePboardTypes = [[NSArray arrayWithObjects:_plistPboardType, NSStringPboardType, nil] retain];
	}
	return self;
}
// =============================================================================
//	awakeFromNib
// =============================================================================
- (void)awakeFromNib
{
	// register notification to notification center
	NSNotificationCenter*	theCenter = [NSNotificationCenter defaultCenter];
	[theCenter addObserver:self selector:@selector(trainACDidInsertObject:) name:DGXTrainACDidInsertObjectNotification object:nil];
	[theCenter addObserver:self selector:@selector(stationACDidInsertObject:) name:DGXStationACDidInsertObjectNotification object:nil];

	[theCenter addObserver:self selector:@selector(tableTVDidEndEditing:) name:NSControlTextDidEndEditingNotification object:_tableView];
}
// =============================================================================
//	dealloc
// =============================================================================
- (void) dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];

	[super dealloc];
}
#pragma mark -
// =============================================================================
//	shiftTime:
// =============================================================================
#define	DO_SHIFTTIME( key )	\
do	\
{	\
	NSInteger	theResult;	\
	NSString*	theFormat;	\
	id	theNewTime;	\
	id	theTime = [theObject valueForKey:( key )];	\
	if( theTime && [theTime length] && ![theTime hasPrefix:@"-"])	\
	{	\
		NSInteger	theOrigin = hhmm2min([theTime intValue]);	\
		theResult = min2hhmm(( theOffset > 0 ) ? addmin( theOrigin, theOffset ) : submin( theOrigin, -theOffset ));	\
		if( ![theTime hasSuffix:@"?"])	\
			theFormat = ( 59 < theResult ) ? @"%d":@"%03d";	\
		else	\
			theFormat = ( 59 < theResult ) ? @"%d?":@"%03d?";	\
		theNewTime = [NSString stringWithFormat:theFormat, theResult];	\
		[theObject setValue:theNewTime forKey:( key )];	\
	}	\
} while(0)
// -----------------------------------------------------------------------------
- (IBAction)shiftTime:(id)sender
{
	NSInteger	theOffset = [offsetStepper intValue];
	if( 0 == theOffset )
		return;	// do nothing
	id	theObjects = nil;
//	if([[self selectedObjects] count])
//		theObjects = [self selectedObjects];
//	else	// if no objects are selected -> change all
		theObjects = [self arrangedObjects];
	for( id theObject in theObjects )
	{
		DO_SHIFTTIME( @"arrival" );
		DO_SHIFTTIME( @"departure" );
	}
}
#pragma mark -
#pragma mark method invoked from notification
// =============================================================================
//	trainACDidInsertObject:
// =============================================================================
- (void)trainACDidInsertObject:(NSNotification*)aNotification
{
	NSManagedObjectContext*	theContext = [_viewController managedObjectContext];
//	NSLog(@"%s,theContext=%@,%@",__FUNCTION__,theContext,[[aNotification object] managedObjectContext]);
	if(theContext != [[aNotification object] managedObjectContext])
	{
//		NSLog(@"skip, MOC is different");
		return;
	}

	NSEntityDescription*	theEntity = [NSEntityDescription entityForName:@"Station" inManagedObjectContext:theContext];
	NSFetchRequest*	theRequest = [[[NSFetchRequest alloc] init] autorelease];
	[theRequest setEntity:theEntity];
	NSError*	theError = nil;
	NSArray*	theStations = [theContext executeFetchRequest:theRequest error:&theError];
	if(theError)
	{
		NSBeep();
		return;
	}
	id	theTrain = [[aNotification userInfo] valueForKey:@"train"];
	for (id theStation in theStations)
	{
		id	theTable = [NSEntityDescription insertNewObjectForEntityForName:@"Table" inManagedObjectContext:theContext];
		[theTable setValue:theStation forKey:@"station"];
		[theTable setValue:theTrain forKey:@"train"];
	}
}
// =============================================================================
//	stationACDidInsertObject:
// =============================================================================
- (void)stationACDidInsertObject:(NSNotification*)aNotification
{
	NSManagedObjectContext*	theContext = [_viewController managedObjectContext];
//	NSLog(@"%s,theContext=%@,%@",__FUNCTION__,theContext,[[aNotification object] managedObjectContext]);
	if(theContext != [[aNotification object] managedObjectContext])
	{
//		NSLog(@"skip, MOC is different");
		return;
	}

	NSEntityDescription*	theEntity = [NSEntityDescription entityForName:@"Train" inManagedObjectContext:theContext];
	NSFetchRequest*	theRequest = [[[NSFetchRequest alloc] init] autorelease];
	[theRequest setEntity:theEntity];
	NSError*	theError = nil;
	NSArray*	theTrains = [theContext executeFetchRequest:theRequest error:&theError];
	if(theError)
	{
		NSBeep();
		return;
	}
	id	theStation = [[aNotification userInfo] valueForKey:@"station"];
	for (id theTrain in theTrains)
	{
		id	theTable = [NSEntityDescription insertNewObjectForEntityForName:@"Table" inManagedObjectContext:theContext];
		[theTable setValue:theTrain forKey:@"train"];
		[theTable setValue:theStation forKey:@"station"];
	}
}
// =============================================================================
//	tableTVDidEndEditing:
//		[tab]	move to right cell or next control on the last col
//		[enter]	move to following row at edit end
//		editColumn:row:withEvent:select: do not work well
// =============================================================================
- (void)tableTVDidEndEditing:(NSNotification*)aNotification
{
//	NSLog(@"%s,aNotification=%@",__FUNCTION__,[aNotification object]);

	NSInteger	theRow = [_tableView editedRow];
	NSInteger	theCol = [_tableView editedColumn];
	NSInteger	theMovement = [[[aNotification userInfo] valueForKey:@"NSTextMovement"] integerValue];
//	NSLog(@"row,col=%ld,%ld,numberOfRows=%ld,theMovement=%ld",theRow,theCol,[_tableView numberOfRows],theMovement);
	if(NSReturnTextMovement != theMovement)
		return;

	switch (theCol) {
		case 2:
//			[_tableView editColumn:theCol+1 row:theRow withEvent:nil select:YES];
//			break;
		case 3:
			{
				NSInteger	theIndex = ([_tableView numberOfRows] == theRow+1) ? 0 : theRow+1;
				NSIndexSet*	theIndexes = [NSIndexSet indexSetWithIndex:theIndex];
				[_tableView selectRowIndexes:theIndexes byExtendingSelection:NO];
				[_tableView scrollRowToVisible:theIndex];
			}
			break;
		default:
			break;
	}
}
#pragma mark -
// -----------------------------------------------------------------------------
//	fetchString:toRow:copying:
// -----------------------------------------------------------------------------
- (BOOL)fetchString:(id)anItem toRow:(NSInteger)aRow copying:(BOOL)copyFlag
{
	NSMutableArray*	theReps = [NSMutableArray array];
#if TARGET_VERSION == 1060
	for	(id theRep in [[anItem stringForType:NSPasteboardTypeString] componentsSeparatedByString:@"\n"])
#else	// TARGET_VERSION == 1050
	for( id theRep in [anItem componentsSeparatedByString:@"\n"])
#endif
	{
		NSInteger	theLength = [theRep length];
		if( 0 == theLength )
			continue;
		else if( 1 == theLength )	// Katakana RE
			[theReps addObject:@"-"];
		else if([theRep hasPrefix:@"0"])	// ex. 06:32
			[theReps addObject:[NSString stringWithFormat:@"%@%@", [theRep substringWithRange:NSMakeRange( 1, 1 )], [theRep substringWithRange:NSMakeRange( theLength - 2, 2 )]]];
		else	// ex. 12:34
			[theReps addObject:[NSString stringWithFormat:@"%@%@", [theRep substringWithRange:NSMakeRange( 0, 2 )], [theRep substringWithRange:NSMakeRange( theLength - 2, 2 )]]];
	}
//	theReps = [[aPboard stringForType:NSStringPboardType] componentsSeparatedByString:@"\n"];
	if( 0 == [theReps count])
		return NO;

	NSArray*	theObjects = [self arrangedObjects];
	NSUInteger	i, j, m = [theObjects count], n = [theReps count];
	for( i = aRow, j = 0; i < m && j < n; i++, j++ )
	{
		id	theObject = [theObjects objectAtIndex:i];
		id	theTime = [theReps objectAtIndex:j];
		if( n > 1 && ( i == m - 1 || j == n - 1 ) && 1 < [theTime length])
			[theObject setValue:theTime forKey:(NSString*)DGXArrival];
		else
			[theObject setValue:theTime forKey:(NSString*)DGXDeparture];
	}
	return YES;
}
// -----------------------------------------------------------------------------
//	fetchItems:toRow:copying:
// -----------------------------------------------------------------------------
- (BOOL)fetchItems:(NSArray*)items toRow:(NSInteger)aRow copying:(BOOL)copyFlag
{
	NSMutableArray*	theReps = [NSMutableArray array];
	for (id theItem in items)
	{
#if TARGET_VERSION == 1060
		[theReps addObject:[theItem propertyListForType:_plistPboardType]];
#else	// TARGET_VERSION == 1050
		[theReps addObject:theItem];	// simple copy items to theReps
#endif
	}
//	NSLog(@"%s,theReps=%@,copyflag=%ld",__FUNCTION__,theReps,copyFlag);
	if(!copyFlag)	// move -> set @"" before set values
	{
		for(id theObject in [self selectedObjects])
		{
			[theObject setValue:nil forKey:(NSString*)DGXArrival];
			[theObject setValue:nil forKey:(NSString*)DGXDeparture];
		}
	}
	NSArray*	theObjects = [self arrangedObjects];
	NSUInteger	i, j, m = [theObjects count], n = [theReps count];
	for(i = aRow, j = 0; i < m && j < n; i++, j++)
	{
		id	theObject = [theObjects objectAtIndex:i];
		id	theRep = [theReps objectAtIndex:j];
		[theObject setValue:[theRep valueForKey:(NSString*)DGXArrival] forKey:(NSString*)DGXArrival];
		[theObject setValue:[theRep valueForKey:(NSString*)DGXDeparture] forKey:(NSString*)DGXDeparture];
	}
	return YES;
}
// =============================================================================
//	readObjectsFromPasteboard:toRow:copying:
// =============================================================================
- (BOOL)readObjectsFromPasteboard:(NSPasteboard *)aPboard toRow:(NSInteger)aRow copying:(BOOL)copyingFlag
{
	if(!aPboard)
		return NO;
	NSString*	theType = [aPboard availableTypeFromArray:[self readablePboardTypes]];
	if(!theType)
		return NO;

	if(aRow == NSNotFound)	// aRow == NSNotFound ie no selection -> add object
		aRow = [[self arrangedObjects] count];

#if TARGET_VERSION == 1060
	NSDictionary*	theOptions = [NSDictionary dictionary];
	NSArray*	theItems = [aPboard readObjectsForClasses:[NSArray arrayWithObject:[NSPasteboardItem class]] options:theOptions];
	if(!theItems || 0 == [theItems count])
		return NO;

//	NSLog(@"theItems=%@,%ld,%ld",theItems,[aPboard canReadItemWithDataConformingToTypes:[NSArray arrayWithObject:_pboardType]],[aPboard canReadItemWithDataConformingToTypes:[NSArray arrayWithObject:NSPasteboardTypeString]]);
//	for (id theItem in theItems)
//	{
//		NSLog(@"theItem=%@,%@,%@",theItem, [theItem propertyListForType:_pboardType],[theItem stringForType:NSPasteboardTypeString]);
//	}
	if([theType isEqualToString:_plistPboardType])
		return [self fetchItems:theItems toRow:aRow copying:copyingFlag];
	else if([theType isEqualToString:NSStringPboardType])
		// supposed to [theItems count] == 1
		return [self fetchString:[theItems lastObject] toRow:aRow copying:copyingFlag];
#else	// TARGET_VERSION == 1050
	if([theType isEqualToString:_plistPboardType])
	{
		id	theItems = [aPboard propertyListForType:_plistPboardType];
		if (!theItems || ![theItems isKindOfClass:[NSArray class]] || 0 == [theItems count])
			return NO;
		return [self fetchItems:theItems toRow:aRow copying:copyingFlag];
	}
	else if([theType isEqualToString:NSStringPboardType])
	{
		id	theItem = [aPboard stringForType:NSStringPboardType];
		if (!theItem)
			return NO;
		return [self fetchString:theItem toRow:aRow copying:copyingFlag];
	}
#endif
	return NO;
}
#pragma mark -
#pragma mark methods for drag & drop (NSTableDataSource Protocol)
// -----------------------------------------------------------------------------
//	tableView:validateDrop:proposedRow:proposedDropOperation:
// -----------------------------------------------------------------------------
- (NSDragOperation)tableView:(NSTableView*)tableView validateDrop:(id <NSDraggingInfo>)info proposedRow:(NSInteger)row proposedDropOperation:(NSTableViewDropOperation)operation
{
	NSPasteboard*   thePboard = [info draggingPasteboard];	// check thePboard
	if(!thePboard || ![thePboard availableTypeFromArray:[self readablePboardTypes]])
		return NSDragOperationNone;
    
	if(0 == [[self arrangedObjects] count])
		return NSDragOperationNone;

	if(operation == NSTableViewDropAbove)
		return NSDragOperationNone;

	NSDragOperation	theMask = [info draggingSourceOperationMask];
//	NSLog( @"draggingSourceOperationMask = %d(isEqual=%d)", theMask,theMask == NSDragOperationCopy );
	return (NSDragOperationCopy == theMask) ? theMask : NSDragOperationMove;
}
#pragma mark -
#pragma mark methods for cut, copy, patste
// =============================================================================
//	cut:
// =============================================================================
- (void)cut:(id)sender
{
	[self copy:sender];

	for( id theObject in [self selectedObjects])
	{
		[theObject setValue:nil forKey:(NSString*)DGXArrival];
		[theObject setValue:nil forKey:(NSString*)DGXDeparture];
	}
//	NSLog( @"cut:sender = %@ in %@", sender, [self class]);
}

@end
