//
//  DGXViewController.m
//  CoreDia
//
//  Created by 北原 基彦 on 09-05-16.
//  Copyright 2009-10 horazaka.net. All rights reserved.
//

#import "DGXViewController.h"


@implementation DGXViewController
// =============================================================================
//	setter/getter
// =============================================================================
@synthesize managedObjectContext;
#pragma mark -
// =============================================================================
//	dealloc
// =============================================================================
- (void) dealloc
{
	[ managedObjectContext release ], managedObjectContext = nil;

	[ super dealloc ];
}

@end
