//
//  CoreDia_AppDelegate.m
//  CoreDia
//
//  Created by 北原 基彦 on 09-05-16.
//  Copyright 2009-10 horazaka.net. All rights reserved.
//

#import "CoreDia_AppDelegate.h"


@implementation CoreDia_AppDelegate
// =============================================================================
//	awakeFromNib
// =============================================================================
- (void)awakeFromNib
{
	// set color panel's attribute
	NSColorPanel*	thePanel = [NSColorPanel sharedColorPanel];
	[thePanel setShowsAlpha:YES];
	return;
}

@end
