//
//  TrainViewController.h
//  CoreDia
//
//  Created by 北原 基彦 on 09-05-16.
//  Copyright 2009 horazaka.net. All rights reserved.
//

#import "DGXViewController.h"
@class DGXArrayController;


@interface TrainViewController : DGXViewController
{
	IBOutlet DGXArrayController*	trainArrayController;
	IBOutlet DGXArrayController*	tableArrayController;
}

@end
