//
//  DGXViewController.h
//  CoreDia
//
//  Created by 北原 基彦 on 09-05-16.
//  Copyright 2009-10 horazaka.net. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface DGXViewController : NSViewController
{
	NSManagedObjectContext*	managedObjectContext;
}
@property (retain) NSManagedObjectContext*	managedObjectContext;

@end
