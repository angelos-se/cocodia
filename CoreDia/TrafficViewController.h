//
//  TrafficViewController.h
//  CoreDia
//
//  Created by 北原 基彦 on 09-05-16.
//  Copyright 2009-10 horazaka.net. All rights reserved.
//

#import "DGXViewController.h"
@class DGXArrayController;


@interface TrafficViewController : DGXViewController
{
    IBOutlet DGXArrayController*	classificationArrayController;
    IBOutlet DGXArrayController*	stationArrayController;
}

@end
