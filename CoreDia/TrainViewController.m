//
//  TrainViewController.m
//  CoreDia
//
//  Created by 北原 基彦 on 09-05-16.
//  Copyright 2009 horazaka.net. All rights reserved.
//

#import "TrainViewController.h"

#import "DGXArrayController.h"


@implementation TrainViewController
// =============================================================================
//	initWithNibName:bundle:
//		call loadNibNamed: here, so that ArrayControllers in the nib are unarchieved soon
//		ie ArrayControllers' -awakeFromNib: are called soon
//		and it makes sure to register D&D types and register notification
// =============================================================================
- (id)initWithNibName:(NSString*)name bundle:(NSBundle*)bundle
{
	self = [super initWithNibName:name bundle:bundle];
	if (self)
	{
		[NSBundle loadNibNamed:@"TrainViewController" owner:self];
	}
	return self;
}
// =============================================================================
//	awakeFromNib
// =============================================================================
- (void)awakeFromNib
{
	NSTableView*	theTrainTableView = [trainArrayController valueForKey:@"tableView"];
	NSTableView*	theTableTableView = [tableArrayController valueForKey:@"tableView"];
    // register dragged types
	[theTrainTableView registerForDraggedTypes:[trainArrayController readablePboardTypes]];
	[theTableTableView registerForDraggedTypes:[tableArrayController readablePboardTypes]];

	// set dragging mask ie only permit copy and move, dragging destination cannot decide operation
	[theTrainTableView setDraggingSourceOperationMask:(NSDragOperationCopy|NSDragOperationMove) forLocal:YES];
	[theTableTableView setDraggingSourceOperationMask:(NSDragOperationCopy|NSDragOperationMove) forLocal:YES];
	[theTableTableView setDraggingSourceOperationMask:(NSDragOperationGeneric) forLocal:NO];
}

@end
