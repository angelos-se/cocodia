//
//  MyDocument.m
//  CoreDia
//
//  Created by 北原 基彦 on 09-08-09.
//  Copyright 2009-10 horazaka.net . All rights reserved.
//

#import "MyDocument.h"
#import "MyDocument_Accessor.h"

#import "InfoViewController.h"
#import "TrafficViewController.h"
#import "TrainViewController.h"

#import "TrainArrayController.h"
#import "TableArrayController.h"

#import	"DGXKeys.h"
#import "DGXUpdater.h"
#import	"OuDiaConverter.h"
#import	"WinDIAConverter.h"

#import	"CocoDia.h"
#import	"CocoTable.h"


@interface MyDocument (Private)
- (void)displayViewController:(NSViewController*)aViewController;
@end

@implementation MyDocument
// =============================================================================
//	initWithType:
//		initializer for 'New'
// =============================================================================
- (id)initWithType:(NSString *)typeName error:(NSError **)outError
{
	self = [super initWithType:typeName error:outError];
	if( self )
	{
		[self setFileType:@"SQLite"];

		NSManagedObjectContext*	theContext = [self managedObjectContext];
		[NSEntityDescription insertNewObjectForEntityForName:@"Info" inManagedObjectContext:theContext];
	}
	return self;
}
// =============================================================================
//	initWithContentsOfURL:
//		initializer for 'Open'
// =============================================================================
/*
- (id)initWithContentsOfURL:(NSURL *)absoluteURL ofType:(NSString *)typeName error:(NSError **)outError
{
	self = [super initWithContentsOfURL:absoluteURL ofType:typeName error:outError];
	if( self )
	{
	}
	return self;
}
*/
// =============================================================================
//	dealloc
// =============================================================================
- (void) dealloc
{
	[_viewControllers release], _viewControllers = nil;
	[_view release];

	[super dealloc];
}
// =============================================================================
//	willPresentError:
// =============================================================================
- (NSError*)willPresentError:(NSError *)error
{
	if ([error code] == NSValidationMultipleErrorsError)
	{
		NSError*	theError = nil;
		for (theError in [[error userInfo] objectForKey:NSDetailedErrorsKey])
		{
			NSLog(@"error=%@, userInfo=%@",theError,[theError userInfo]);
		}
	}
	else
		NSLog(@"error=%@, userInfo=%@",error,[error userInfo]);
	return [super willPresentError:error];
}
#pragma mark -
// =============================================================================
//	windowNibName
// =============================================================================
- (NSString *)windowNibName 
{
    return @"MyDocument";
}
// =============================================================================
//	windowControllerDidLoadNib:
// =============================================================================
- (void)windowControllerDidLoadNib:(NSWindowController *)windowController 
{
	[super windowControllerDidLoadNib:windowController];

	NSManagedObjectContext*	theContext = [self managedObjectContext];
	// create viewcontrollers
	_viewControllers = [[NSMutableArray array] retain];
	id	theVC = [[InfoViewController alloc] initWithNibName:@"InfoView" bundle:nil];
	[theVC setManagedObjectContext:theContext];
	[_viewControllers addObject:theVC];
	[theVC release];
	theVC = [[TrafficViewController alloc] initWithNibName:@"TrafficView" bundle:nil];
	[theVC setManagedObjectContext:theContext];
	[_viewControllers addObject:theVC];
	[theVC release];
	theVC = [[TrainViewController alloc] initWithNibName:@"TrainView" bundle:nil];
	[theVC setManagedObjectContext:theContext];
	[_viewControllers addObject:theVC];
	[theVC release];

	// store blank view
	_view = [[[windowController window] contentView] retain];

	// first display -> traffic view
	[self displayViewController:[_viewControllers objectAtIndex:1]];
	[entryPUB selectItemAtIndex:1];

	// all undos are cleared
	[theContext processPendingChanges];
	[[theContext undoManager] removeAllActions];
	[self updateChangeCount:NSChangeCleared];
}
#pragma mark -
// =============================================================================
//	readFromURL:ofType:error:
// =============================================================================
- (BOOL)readFromURL:(NSURL *)absoluteURL ofType:(NSString *)typeName error:(NSError **)outError
{
	NSDictionary*	theProperties = nil;
	id	theConverter = nil;
	if ([typeName isEqualToString:@"SQLite"])
	{
		[self setFileType:typeName];
		return [super readFromURL:absoluteURL ofType:typeName error:outError];
	}
	else if ([typeName isEqualToString:@"CocoDiaFile"])
	{
		theConverter = [[[DGXUpdater alloc] init] autorelease];
		theProperties = [theConverter convert:absoluteURL];
	}
	else if ([typeName isEqualToString:@"OuDiaFile"])
	{
		theConverter = [[[OuDiaConverter alloc] init] autorelease];
		theProperties = [theConverter convert:absoluteURL];
	}
	else	// if ([typeName isEqualToString:@"WinDIAFile"])
	{
		theConverter = [[[WinDIAConverter alloc] init] autorelease];
		theProperties = [theConverter convert:absoluteURL];
	}

	// check result
	if( !theProperties )
		return NO;

	// if file's version > app's version -> error
	if([[theProperties valueForKey:(NSString*)DGXVersion] integerValue] > [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"DGXDocumentVersion"] integerValue])
		return NO;

	// create new store in temporary directory
#if TARGET_VERSION == 1060
	NSString*	theFilename = [[[absoluteURL lastPathComponent] stringByDeletingPathExtension] stringByAppendingPathExtension:@"sqlite"];
#else	// TARGET_VERSION == 1050
	NSString*	theFilename = [[[[absoluteURL path] lastPathComponent] stringByDeletingPathExtension] stringByAppendingPathExtension:@"sqlite"];
#endif
	NSString*	thePath = [NSTemporaryDirectory() stringByAppendingPathComponent:theFilename];
	NSURL*	theURL = [NSURL fileURLWithPath:thePath];
	NSFileManager*	theFM = [NSFileManager defaultManager];
	if ([theFM fileExistsAtPath:thePath])
	{
#if TARGET_VERSION == 1060
		BOOL	theResult = [theFM removeItemAtURL:theURL error:NULL];
#else	// TARGET_VERSION == 1050
		BOOL	theResult = [theFM removeItemAtPath:[theURL path] error:NULL];
#endif
		if (!theResult)
			return NO;
	}

//	NSLog(@"theURL=%@",theURL);
	NSManagedObjectContext*	theMOC = [self managedObjectContext];
	NSPersistentStoreCoordinator*	thePSC = [theMOC persistentStoreCoordinator];
	NSError*	theError = nil;
	[thePSC addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:theURL options:nil error:&theError];
//	NSPersistentStore*	theStore = [thePSC addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:theURL options:nil error:&theError];
//	NSLog(@"theStore=%@,error=%@",theStore,theError);
	if (theError)
		return NO;

	// set entities
	[self setInfo:[theProperties objectForKey:DGXInfo]];
	[self setClassification:[theProperties objectForKey:DGXClassification]];
	[self setStation:[theProperties objectForKey:DGXStation]];
	[self setTrainWithDown:[theProperties objectForKey:DGXDownTrain]
		andUp:[theProperties objectForKey:DGXUpTrain]];	// must be after setClassification: and setStation:
	return YES;
}
// =============================================================================
//	writeSafelyToURL:ofType:forSaveOperation:error:
// =============================================================================
- (BOOL)writeSafelyToURL:(NSURL *)absoluteURL ofType:(NSString *)typeName forSaveOperation:(NSSaveOperationType)saveOperation error:(NSError **)outError
{
//	NSLog(@"\nabsoluteURL=%@\nabsoluteOriginalContentsURL=%@\n",absoluteURL,[self fileURL]);
	NSManagedObjectContext*	theMOC = [self managedObjectContext];
	NSPersistentStoreCoordinator*	thePSC = [theMOC persistentStoreCoordinator];
	NSURL*	theFileURL = [self fileURL];
	if (!theFileURL	// 'Save' or 'Save as' 'New' document
	|| NSSQLiteStoreType == [[thePSC persistentStoreForURL:theFileURL] type])	// 'Save' or 'Save as' 'Open'ed SQLite document
	{
		return [super writeSafelyToURL:absoluteURL ofType:typeName forSaveOperation:saveOperation error:outError];
	}
	else	// 'Save as' 'Open'ed other type document with SQLite type
	{
		NSFileManager*	theFM = [NSFileManager defaultManager];
		if ([theFM fileExistsAtPath:[absoluteURL path]])
#if TARGET_VERSION == 1060
			[theFM removeItemAtURL:absoluteURL error:NULL];
		NSString*	theFilename = [[[[self fileURL] lastPathComponent] stringByDeletingPathExtension] stringByAppendingPathExtension:@"sqlite"];
#else	// TARGET_VERSION = 1050
			[theFM removeItemAtPath:[absoluteURL path] error:NULL];
		NSString*	theFilename = [[[[[self fileURL] path] lastPathComponent] stringByDeletingPathExtension] stringByAppendingPathExtension:@"sqlite"];
#endif
		NSURL*	theURL = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:theFilename]];
//		NSLog(@"theURL=%@",theURL);
		NSManagedObjectContext*	theMOC = [self managedObjectContext];
		[theMOC save:outError];
//		NSLog(@"error=%@",*outError);
		NSPersistentStoreCoordinator*	thePSC = [theMOC persistentStoreCoordinator];
		NSPersistentStore*	theStore = [thePSC persistentStoreForURL:theURL];
		theStore = [thePSC migratePersistentStore:theStore toURL:absoluteURL options:nil withType:NSSQLiteStoreType error:outError];
//		NSLog(@"error=%@,theURL=%@",*outError,[thePSC URLForPersistentStore:theStore]);
		return (nil == *outError);
	}
}
#pragma mark -
#pragma mark IBActions
// -----------------------------------------------------------------------------
//	displayViewController:
// -----------------------------------------------------------------------------
- (void)displayViewController:(NSViewController*)aViewController
{
	NSWindow*	theWindow = [[[self windowControllers] lastObject] window];
	BOOL	ended = [theWindow makeFirstResponder:theWindow];
	if(!ended)
	{
		NSBeep();
		return;
	}
	// put the view in the contents view
	NSView	*theView = [aViewController view];

	// compute the new window frame
	NSSize	theCurrentSize = [[theWindow contentView] frame].size;
	NSSize	theNewSize = [theView frame].size;
	CGFloat	theDeltaWidth = theNewSize.width - theCurrentSize.width;
	CGFloat	theDeltaHeight = theNewSize.height - theCurrentSize.height;
	NSRect	theFrame = [theWindow frame];
	theFrame.size.height += theDeltaHeight;
	theFrame.origin.y -= theDeltaHeight;
	theFrame.size.width += theDeltaWidth;
	[theWindow setContentView:_view];	// hide contents with blank view
//	[theWindow setContentView:nil];	// NG Hillegass 3rd ed. pp363
	[theWindow setFrame:theFrame display:YES animate:YES];
	[theWindow setContentView:theView];

	NSString*	theKeyPath = nil;
	switch ([_viewControllers indexOfObject:aViewController])
	{
		case 0:
			theKeyPath = @"lineTF";
			break;
		case 1:
			theKeyPath = @"classificationArrayController.tableView";
			break;
		case 2:
			theKeyPath = @"trainArrayController.tableView";
			break;
		default:
			break;
	}
	[theWindow makeFirstResponder:[aViewController valueForKeyPath:theKeyPath]];
}
// =============================================================================
//	changeViewController:
// =============================================================================
- (IBAction)changeViewController:(id)sender
{
	NSInteger	theIndex = [sender indexOfSelectedItem];
	[self displayViewController:[_viewControllers objectAtIndex:theIndex]];
}
// =============================================================================
//	changeViewControllerByMenu:
// =============================================================================
- (IBAction)changeViewControllerByMenu:(id)sender
{
	NSInteger	theIndex = [sender tag] - 1000;	// tag is 1000 ~ 1002
	[entryPUB selectItemAtIndex:theIndex];
	[self displayViewController:[_viewControllers objectAtIndex:theIndex]];
}
#pragma mark -
// -----------------------------------------------------------------------------
//	viewDocument:withApplication:
// -----------------------------------------------------------------------------
- (IBAction)viewDocument:(id)sender withApplication:(NSString*)application
{
	NSURL*	theURL = [self fileURL];
	// save document first
	if([self isDocumentEdited] && [[self fileType] isEqualToString:@"SQLite"] && [self fileURL])
		[self saveDocument:sender];

	SBApplication*	theApplication = [SBApplication applicationWithBundleIdentifier:application];
	if([theApplication isRunning])
	{
		// check whether the document is now opened or not, if opened then close it first
		NSPredicate*	thePredicate = [NSPredicate predicateWithFormat:@"path == %@", [theURL path]];
		id	theDocument = [[[[(id)theApplication windows] valueForKey:@"document"] filteredArrayUsingPredicate:thePredicate] lastObject];	// check theDocument is nil or not
		[theDocument closeSaving:CocoDiaSavoNo savingIn:theURL];	// if no nil, close it, else close nil-obj
		// directly send 'open:'message result in warning, so use performSelector:withObject:
		[theApplication performSelector:@selector(open:) withObject:theURL];
		[theApplication activate];	// make the window at front most
	}
}
// =============================================================================
//	viewDiagram:
// =============================================================================
- (IBAction)viewDiagram:(id)sender
{
	[self viewDocument:sender withApplication:@"net.horazaka.CocoDia"];
}
// =============================================================================
//	viewTimetable:
// =============================================================================
- (IBAction)viewTimetable:(id)sender
{
	[self viewDocument:sender withApplication:@"net.horazaka.CocoTable"];
}
#pragma mark -
// -----------------------------------------------------------------------------
//	savePanelDidEnd:returnCode:contextInfo:
// -----------------------------------------------------------------------------
- (void)savePanelDidEnd:(NSSavePanel *)sheet returnCode:(int)returnCode contextInfo:(void *)contextInfo
{
	if (returnCode == NSOKButton)
	{
		NSURL*	theURL = [sheet URL];
		NSDictionary*	theDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
			[[NSBundle mainBundle] objectForInfoDictionaryKey:@"DGXDocumentVersion"], DGXVersion, 
			[self classification], DGXClassification, 
			[self station], DGXStation, 
			[self downTrain], DGXDownTrain, 
			[self upTrain], DGXUpTrain, 
			[self info], DGXInfo, nil];
		BOOL	theResult = [theDictionary writeToURL:theURL atomically:YES];
		if (!theResult)
			[self presentError:[NSError errorWithDomain:NSCocoaErrorDomain code:NSFileWriteUnknownError userInfo:nil]];
		return;
	}
	else	// returnCode == NSCancelButton
	{
		// do nothing
	}
}
// =============================================================================
//	export:
// =============================================================================
- (IBAction)export:(id)sender
{
	NSSavePanel*	theSavePanel = [NSSavePanel savePanel];
	[theSavePanel setCanSelectHiddenExtension:YES];
	[theSavePanel setExtensionHidden:NO];
	NSString*	theFilename = [[[self displayName] stringByDeletingPathExtension] stringByAppendingPathExtension:@"cocodia"];
	[theSavePanel beginSheetForDirectory:nil	// !!!: the method is deprecated at 10.6
		file:theFilename 
		modalForWindow:[self windowForSheet] 
		modalDelegate:self didEndSelector:@selector(savePanelDidEnd:returnCode:contextInfo:) 
		contextInfo:nil];
}
#pragma mark -
#pragma mark methods for responder chain
// -----------------------------------------------------------------------------
//	arrayControllerForFirstResponder:
// -----------------------------------------------------------------------------
- (DGXArrayController*)arrayControllerForFirstResponder
{
	return [(id)[[NSApp keyWindow] firstResponder] delegate];
}
// =============================================================================
//	copy:
// =============================================================================
- (void)copy:(id)sender
{
	[[self arrayControllerForFirstResponder] copy:sender];
}
// =============================================================================
//	paste:
// =============================================================================
- (void)paste:(id)sender
{
	[[self arrayControllerForFirstResponder] paste:sender];
}
// =============================================================================
//	cut:
// =============================================================================
- (void)cut:(id)sender
{
	[[self arrayControllerForFirstResponder] cut:sender];
}
#pragma mark -
#pragma mark methods for NSUserInterfaceValidations Protocol
// =============================================================================
//	validateUserInterfaceItem:
// =============================================================================
- (BOOL)validateUserInterfaceItem:(id < NSValidatedUserInterfaceItem >)anItem
{
	BOOL	isSelected = NO;
	id	theObject = [self arrayControllerForFirstResponder];
	if([theObject respondsToSelector:@selector(selectedObjects)])
		isSelected = [[theObject selectedObjects] count];

	SEL	theAction = [anItem action];
	if(theAction == @selector(cut:))
		return ([TableArrayController class] == [theObject class]) && isSelected;
	else if(theAction == @selector(copy:))
		return ([TrainArrayController class] != [theObject class]) && isSelected;
	else if(theAction == @selector(paste:))
		return [theObject respondsToSelector:@selector(canReadObjectsFromPasteboard:)] 
		&& [theObject canReadObjectsFromPasteboard:[NSPasteboard generalPasteboard]];
//		return [theObject canReadObjectsFromPasteboard:[NSPasteboard generalPasteboard]];
	else if(theAction == @selector(changeViewControllerByMenu:))
		return [[entryPUB selectedItem] tag] != [anItem tag];
	else if(theAction == @selector(viewDiagram:) || theAction == @selector(viewTimetable:))
		return ([[self fileType] isEqualToString:@"SQLite"] && [self fileURL]);
	else
		return [super validateUserInterfaceItem:anItem];
}
#pragma mark -
#pragma mark methods for NSWindowDelegate Protocol
// -----------------------------------------------------------------------------
//	windowShouldZoom:toFrame:
// -----------------------------------------------------------------------------
- (BOOL)windowShouldZoom:(NSWindow *)window toFrame:(NSRect)proposedFrame
{
	return ( 0 != [entryPUB indexOfSelectedItem]);
}

@end
