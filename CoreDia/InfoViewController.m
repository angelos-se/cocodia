//
//  InfoViewController.m
//  CoreDia
//
//  Created by 北原 基彦 on 09-05-16.
//  Copyright 2009-10 horazaka.net. All rights reserved.
//

#import "InfoViewController.h"


@implementation InfoViewController
// =============================================================================
//	initWithNibName:bundle:
//		call loadNibNamed: here, so that ArrayControllers in the nib are unarchieved soon
//		ie ArrayControllers' -awakeFromNib: are called soon
//		and it makes sure to register D&D types and register notification
// =============================================================================
- (id)initWithNibName:(NSString*)name bundle:(NSBundle*)bundle
{
	self = [super initWithNibName:name bundle:bundle];
	if (self)
	{
		[NSBundle loadNibNamed:@"InfoViewController" owner:self];
	}
	return self;
}

@end
