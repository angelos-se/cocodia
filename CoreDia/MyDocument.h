//
//  MyDocument.h
//  CoreDia
//
//  Created by 北原 基彦 on 09-08-09.
//  Copyright 2009-10 horazaka.net . All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface MyDocument : NSPersistentDocument
{
	IBOutlet NSPopUpButton*	entryPUB;

	NSMutableArray*	_viewControllers;
	NSView*	_view;
}
- (IBAction)changeViewController:(id)sender;
- (IBAction)changeViewControllerByMenu:(id)sender;
- (IBAction)viewDiagram:(id)sender;
- (IBAction)viewTimetable:(id)sender;
- (IBAction)export:(id)sender;

@end
