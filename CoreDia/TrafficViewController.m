//
//  TrafficViewController.m
//  CoreDia
//
//  Created by 北原 基彦 on 09-05-16.
//  Copyright 2009-10 horazaka.net. All rights reserved.
//

#import "TrafficViewController.h"

#import "DGXKeys.h"
#import "DGXArrayController.h"


@implementation TrafficViewController
// =============================================================================
//	initWithNibName:bundle:
//		call loadNibNamed: here, so that ArrayControllers in the nib are unarchieved soon
//		ie ArrayControllers' -awakeFromNib: are called soon
//		and it makes sure to register D&D types and register notification
// =============================================================================
- (id)initWithNibName:(NSString*)name bundle:(NSBundle*)bundle
{
	self = [super initWithNibName:name bundle:bundle];
	if (self)
	{
		[NSBundle loadNibNamed:@"TrafficViewController" owner:self];
	}
	return self;
}
// =============================================================================
//	awakeFromNib
// =============================================================================
- (void)awakeFromNib
{
//	NSLog(@"%s, moc=%@",__FUNCTION__, [self managedObjectContext]);
	NSTableView*	theClassificationTableView = [classificationArrayController valueForKey:@"tableView"];
	NSTableView*	theStationTableView =[stationArrayController valueForKey:@"tableView"];
//	NSLog(@"theClassificationTableView=%@,theStationTableView=%@",theClassificationTableView,theStationTableView);
    // register dragged types
	[theClassificationTableView registerForDraggedTypes:[classificationArrayController readablePboardTypes]];
	[theStationTableView registerForDraggedTypes:[stationArrayController readablePboardTypes]];

	// set dragging mask ie only permit copy and move, dragging destination cannot decide operation
	[theClassificationTableView setDraggingSourceOperationMask:(NSDragOperationCopy|NSDragOperationMove) forLocal:YES];
	[theStationTableView setDraggingSourceOperationMask:(NSDragOperationCopy|NSDragOperationMove) forLocal:YES];

	// set behavior the image for 'lineImage' cell
	[[[theClassificationTableView tableColumnWithIdentifier:DGXLineImage] dataCell] setImageScaling:NSScaleToFit];
}

@end
