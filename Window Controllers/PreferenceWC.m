//
//  PreferenceWC.m
//  CocoDia
//
//  Created by 北原 基彦 on 06-09-10.
//  Copyright 2006 Motohiko KITAHARA. All rights reserved.
//

#import "PreferenceWC.h"


@implementation PreferenceWC
// =============================================================================
//	windowDidLoad
// =============================================================================
- (void)windowDidLoad
{
	[ preferenceController setAppliesImmediately:NO ];
}
#pragma mark -
// =============================================================================
//	cancelPref:
// =============================================================================
- (IBAction)cancelPref:(id)sender
{
	// discard change
	[ preferenceController revert:sender ];

	[[ self window ] orderOut:sender ];
}
// =============================================================================
//	donePref:
// =============================================================================
- (IBAction)donePref:(id)sender
{
	// save pref
	[ preferenceController save:sender ];

	[[ self window ] orderOut:sender ];
}

@end
