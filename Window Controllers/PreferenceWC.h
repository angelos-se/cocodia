//
//  PreferenceWC.h
//  CocoDia
//
//  Created by 北原 基彦 on 06-09-10.
//  Copyright 2006 Motohiko KITAHARA. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface PreferenceWC : NSWindowController
{
    IBOutlet NSUserDefaultsController*	preferenceController;
}
- (IBAction)cancelPref:(id)sender;
- (IBAction)donePref:(id)sender;
@end
