//
//  FileInfoWC.m
//  CocoDia
//
//  Created by 北原 基彦 on 08-08-30.
//  Copyright 2008 horazaka.net. All rights reserved.
//

#import "FileInfoWC.h"
#import "DGXKeys.h"


NSString*	DGXMainWindowDidChangeNotification = @"DGXMainWindowDidChange";
NSString*	DGXDocumentWindowWillCloseNotification = @"DGXDocumentWindowWillClose";
static CGFloat	gRightMergin = 40.0;
static CGFloat	gUpperMergin = 40.0;

@implementation FileInfoWC
@synthesize displayName = displayName_;
@synthesize line = line_;
@synthesize date = date_;
@synthesize	creator = creator_;
@synthesize comment = comment_;
@synthesize	filePath = filePath_;
@synthesize creationDate = creationDate_;
@synthesize modificationDate = modificationDate_;
@synthesize	managedObjectContext = _managedObjectContext;
// =============================================================================
//	initWithWindowNibName:
// =============================================================================
- (id)initWithWindowNibName:(NSString *)windowNibName
{
	self = [super initWithWindowNibName:windowNibName];
	if (self != nil) {
		NSRect	theVisibleFrame = [[[NSScreen screens] objectAtIndex:0] visibleFrame];
//		NSLog(@"theVisibleFrame=%@",NSStringFromRect(theVisibleFrame));
		CGFloat x, y;
		x = NSMaxX(theVisibleFrame) - NSWidth([[self window] frame]) - gRightMergin;
		y = NSMaxY(theVisibleFrame) - NSHeight([[self window] frame]) - gUpperMergin;
//		NSLog(@"x=%f,y=%f",x,y); 
		[[self window] setFrameOrigin:NSMakePoint(x, y)];
	}
	return self;
}
// =============================================================================
//	dealloc
// =============================================================================
- (void) dealloc
{
	[displayName_ release], displayName_ =nil;
	[line_ release], line_ = nil;
	[date_ release], date_ = nil;
	[creator_ release], creator_ = nil;
	[comment_ release], comment_ = nil;
	[filePath_ release], filePath_ = nil;
	[creationDate_ release], creationDate_ = nil;
	[modificationDate_ release], modificationDate_ = nil;
	[_managedObjectContext release], _managedObjectContext = nil;
	[_fileInfo release], _fileInfo = nil;

	[super dealloc];
}
#pragma mark -
// =============================================================================
//	validDocumentWindows
//		cut out windows without title( they may be rubbish window ) and NSPanels
// =============================================================================
- (NSArray*) validDocumentWindows
{
//	for( id theWindow in [NSApp windows])
//	{
//		NSLog(@"class=%@,title=%@,isKey(Main)=%ld(%ld)",[theWindow class],[theWindow title],[theWindow isKeyWindow],[theWindow isMainWindow]);
//	}
	NSPredicate*	thePredicate = [NSPredicate predicateWithFormat:@"title != '' && class != %@",[NSPanel class]];
	return [[NSApp windows] filteredArrayUsingPredicate:thePredicate];
}
#pragma mark -
// =============================================================================
//	mainWindowDidChange:
// =============================================================================
- (void) mainWindowDidChange:(NSNotification*)aNotification
{
//	NSLog(@"aNotification=%@",aNotification);
	if(![[aNotification name] isEqualTo:DGXMainWindowDidChangeNotification])
		return;
	id	theDocument = [aNotification object];
	if(![theDocument isKindOfClass:[NSDocument class]])	// object != subclass of NSDocument -> return
		return;
	NSManagedObjectContext*	theMOC = [theDocument valueForKey:@"managedObjectContext"];
	NSFetchRequest*	theRequest = [[[NSFetchRequest alloc] init] autorelease];
	[theRequest setEntity:[NSEntityDescription entityForName:@"Info" inManagedObjectContext:theMOC]];
	_fileInfo = [[theMOC executeFetchRequest:theRequest error:NULL] lastObject];
	[self setDisplayName:[theDocument displayName]];
	if(_fileInfo)
	{
		[self setLine:[_fileInfo valueForKey:(NSString*)DGXLine]];
		[self setCreator:[_fileInfo valueForKey:(NSString*)DGXCreator]];
		[self setComment:[_fileInfo valueForKey:(NSString*)DGXComment]];
		[self setDate:[_fileInfo valueForKey:(NSString*)DGXDate]];

		NSString*	thePath = [[theDocument fileURL] path];
		[self setFilePath:thePath];
		NSDictionary*	theAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:thePath error:NULL];
		[self setCreationDate:[theAttributes valueForKey:@"NSFileCreationDate"]];
		[self setModificationDate:[theAttributes valueForKey:@"NSFileModificationDate"]];
	}
}
// =============================================================================
//	documentWindowWillClose:
// =============================================================================
- (void) documentWindowWillClose:(NSNotification*)aNotification
{
//	NSLog(@"aNotification=%@",aNotification);
	if(![[aNotification name] isEqualTo:DGXDocumentWindowWillCloseNotification])
		return;
	id	theDocument = [aNotification object];
	if(![theDocument isKindOfClass:[NSDocument class]])	// object != subclass of NSDocument -> return
		return;
//	NSLog(@"widows=%@",[NSApp windows]);
	NSArray*	theValidWidows = [self validDocumentWindows];
//	NSLog(@"theValidWidows=%@",theValidWidows);
	if( 1 == [theValidWidows count])	// if there is only one Document
		[[self window] orderOut:self];
}

@end
