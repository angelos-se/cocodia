//
//  FileInfoWC.h
//  CocoDia
//
//  Created by 北原 基彦 on 08-08-30.
//  Copyright 2008 horazaka.net. All rights reserved.
//

#import <Cocoa/Cocoa.h>


extern NSString*	DGXMainWindowDidChangeNotification;
extern NSString*	DGXDocumentWindowWillCloseNotification;

@interface FileInfoWC : NSWindowController
{
	NSString*	displayName_;
	NSString*	line_;
	NSDate*	date_;
	NSString*	creator_;
	NSString*	comment_;
	NSString*	filePath_;
	NSDate*	creationDate_;
	NSDate*	modificationDate_;

	NSManagedObjectContext*	_managedObjectContext;

	id	_fileInfo;
}
@property (nonatomic, retain) NSString*	displayName;
@property (nonatomic, retain) NSString*	line;
@property (nonatomic, retain) NSDate*	date;
@property (nonatomic, retain) NSString*	creator;
@property (nonatomic, retain) NSString*	comment;
@property (nonatomic, retain) NSString*	filePath;
@property (nonatomic, retain) NSDate*	creationDate;
@property (nonatomic, retain) NSDate*	modificationDate;
@property (nonatomic, retain) NSManagedObjectContext*	managedObjectContext;

- (NSArray*) validDocumentWindows;

// methods invoked with notification
- (void) mainWindowDidChange:(NSNotification*)aNotification;
- (void) documentWindowWillClose:(NSNotification*)aNotification;

@end
