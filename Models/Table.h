//
//  Table.h
//  CoreDia
//
//  Created by 北原 基彦 on 09-09-21.
//  Copyright 2009-10 horazaka.net. All rights reserved.
//

#import <CoreData/CoreData.h>

@class Station;
@class Train;

//@interface Table :  NSManagedObject  
@interface Table :  NSManagedObject
{
}

@property (nonatomic, retain) NSString * arrival;
@property (nonatomic, retain) NSString * departure;
@property (nonatomic, retain) Train * train;
@property (nonatomic, retain) Station * station;

+ (NSArray*)copykeys;
- (NSString*)urlRepresentation;
- (id)dictionaryRepresentation;

@end



