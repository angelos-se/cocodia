// 
//  Index.m
//  CoreDia
//
//  Created by 北原 基彦 on 09-09-21.
//  Copyright 2009-10 horazaka.net. All rights reserved.
//

#import "Index.h"


@implementation Index 
// =============================================================================
//	getter/setter
// =============================================================================
@dynamic index;
#pragma mark -
// =============================================================================
//	copykeys
// =============================================================================
+ (NSArray*)copykeys
{
	return nil;
}
// =============================================================================
//	urlRepresentation
// =============================================================================
- (NSString*)urlRepresentation
{
	return [[[self objectID] URIRepresentation] absoluteString];
}
// =============================================================================
//	dictionaryRepresentation
//		do not use dictionaryWithValuesForKeys: as it transform nil to NSNull
//		and that causes not to write object to pasteboard
// =============================================================================
- (id)dictionaryRepresentation
{
	NSMutableDictionary*	theRep = [NSMutableDictionary dictionary];
	for (id theKey in [[self class] copykeys])
	{
		[theRep setValue:[self valueForKey:theKey] forKey:theKey];
	}
	return [NSDictionary dictionaryWithDictionary:theRep];
//	return [self dictionaryWithValuesForKeys:[[self class] copykeys]];
}

@end
