//
//  Train.h
//  CoreDia
//
//  Created by 北原 基彦 on 09-09-21.
//  Copyright 2009-10 horazaka.net. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "Index.h"

@class Classification;

@interface Train :  Index  <NSCopying>
{
}

@property (nonatomic, retain) NSNumber * number;
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) NSString * nickname;
@property (nonatomic, retain) NSNumber * visible;
@property (nonatomic, retain) NSNumber * direction;
@property (nonatomic, retain) NSString * trainID;
@property (nonatomic, retain) NSSet* tables;
@property (nonatomic, retain) Classification * classification;

@end


@interface Train (CoreDataGeneratedAccessors)
- (void)addTablesObject:(NSManagedObject *)value;
- (void)removeTablesObject:(NSManagedObject *)value;
- (void)addTables:(NSSet *)value;
- (void)removeTables:(NSSet *)value;

@end

