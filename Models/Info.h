//
//  Info.h
//  CoreDia
//
//  Created by 北原 基彦 on 09-09-22.
//  Copyright 2009-10 horazaka.net. All rights reserved.
//

#import <CoreData/CoreData.h>


@interface Info :  NSManagedObject  
{
}

@property (nonatomic, retain) NSString * line;
@property (nonatomic, retain) NSString * comment;
@property (nonatomic, retain) NSString * creator;
@property (nonatomic, retain) NSDate * date;

- (id)dictionaryRepresentation;

@end



