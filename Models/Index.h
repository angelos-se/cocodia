//
//  Index.h
//  CoreDia
//
//  Created by 北原 基彦 on 09-09-21.
//  Copyright 2009-10 horazaka.net. All rights reserved.
//

#import <CoreData/CoreData.h>


@interface Index :  NSManagedObject  
{
}

@property (nonatomic, retain) NSNumber * index;

+ (NSArray*)copykeys;
- (NSString*)urlRepresentation;
- (id)dictionaryRepresentation;

@end



