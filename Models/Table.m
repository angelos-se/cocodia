// 
//  Table.m
//  CoreDia
//
//  Created by 北原 基彦 on 09-09-21.
//  Copyright 2009-10 horazaka.net. All rights reserved.
//

#import "Table.h"
#import "DGXKeys.h"
#import "DGXUtilities.h"

#import "Station.h"

@implementation Table
// =============================================================================
//	copykeys
// =============================================================================
+ (NSArray*)copykeys
{
	static NSArray*	copykeys = nil;
	if( !copykeys )
	{
		copykeys = [[NSArray alloc] initWithObjects:(NSString*)DGXArrival, (NSString*)DGXDeparture, nil];
	}
	return copykeys;
}
#pragma mark -
// =============================================================================
//	getter/setter
// =============================================================================
@dynamic arrival;
@dynamic departure;
@dynamic train;
@dynamic station;
// =============================================================================
//	validateValue:forKey:error:
// =============================================================================
- (BOOL)validateValue:(id *)ioValue forKey:(NSString *)key error:(NSError **)error
{
	if (![key isEqualToString:(NSString*)DGXArrival] && ![key isEqualToString:(NSString*)DGXDeparture])
		return [super validateValue:ioValue forKey:key error:error];

	if (nil == *ioValue)
		return YES;
//	NSAttributeDescription*	theDescription = [[[self entity] propertiesByName] valueForKey:key];
//	NSPredicate*	thePredicate = [[theDescription validationPredicates] lastObject];
//	BOOL	theResult = (ishhmm([(NSString*)*ioValue integerValue]) && [thePredicate evaluateWithObject:*ioValue]);
//	return theResult;
	// first check ioValue's format with length and regular expression
	BOOL	theResult = [super validateValue:ioValue forKey:key error:error];
	if (!theResult)
		return theResult;

	// second check if ioValue is legal time 
	theResult = ishhmm([(NSString*)*ioValue integerValue]);
	if (!theResult)
	{
		// similar error message as system shows -- "%{PROPERTY}@" is invalid.
		NSString*	theFormat = NSLocalizedString(@"\"%@\" is invalid.", @"\"%@\" is invalid.");
		NSString*	theKey = NSLocalizedString(key, key);
		*error = [NSError errorWithDomain:NSCocoaErrorDomain code:NSManagedObjectValidationError userInfo:[NSDictionary dictionaryWithObject:[NSString stringWithFormat:theFormat,theKey] forKey:NSLocalizedDescriptionKey]];
	}
	return theResult;
}
#pragma mark -
// =============================================================================
//	urlRepresentation
// =============================================================================
- (NSString*)urlRepresentation
{
	return [[[self objectID] URIRepresentation] absoluteString];
}
// =============================================================================
//	dictionaryRepresentation
//		do not use dictionaryWithValuesForKeys: as it transform nil to NSNull
//		and that causes not to write object to pasteboard
// =============================================================================
- (id)dictionaryRepresentation
{
	NSMutableDictionary*	theRep = [NSMutableDictionary dictionary];
	for (id theKey in [[self class] copykeys])
	{
		[theRep setValue:[self valueForKey:theKey] forKey:theKey];
	}
	return [NSDictionary dictionaryWithDictionary:theRep];
//	return [self dictionaryWithValuesForKeys:[[self class] copykeys]];
}

@end
