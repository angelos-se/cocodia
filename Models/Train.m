// 
//  Train.m
//  CoreDia
//
//  Created by 北原 基彦 on 09-09-21.
//  Copyright 2009-10 horazaka.net. All rights reserved.
//

#import "Train.h"
#import "DGXKeys.h"

#import "Classification.h"

@implementation Train 
// =============================================================================
//	copykeys
// =============================================================================
+ (NSArray*)copykeys
 {
	static NSArray*	copykeys = nil;
	if( !copykeys )
	{
		// !!!: not include @"direction", as no necessary in translate ManagedObject <-> plist 
		copykeys = [[NSArray alloc] initWithObjects:(NSString*)DGXTrainID, (NSString*)DGXVisible, (NSString*)DGXNickname, (NSString*)DGXNumber, (NSString*)DGXNote, nil];
	}
	return copykeys;
 }
// =============================================================================
//	copyWithZone:
// =============================================================================
- (id)copyWithZone:(NSZone*)zone
{
    id  copy = [[[self class] alloc] initWithEntity:[self entity] insertIntoManagedObjectContext:[self managedObjectContext]];
	// copy attributes
	[copy setValue:[self valueForKey:(NSString*)DGXTrainID] forKey:(NSString*)DGXTrainID];
	[copy setValue:[self valueForKey:(NSString*)DGXVisible] forKey:(NSString*)DGXVisible];
	[copy setValue:[self valueForKey:(NSString*)DGXDirection] forKey:(NSString*)DGXDirection];
	[copy setValue:[self valueForKey:(NSString*)DGXNickname] forKey:(NSString*)DGXNickname];
	[copy setValue:[self valueForKey:(NSString*)DGXNumber] forKey:(NSString*)DGXNumber];
	[copy setValue:[self valueForKey:(NSString*)DGXNote] forKey:(NSString*)DGXNote];
	// copy to-one relationship
	[copy setValue:[self valueForKey:(NSString*)DGXClassification] forKey:(NSString*)DGXClassification];

	// copy to-many relationship
	NSSet*	theSet = [NSSet setWithSet:[self valueForKey:(NSString*)DGXTables]];
	NSMutableSet*	theNewSet = [NSMutableSet set];
	for(id theObject in theSet)
	{
		id	theNewObject = [[[theObject class] alloc ] initWithEntity:[theObject entity] insertIntoManagedObjectContext:[theObject managedObjectContext]];
		// copy attributes
		[theNewObject setValue:[theObject valueForKey:(NSString*)DGXArrival] forKey:(NSString*)DGXArrival];
		[theNewObject setValue:[theObject valueForKey:(NSString*)DGXDeparture] forKey:(NSString*)DGXDeparture];
		// copy to-one relatiionship
		[theNewObject setValue:[theObject valueForKey:(NSString*)DGXStation] forKey:(NSString*)DGXStation];
		[theNewSet addObject:theNewObject];
	}
	[copy setValue:theNewSet forKey:(NSString*)DGXTables];
    
    return copy;
}
#pragma mark -
// =============================================================================
//	awakeFromInsert
// =============================================================================
- (void)awakeFromInsert
{
	[super awakeFromInsert];

	[self setValue:@"" forKey:(NSString*)DGXTrainID];
}
// =============================================================================
//	getter/setter
// =============================================================================
@dynamic number;
@dynamic note;
@dynamic nickname;
@dynamic visible;
@dynamic direction;
@dynamic trainID;
@dynamic tables;
@dynamic classification;
#pragma mark -
// =============================================================================
//	validateValue:forKey:error:
// =============================================================================
- (BOOL)validateValue:(id *)ioValue forKey:(NSString *)key error:(NSError **)error
{
	if([key isEqualToString:@"trainID"])
	{
		if (nil == *ioValue)
		{
			*ioValue = @"";
			return YES;
		}
	}
	return [super validateValue:ioValue forKey:key error:error];
}
#pragma mark -
// =============================================================================
//	dictionaryRepresentation
// =============================================================================
- (id)dictionaryRepresentation
{
	NSSortDescriptor*	theDescriptor = [[NSSortDescriptor alloc] initWithKey:@"station.index" ascending:YES];
	NSArray*	theDescriptors = [NSArray arrayWithObject:theDescriptor];
    [theDescriptor release];
	NSArray*	theTables = [[[self mutableSetValueForKey:(NSString*)DGXTables] allObjects] sortedArrayUsingDescriptors:theDescriptors];
	NSMutableArray*	theNewTables = [NSMutableArray array];
	for (id theTable in theTables)
	{
		NSString*	theNewTable = nil;
		NSString*	theArrival = [theTable valueForKey:(NSString*)DGXArrival];
		NSString*	theDeparture = [theTable valueForKey:(NSString*)DGXDeparture];
		if (theArrival)
			theNewTable = [NSString stringWithFormat:@"%@/%@", theArrival, (theDeparture) ? theDeparture : @""];
		else
			theNewTable = (theDeparture) ? theDeparture : @"";
		[theNewTables addObject:theNewTable];
	}
	NSDictionary*	theNewObject = [NSDictionary dictionaryWithObjectsAndKeys:
		[self valueForKey:(NSString*)DGXTrainID], (NSString*)DGXTrainID, 
		[self valueForKey:(NSString*)DGXVisible], (NSString*)DGXVisible, 
		[[self valueForKeyPath:@"classification.index"] stringValue], (NSString*)DGXClassificationID, 
		theNewTables, (NSString*)DGXTimetable, 
		[self valueForKey:(NSString*)DGXNickname], (NSString*)DGXNickname, 
		[self valueForKey:(NSString*)DGXNumber], (NSString*)DGXNumber, 
		[self valueForKey:(NSString*)DGXNote], (NSString*)DGXNote, nil];
	return theNewObject;
}

@end
