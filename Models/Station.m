// 
//  Station.m
//  CoreDia
//
//  Created by 北原 基彦 on 09-09-21.
//  Copyright 2009-10 horazaka.net. All rights reserved.
//

#import "Station.h"
#import "DGXKeys.h"


@implementation Station 
// =============================================================================
//	copykeys
// =============================================================================
+ (NSArray*)copykeys
{
	static NSArray*	copykeys = nil;
	if( !copykeys )
	{
		copykeys = [[NSArray alloc] initWithObjects:(NSString*)DGXName, (NSString*)DGXState, (NSString*)DGXDistance, nil];
	}
	return copykeys;
}
#pragma mark -
// =============================================================================
//	awakeFromInsert
// =============================================================================
- (void)awakeFromInsert
{
	[super awakeFromInsert];

	[self setValue:@"" forKey:(NSString*)DGXName];
}
// =============================================================================
//	getter/setter
// =============================================================================
@dynamic state;
@dynamic name;
@dynamic distance;
@dynamic tables;
#pragma mark -
// =============================================================================
//	validateValue:forKey:error:
// =============================================================================
- (BOOL)validateValue:(id *)ioValue forKey:(NSString *)key error:(NSError **)error
{
	if([key isEqualToString:(NSString*)DGXName])
	{
		if (nil == *ioValue)
		{
			*ioValue = @"";
			return YES;
		}
	}
	return [super validateValue:ioValue forKey:key error:error];
}

@end
