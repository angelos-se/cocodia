//
//  Classification.h
//  CoreDia
//
//  Created by 北原 基彦 on 09-09-21.
//  Copyright 2009-10 horazaka.net. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "Index.h"


@interface Classification :  Index  
{
}

@property (nonatomic, retain) NSString * longName;
@property (nonatomic, retain) NSString * lineColor;
@property (nonatomic, retain) NSNumber * lineStyle;
@property (nonatomic, retain) NSString * shortName;
@property (nonatomic, retain) NSNumber * lineThickness;
@property (nonatomic, retain, readonly) NSImage* lineImage;
@property (nonatomic, retain) NSSet* trains;

@end


@interface Classification (CoreDataGeneratedAccessors)
- (void)addTrainsObject:(NSManagedObject *)value;
- (void)removeTrainsObject:(NSManagedObject *)value;
- (void)addTrains:(NSSet *)value;
- (void)removeTrains:(NSSet *)value;

@end

