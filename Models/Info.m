// 
//  Info.m
//  CoreDia
//
//  Created by 北原 基彦 on 09-09-22.
//  Copyright 2009-10 horazaka.net. All rights reserved.
//

#import "Info.h"
#import "DGXKeys.h"


@implementation Info 
// =============================================================================
//	awakeFromInsert
// =============================================================================
- (void)awakeFromInsert
{
	[super awakeFromInsert];

	id	theObject = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"DGXDocumentVersion"];
	[self setValue:[NSNumber numberWithUnsignedInteger:[theObject intValue]] forKey:@"version"];
	[self setValue:NSFullUserName() forKey:@"creator"];
	[self setValue:[NSDate date] forKey:@"date"];
}
// =============================================================================
//	getter/setter
// =============================================================================
@dynamic line;
@dynamic comment;
@dynamic creator;
@dynamic date;
// =============================================================================
//	dictionaryRepresentation
//		do not use dictionaryWithValuesForKeys: as it transform nil to NSNull
//		and that causes not to write object to pasteboard
// =============================================================================
- (id)dictionaryRepresentation
{
	NSDictionary*	theResult = [NSDictionary dictionaryWithObjectsAndKeys:
		[self valueForKey:(NSString*)DGXLine], DGXLine, 
		[self valueForKey:(NSString*)DGXDate], DGXDate, 
		[self valueForKey:(NSString*)DGXCreator], DGXCreator, 
		[self valueForKey:(NSString*)DGXComment], DGXComment, nil];
	return theResult;
}

@end
