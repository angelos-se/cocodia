// 
//  Classification.m
//  CoreDia
//
//  Created by 北原 基彦 on 09-09-21.
//  Copyright 2009-10 horazaka.net. All rights reserved.
//

#import "Classification.h"
#import "NSColor_Category.h"

#import "DGXKeys.h"
#import "DGXUtilities.h"


const CGFloat	DGXLineImageCellWidth = 150.0;
const CGFloat	DGXLineImageCellHeight = 19.0;

@implementation Classification 
// =============================================================================
//	keyPathsForValuesAffectingValueForKey:
// =============================================================================
+ (NSSet *)keyPathsForValuesAffectingValueForKey:(NSString *)key
{
	if([key isEqualToString:(NSString*)DGXLineImage])
	{
		return [NSSet setWithObjects:(NSString*)DGXLineColor, (NSString*)DGXLineStyle, (NSString*)DGXLineThickness, nil];
	}
	return nil;
}
// =============================================================================
//	copykeys
// =============================================================================
+ (NSArray*)copykeys
{
	static NSArray*	copykeys = nil;
	if( !copykeys )
	{
		copykeys = [[NSArray alloc] initWithObjects:(NSString*)DGXLongName, (NSString*)DGXShortName, (NSString*)DGXLineColor, (NSString*)DGXLineStyle, (NSString*)DGXLineThickness, nil];
	}
	return copykeys;
}
#pragma mark -
// =============================================================================
//	awakeFromInsert
// =============================================================================
- (void)awakeFromInsert
{
	[super awakeFromInsert];

	[self setValue:@"" forKey:(NSString*)DGXLongName];
	[self setValue:@"" forKey:(NSString*)DGXShortName];
}
// =============================================================================
//	getter/setter
// =============================================================================
@dynamic longName;
@dynamic lineColor;
@dynamic lineStyle;
@dynamic shortName;
@dynamic lineThickness;
@dynamic lineImage;
@dynamic trains;
// =============================================================================
//	lineImage
// =============================================================================
- (NSImage*)lineImage
{
	NSImage*	theImage = [[[NSImage alloc] initWithSize:NSMakeSize(DGXLineImageCellWidth, DGXLineImageCellHeight)] autorelease];
	NSColor*	theColor = [NSColor colorFromString:[self lineColor]];

	[theImage lockFocus];
	[[NSColor clearColor] set];
	NSRectFill(NSMakeRect(0.0, 0.0, DGXLineImageCellWidth, DGXLineImageCellHeight));
	[theColor set];
	[NSBezierPath setDefaultLineWidth:[[self lineThickness] boolValue] ? 2.0 : 1.0 ];
	NSBezierPath*	thePath = [NSBezierPath bezierPath];
	[thePath moveToPoint:NSMakePoint(10.0, DGXLineImageCellHeight/2)];
	[thePath lineToPoint:NSMakePoint(DGXLineImageCellWidth-10.0, DGXLineImageCellHeight/2)];
	switch ([[self lineStyle] integerValue])
	{
		case 0:	break;
		case 1:[thePath setLineDash:dash1 count:2 phase:0.0];	break;
		case 2:[thePath setLineDash:dash2 count:2 phase:0.0];	break;
		case 3:[thePath setLineDash:dash3 count:4 phase:0.0];	break;
		default:	break;
	}
	[thePath stroke];
	[theImage unlockFocus];

	return theImage;
}
#pragma mark -
// =============================================================================
//	validateValue:forKey:error:
// =============================================================================
- (BOOL)validateValue:(id *)ioValue forKey:(NSString *)key error:(NSError **)error
{
	if([key isEqualToString:(NSString*)DGXLongName] || [key isEqualToString:(NSString*)DGXShortName])
	{
		if (nil == *ioValue)
		{
			*ioValue = @"";
			return YES;
		}
	}
	return [super validateValue:ioValue forKey:key error:error];
}

@end
