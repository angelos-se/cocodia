//
//  Station.h
//  CoreDia
//
//  Created by 北原 基彦 on 09-09-21.
//  Copyright 2009-10 horazaka.net. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "Index.h"


@interface Station :  Index  
{
}

@property (nonatomic, retain) NSNumber * state;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * distance;
@property (nonatomic, retain) NSSet* tables;

@end


@interface Station (CoreDataGeneratedAccessors)
- (void)addTablesObject:(NSManagedObject *)value;
- (void)removeTablesObject:(NSManagedObject *)value;
- (void)addTables:(NSSet *)value;
- (void)removeTables:(NSSet *)value;

@end

