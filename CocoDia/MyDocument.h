//
//  MyDocument.h
//  CocoDia
//
//  Created by KITAHARA Motohiko on 07-11-25.
//  Copyright horazaka.net 2007-10 . All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class	DiagramView;
@class	Runtime;


@interface MyDocument : NSPersistentDocument
{
    IBOutlet DiagramView *diagramView;
    IBOutlet NSScrollView *scrollView;

    IBOutlet NSPopUpButton *directionPUB;
    IBOutlet NSPopUpButton *trainIDPUB;
	IBOutlet NSPopUpButton *heightPUB;

	NSInteger	_smoothness;	// 0...visit all timetables, 1...skip timetable for minor stations
	NSInteger	_stationGrid;	// 0...station grid created from runtime, 1...from distance

	Runtime*	_runtime;
	NSArray*	_yPositions;
	NSArray*	_downTrainLines;
	NSArray*	_upTrainLines;
}

- (IBAction)selectDirectionByMenu:(id)sender;
- (IBAction)selectTrainIDStateByMenu:(id)sender;
- (IBAction)selectHeightByMenu:(id)sender;
- (IBAction)selectDirection:(id)sender;
- (IBAction)selectTrainIDState:(id)sender;
- (IBAction)selectHeight:(id)sender;

@end
