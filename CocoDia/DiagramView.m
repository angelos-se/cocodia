//
//  DiagramView.m
//  CocoDia
//
//  Created by KITAHARA Motohiko on 06-05-13.
//  Copyright 2006-10 horazaka.net. Distributed under GPL.
//

#import "DiagramView.h"
#import "DGXKeys.h"
#import "DGXUtilities.h"
#import	"Runtime.h"
#import "TrainLine.h"
#import "NSColor_Category.h"

// width and height
static const CGFloat	DGXStationWidth = 200.0;
static const CGFloat	DGXHourWidth = 240.0;
static const CGFloat	DGXWidth = 5760.0;
static const CGFloat	DGXMinHeight = 400.0;
static const CGFloat	DGXMaxHeight = 1600.0;
static const CGFloat	DGXAllowance = 200.0;
static const CGFloat	DGXCharacterHeight = 20.0;

@interface DiagramView( privateMethods )
- (void)drawStationGrid;
- (void)drawTimeGrid;
- (void)drawStationLine:(NSSize)aSize;
- (void)drawTrainLine:(NSArray*)trainlines byDirection:(BOOL)aDirection;
- (void)drawTrainID:(NSArray*)trainlines byDirection:(BOOL)aDirection;
@end

@implementation DiagramView
@synthesize height = _height;
@synthesize	runtime = _runtime;
// =============================================================================
//	initWithFrame:
// =============================================================================
- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if( self )
	{
		_direction = 0;
		_trainIDOn = YES;

		_stationImage = [[NSImage alloc] initWithSize:NSMakeSize( DGXStationWidth, DGXMaxHeight )];

		NSSize theSize = NSMakeSize( DGXWidth, DGXMaxHeight );
		_timetableImage = [[NSImage alloc] initWithSize:theSize];
		_downTrainLineImage = [[NSImage alloc] initWithSize:theSize];
		_upTrainLineImage = [[NSImage alloc] initWithSize:theSize];
		_downTrainIDImage = [[NSImage alloc] initWithSize:theSize];
		_upTrainIDImage = [[NSImage alloc] initWithSize:theSize];
    }
    return self;
}
// =============================================================================
//	dealloc
// =============================================================================
- (void)dealloc
{
	if( _yPositions )
		free( _yPositions );
	[_runtime release], _runtime = nil;

	[_stationImage release], _stationImage = nil;
	[_timetableImage release], _timetableImage = nil;
	[_downTrainLineImage release],  _downTrainLineImage= nil;
	[_upTrainLineImage release], _upTrainLineImage = nil;
	[_downTrainIDImage release],  _downTrainIDImage= nil;
	[_upTrainIDImage release], _upTrainIDImage = nil;

	[super dealloc];
}
// =============================================================================
//	drawDiagramDown:andUp:withRuntime:height:
//		here _station is instanciated
// =============================================================================
//#define	LOG
- (void)drawDiagramDown:(NSArray*)downTrainLines andUp:(NSArray*)upTrainLines withRuntime:(Runtime*)runtime height:(DGXHeightState)height
{
#if defined( LOG )
	NSTimeInterval	t0 = [NSDate timeIntervalSinceReferenceDate];
#endif

	[self setRuntime:runtime];
	[self setHeight:height];

	NSInteger	theTotal = [runtime totalRuntime];
	CGFloat	p;	// scale for y-axis
	switch( height )
	{
		case DGXHeightMinimum:	// minimum height
			p = ( DGXMinHeight - DGXAllowance ) / (CGFloat)theTotal;
			break;
		case DGXHeightMedium:	// medium height
			p = ((( DGXMinHeight + DGXMaxHeight ) / 2 ) - DGXAllowance ) / (CGFloat)theTotal;
			break;
		case DGXHeightMaximum:	// maximum height
			p = ( DGXMaxHeight - DGXAllowance ) / (CGFloat)theTotal;
			break;
		case DGXHeightAuto:	// auto height
		default:
			if( DGXMaxHeight - DGXAllowance < theTotal * 4 )
				p = ( DGXMaxHeight - DGXAllowance ) / (CGFloat)theTotal;
			else if( theTotal * 4 < DGXMinHeight - DGXAllowance )
				p = ( DGXMinHeight - DGXAllowance ) / (CGFloat)theTotal;
			else
				p = 4.0;
			break;
	}

	CGFloat	q = DGXMaxHeight - DGXAllowance / 2;
	NSArray*	theStationGrid = [runtime stationGrid];
	NSUInteger i, j = [theStationGrid count];
	if( _yPositions )
		free( _yPositions );
	if( NULL == ( _yPositions = malloc( sizeof(CGFloat) * j )))
		return;
	for( i = 0; i < j; i++ )
	{
#ifdef __LP64__
		q -= (CGFloat)[[theStationGrid objectAtIndex:i] doubleValue] * p;
#else
		q -= (CGFloat)[[theStationGrid objectAtIndex:i] floatValue] * p;
#endif
		_yPositions[i] = q;
	}

	[self drawStationGrid];
	[self drawTimeGrid];
	[self drawTrainLine:downTrainLines byDirection:YES];	// draw lines on downTrainImage
	[self drawTrainLine:upTrainLines byDirection:NO];	// draw lines on upTrainImage
	[self drawTrainID:downTrainLines byDirection:YES];	// draw trainIDs on downTrainIDImage
	[self drawTrainID:upTrainLines byDirection:NO];	// draw trainIDs on upTrainIDImage

#if defined( LOG )
	NSLog( @"total = %f", [NSDate timeIntervalSinceReferenceDate] - t0 );
#endif
}
#pragma mark -
// -----------------------------------------------------------------------------
//	drawStationLine:
//		invoked from drawStationGrid and drawTimeGrid:
// -----------------------------------------------------------------------------
- (void)drawStationLine:(NSSize)aSize
{
	[[NSColor clearColor] set];
	NSRectFill( NSMakeRect( 0.0, 0.0, aSize.width, aSize.height ));
	[[NSColor lightGrayColor] set];
	NSArray*	theStations = [[self runtime] stations];
	NSUInteger i, count = [theStations count];
	for( i = 0; i < count; i++ )
	{
		id theStation = [theStations objectAtIndex:i];
		[NSBezierPath setDefaultLineWidth:(( 2 == [[theStation valueForKey:(NSString*)DGXState] integerValue]) ? 2.0 : 1.0 )];
		CGFloat	p = _yPositions[i];
		[NSBezierPath strokeLineFromPoint:NSMakePoint( 0.0, p ) toPoint:NSMakePoint( aSize.width, p )];
	}
}
#pragma mark -
// -----------------------------------------------------------------------------
//	drawStationGrid
// -----------------------------------------------------------------------------
static const CGFloat	DGXXAtDistance = 50.0;
static const CGFloat	DGXXAtName = 100.0;
static const CGFloat	DGXIntervalWidth = 46.0;
static const CGFloat	DGXDistanceWidth = 46.0;
static const CGFloat	DGXNameWidth = 92.0;
- (void)drawStationGrid
{
	NSArray*	theStations = [[self runtime] stations];

	CGFloat	theTopLine = _yPositions[0];
	CGFloat	theBottomLine = _yPositions[[theStations count] - 1];

	[_stationImage lockFocus];
	// draw horizontal lines
	[self drawStationLine:[_stationImage size]];
	// draw vertical lines
	[[NSColor lightGrayColor] set];
	[NSBezierPath setDefaultLineWidth:1.0];
	[NSBezierPath strokeLineFromPoint:NSMakePoint( DGXXAtDistance, theTopLine ) toPoint:NSMakePoint( DGXXAtDistance, theBottomLine )];
	[NSBezierPath strokeLineFromPoint:NSMakePoint( DGXXAtName, theTopLine ) toPoint:NSMakePoint( DGXXAtName, theBottomLine )];
	// draw station interval, distance, name
	static NSDictionary *info = nil;
	if (nil == info)
	{
		NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
		[style setLineBreakMode:NSLineBreakByTruncatingMiddle];
		[style setAlignment:NSRightTextAlignment];
		info = [[NSDictionary alloc] initWithObjectsAndKeys:style, NSParagraphStyleAttributeName, nil];
		[style release];
	}
	NSArray*	theIntervals = [[self runtime] intervals];
	NSUInteger i, count = [theStations count];
	for (i = 0; i < count; i++)
	{
		id theStation = [theStations objectAtIndex:i];
		// set up strings( name, interval, distance )
		NSAttributedString*	theName = [[[NSAttributedString alloc] initWithString:[theStation valueForKey:(NSString*)DGXName] attributes:info] autorelease];
		NSString*	theDistanceString = [NSString stringWithFormat:@"%4.1f", [[theStation valueForKey:(NSString*)DGXDistance] doubleValue]];
		NSAttributedString*	theDistance = [[[NSAttributedString alloc] initWithString:theDistanceString attributes:info] autorelease];
		NSString*	theIntervalString = ( 0 == i ) ? @"" : [NSString stringWithFormat:@"%4.1f", [[theIntervals objectAtIndex:i] doubleValue]];
		NSAttributedString*	theInterval = [[[NSAttributedString alloc] initWithString:theIntervalString attributes:info] autorelease];

		// draw strings
		CGFloat	y = _yPositions[i] - 5.0;
		[theName drawInRect:NSMakeRect( DGXXAtName + 4.0, y, DGXNameWidth, DGXCharacterHeight )];
		[theDistance drawInRect:NSMakeRect( DGXXAtDistance, y, DGXDistanceWidth, DGXCharacterHeight )];
		[theInterval drawInRect:NSMakeRect( 0.0, y, DGXIntervalWidth, DGXCharacterHeight )];
	}
	[_stationImage unlockFocus];
}
// -----------------------------------------------------------------------------
//	drawTimeGrid
// -----------------------------------------------------------------------------
static const CGFloat	DGXYOffsetForHour = 40.0;
- (void)drawTimeGrid
{
	NSArray*	theStations = [[self runtime] stations];
	CGFloat	theTopLine = _yPositions[0];
	CGFloat	theBottomLine = _yPositions[[theStations count] - 1];

	[_timetableImage lockFocus];
	NSSize	theSize = [_timetableImage size];
	[self drawStationLine:theSize];
	NSInteger	i, j, k = theSize.width;
	[[NSColor lightGrayColor] set];
	for( i = 0, j = 0; i <= k; i += 8, j++ )
	{
		if( j % 15 == 0 )
			[NSBezierPath setDefaultLineWidth:2.0];
		else if( j % 5 == 0 )
			[NSBezierPath setDefaultLineWidth:1.0];
		else
			[NSBezierPath setDefaultLineWidth:0.5];
		[NSBezierPath strokeLineFromPoint:NSMakePoint( i, theTopLine ) toPoint:NSMakePoint( i, theBottomLine )];
	}
	// draw hour
	CGFloat	theYUpper = theTopLine + DGXYOffsetForHour;
	CGFloat	theYLower = theBottomLine - DGXCharacterHeight - DGXYOffsetForHour;
	for( i = 0; i <= 24; i ++ )
	{
		NSAttributedString*	theHour = [[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%2d", i]] autorelease];
		CGFloat	theXOffset, theWidth = [theHour size].width;
		switch( i )
		{
			default:
				theXOffset = -( theWidth / 2 );
				break;
			case 0:
				theXOffset = theWidth;
				break;
			case 24:
				theXOffset = -theWidth;
				break;
		}
		[theHour drawInRect:NSMakeRect( i * DGXHourWidth + theXOffset, theYUpper, theWidth, DGXCharacterHeight )];
		[theHour drawInRect:NSMakeRect( i * DGXHourWidth + theXOffset, theYLower, theWidth, DGXCharacterHeight )];
	}
	[_timetableImage unlockFocus];
}
// -----------------------------------------------------------------------------
//	drawTrainLine:byDirection:
// -----------------------------------------------------------------------------
- (void)drawTrainLine:(NSArray*)trainLines byDirection:(BOOL)aDirection
{
	NSImage*	theImage = ( aDirection ) ? _downTrainLineImage : _upTrainLineImage;
	NSSize	theSize = [theImage size];
	
	[theImage lockFocus];
	// fill rect transprent color
	[[NSColor clearColor] set];
	NSRectFill( NSMakeRect( 0, 0, theSize.width, theSize.height ));

	for( id theTrainLine in trainLines )
	{
		// set line color/style/thickness
		NSInteger	theLineStyle = [[theTrainLine objectForKey:DGXLineStyle] integerValue];
		NSInteger	theLineThickness = [[theTrainLine objectForKey:DGXLineThickness] integerValue];
		NSColor*	theLineColor = [NSColor colorFromString:[theTrainLine objectForKey:DGXLineColor]];
		[theLineColor set];
		NSBezierPath*	theLine = [NSBezierPath bezierPath];
		[theLine setLineWidth:( theLineThickness ) ? 2.0 : 1.0];
		switch ( theLineStyle ) {
			case 0:
				break;
			case 1:
				[theLine setLineDash:dash1 count:2 phase:0.0];
				break;
			case 2:
				[theLine setLineDash:dash2 count:2 phase:0.0];
				break;
			case 3:
				[theLine setLineDash:dash3 count:4 phase:0.0];
				break;
			default:
				break;
		}
		for( id theTimeData in [theTrainLine objectForKey:DGXLineData])
		{
			DGXLineDatum	theLineDatum;
			[theTimeData getValue:&theLineDatum];
			NSInteger	x1 = theLineDatum.dept;
			NSInteger	x2 = theLineDatum.arrt;
			CGFloat	y1 = _yPositions[theLineDatum.deps];
			CGFloat	y2 = _yPositions[theLineDatum.arrs];
			[theLine moveToPoint:NSMakePoint( time2x( x1 ), y1 )];
			[theLine lineToPoint:NSMakePoint( time2x( x2 ), y2 )];
			[theLine stroke];
			[theLine removeAllPoints];
		}
	}
	[theImage unlockFocus];
}
// -----------------------------------------------------------------------------
//	drawTrainID:byDirection:
//		if rotation angle <> 45, cos and sin must be set instead use M_PI_4, M_SQRT1_2
// -----------------------------------------------------------------------------
- (void)drawTrainID:(NSArray*)trainLines byDirection:(BOOL)aDirection
{
	NSImage*	theImage = ( aDirection ) ? _downTrainIDImage : _upTrainIDImage;
	NSSize	theSize = [theImage size];
	
	[theImage lockFocus];
	// fill rect transprent color
	[[NSColor clearColor] set];
	NSRectFill( NSMakeRect( 0, 0, theSize.width, theSize.height ));

	// rotate axis -45 or +45
	NSAffineTransform*	theXform = [NSAffineTransform transform];
	[theXform rotateByRadians:M_PI_4 * (( aDirection ) ? -1 : 1 )];	// M_PI_4...math.h
	[theXform concat];

	for( id theTrainLine in trainLines )
	{
		// create attr str from trainID and lineColor
		NSColor*	theLineColor = [NSColor colorFromString:[theTrainLine objectForKey:DGXLineColor]];
		NSAttributedString*	theString = [[[NSAttributedString alloc] initWithString:[theTrainLine objectForKey:@"trainID"] attributes:[NSDictionary dictionaryWithObject:theLineColor forKey:NSForegroundColorAttributeName]] autorelease];
		theSize = [theString size];

		NSArray*	theLineData = [theTrainLine objectForKey:DGXLineData];
		DGXLineDatum	theLineDatum;
		[[theLineData objectAtIndex:0] getValue:&theLineDatum];	//	item count has checked at createTrainLine: of MyDocument

		NSInteger	x = time2x( theLineDatum.dept );
		CGFloat	y = _yPositions[theLineDatum.deps];
		NSRect	theRect;
		if( aDirection )
		{
//			theRect = NSMakeRect( x * cos - y * sin - theSize.width, x * sin + y * cos, theSize.width, theSize.height );
			theRect = NSMakeRect(( x - y ) * M_SQRT1_2 - theSize.width, ( x + y ) * M_SQRT1_2, theSize.width, theSize.height );
		}
		else
		{
//			theRect = NSMakeRect( x * cos + y * sin - theSize.width, - ( x * sin ) + y * cos, theSize.width, theSize.height );
			theRect = NSMakeRect(( x + y ) * M_SQRT1_2 - theSize.width, ( -x + y ) * M_SQRT1_2 - theSize.height, theSize.width, theSize.height );
		}
		[theString drawInRect:theRect];
	}
	[theXform invert];	// invert axis
	[theImage unlockFocus];
}
#pragma mark -
// =============================================================================
//	setDirection:	// 0..down/up, 1..down only, 2..up only
// =============================================================================
- (void)setDirection:(NSInteger)aDirection
{
	_direction = aDirection;
}
// =============================================================================
//	setTrainIDOn:
// =============================================================================
- (void)setTrainIDOn:(BOOL)yn
{
	_trainIDOn = yn;
}
#pragma mark -
// =============================================================================
//	drawRect:
// =============================================================================
- (void)drawRect:(NSRect)rect
{
	NSRect	theVisibleRect = [self visibleRect];
	NSSize	theStationSize = [_stationImage size];
	NSSize	theTimetableSize = [_timetableImage size];

//	NSRect	theBounds = [self bounds];
//	NSLog( @"bound = %@", NSStringFromRect(theBounds));
//	NSRect	theFrame = [self frame];
//	NSLog( @"frame = %@", NSStringFromRect(theFrame));
//	NSLog( @"rect = %@", NSStringFromRect(rect));
//	NSLog( @"visibleRect = %@", NSStringFromRect(theVisibleRect));

	CGFloat	xOrigin;	// station grid in screen -> move, in printer -> not move
	if([NSGraphicsContext currentContextDrawingToScreen])
	{
		NSEraseRect( rect );
		[self setNeedsDisplayInRect:theVisibleRect];
		xOrigin = theVisibleRect.origin.x;
	}
	else
	{
		xOrigin = 0.0;
	}
	// compose station grid
	[_stationImage drawAtPoint:NSMakePoint( xOrigin, 0.0 )
		fromRect:NSMakeRect( 0, 0, theStationSize.width, theStationSize.height )
		operation:NSCompositeSourceOver
		fraction:1.0];

	NSPoint	theTimetablePoint = NSMakePoint( xOrigin + theStationSize.width, 0.0 );
	NSRect	theSourceRect = NSMakeRect( xOrigin, 0, theTimetableSize.width, theTimetableSize.height );
	// compose time grid
	[_timetableImage drawAtPoint:theTimetablePoint
		fromRect:theSourceRect
		operation:NSCompositeSourceOver
		fraction:1.0];
	if( 2 != _direction )
	{
		// compose down train
		[_downTrainLineImage drawAtPoint:theTimetablePoint
			fromRect:theSourceRect
			operation:NSCompositeSourceOver
			fraction:1.0];
		if( _trainIDOn )
			// compose down trainID
			[_downTrainIDImage drawAtPoint:theTimetablePoint
				fromRect:theSourceRect
				operation:NSCompositeSourceOver
				fraction:1.0];
	}
	if( 1 != _direction )
	{
		// compose up train
		[_upTrainLineImage drawAtPoint:theTimetablePoint
			fromRect:theSourceRect
			operation:NSCompositeSourceOver
			fraction:1.0];
		if( _trainIDOn )
			// compose up trainID
			[_upTrainIDImage drawAtPoint:theTimetablePoint
				fromRect:theSourceRect
				operation:NSCompositeSourceOver
				fraction:1.0];
	}
}
// =============================================================================
//	adjustPageWidthNew:left:right:limit
// =============================================================================
- (void)adjustPageWidthNew:(CGFloat *)newRight left:(CGFloat)left right:(CGFloat)proposedRight limit:(CGFloat)rightLimit
{
	[super adjustPageWidthNew:newRight
		left:left
		right:proposedRight
		limit:rightLimit];
//	NSLog( @"*newRight = %f, proposedRight = %f", *newRight, proposedRight );
	CGFloat	theWidth = [_stationImage size].width;
	NSInteger	n = DGXHourWidth * 10;
	*newRight = ((NSInteger)(( proposedRight - theWidth ) / n )) * n + theWidth;
}

@end
