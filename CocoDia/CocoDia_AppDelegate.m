//
//  CocoDia_AppDelegate.m
//  CocoDia
//
//  Created by KITAHARA Motohiko on 07-11-25.
//  Copyright 2007-10 horazaka.net. All rights reserved.
//

#import "CocoDia_AppDelegate.h"
#import "FileInfoWC.h"
#import "PreferenceWC.h"


@implementation CocoDia_AppDelegate
// =============================================================================
//	init
// =============================================================================
- (id) init
{
	self = [super init];
	if (self != nil)
	{
		// create info panel
		_fileInfoWC = [[FileInfoWC alloc] initWithWindowNibName:@"FileInfo"];
		// create pref window
		_preferenceWC = [[PreferenceWC alloc] initWithWindowNibName:@"Preference"];
	}
	return self;
}
// =============================================================================
//	dealloc
// =============================================================================
- (void) dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:_fileInfoWC];

	[_fileInfoWC release], _fileInfoWC = nil;
	[_preferenceWC release], _preferenceWC = nil;

	[super dealloc];
}
// =============================================================================
//	awakeFromNib
// =============================================================================
- (void)awakeFromNib
{
	// register notifications
	[[NSNotificationCenter defaultCenter] addObserver:_fileInfoWC 
		selector:@selector(mainWindowDidChange:) 
//		name:NSWindowDidBecomeKeyNotification 
		name:DGXMainWindowDidChangeNotification 
		object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:_fileInfoWC 
		selector:@selector(documentWindowWillClose:) 
//		name:NSWindowWillCloseNotification 
		name:DGXDocumentWindowWillCloseNotification 
		object:nil];
	// info panel hide at first, create and show panel in -init and orderOut: here, the panel can have value
	[[_fileInfoWC window] orderOut:self];
	// pref window hide at first
	[[_preferenceWC window] orderOut:self];
}
// =============================================================================
//	applicationShouldOpenUntitledFile:
// =============================================================================
- (BOOL)applicationShouldOpenUntitledFile:(NSApplication *)sender
{
	return NO;
}
#pragma mark -
// =============================================================================
//	showInfoPanel:
// =============================================================================
- (IBAction)showInfoPanel:(id)sender
{
	NSWindow*	thePanel = [_fileInfoWC window];
	[thePanel performSelector:([thePanel isVisible] ? @selector(orderOut:) : @selector(orderFront:)) withObject:sender];
}
// =============================================================================
//	showPreferencePanel:
// =============================================================================
- (IBAction)showPreferencePanel:(id)sender
{
	[_preferenceWC showWindow:sender];
}
#pragma mark -
// =============================================================================
//	validateMenuItem:
// =============================================================================
- (BOOL)validateMenuItem:(NSMenuItem*)menuItem
{
	SEL	theAction = [menuItem action];
	if( theAction == @selector(showInfoPanel:))
	{
		NSWindow*	thePanel = [_fileInfoWC window];
		[menuItem setTitle:([thePanel isVisible]) ? NSLocalizedString(@"Hide Info", @"Hide Info") : NSLocalizedString(@"Show Info", @"Show Info")];
		return ([[_fileInfoWC validDocumentWindows] count] > 0 );	// if valid Document, menu valid
	}
	else if( theAction == @selector( showPreferencePanel: ))
		return YES;
	else
		return [super validateMenuItem:menuItem];
}

@end
