//
//  Runtime.h
//  CocoDia
//
//  Created by KITAHARA Motohiko on 06-06-11.
//  Copyright 2006-10 KITAHARA Motohiko. Distributed under GPL.
//

#import <Cocoa/Cocoa.h>


extern const NSInteger	DGXStationGridID;

@interface Runtime : NSObject
{
@private
	NSArray*	stations_;
	NSArray*	downTrains_;
	NSArray*	upTrains_;
	BOOL	smoothness_;
	BOOL	grid_;	// NO = with runtime(default) YES = with intervals

	NSArray*	intervals_;
	NSMutableDictionary*	minRuntime_;

	NSArray*	stationGrid_;
	NSInteger	totalRuntime_;
}
@property (nonatomic, retain) NSArray*	stations;
@property (nonatomic, retain, readonly) NSArray*	intervals;

- (id)initWithStations:(NSArray*)stations downTrains:(NSArray*)downTrains upTrains:(NSArray*)upTrains smoothness:(BOOL)smoothness grid:(BOOL)grid;
- (NSInteger)minRuntimeFrom:(NSInteger)start to:(NSInteger)end withClassificationID:(unsigned)index;
- (NSInteger)totalRuntime;
- (NSArray*)stationGrid;

@end
