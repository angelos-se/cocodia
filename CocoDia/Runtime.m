//
//  Runtime.m
//  CocoDia
//
//  Created by KITAHARA Motohiko on 06-06-11.
//  Copyright 2006-10 KITAHARA Motohiko. Distributed under GPL.
//

#import "Runtime.h"
#import	"DGXKeys.h"
#import	"DGXUtilities.h"


const NSInteger	DGXStationGridID = 255;
static NSString*	DGXRuntimeFormat = @"%03X%03X%02X";
static const NSInteger	DGXMaxRuntime = 1439;	// 60 * 24 - 1

@interface Runtime( privateMethods )
- (NSString*)makeKeyFrom:(NSInteger)start to:(NSInteger)end withClassificationID:(NSInteger)index;
- (void)setMinRuntime:(NSInteger)aRuntime from:(NSInteger)start to:(NSInteger)end withClassificationID:(unsigned)index;
- (void)setRuntime:(NSArray*)trains byDirection:(BOOL)aDirection smoothness:(NSInteger)smoothness;
- (void)setStationGrid;
- (void)setStationGrid:(NSArray*)trains byDirection:(BOOL)aDirection;
@end

@implementation Runtime
@synthesize stations = stations_;
@synthesize	intervals = intervals_;
#pragma mark -
// =============================================================================
//	initWithStations:downTrains:upTrains:smoothness:gridWith:
// =============================================================================
- (id)initWithStations:(NSArray*)stations downTrains:(NSArray*)downTrains upTrains:(NSArray*)upTrains smoothness:(BOOL)smoothness grid:(BOOL)grid
{
	self = [super init];
	if (!self || !stations || !downTrains || !upTrains)
		return nil;

	stations_ = [[NSArray arrayWithArray:stations] retain];
	downTrains_ = [[NSArray arrayWithArray:downTrains] retain];
	upTrains_ = [[NSArray arrayWithArray:upTrains] retain];
	smoothness_ = smoothness;
	grid_ = grid;
	minRuntime_ = [[NSMutableDictionary dictionary] retain];
	stationGrid_ = nil;	// lazy initialization

	// create intervals_
	NSMutableArray*	theIntervals = [NSMutableArray array];
	id	theLastStation = [stations objectAtIndex:0];
	for( id theStation in stations )
	{
#ifdef __LP64__
		CGFloat	theInterval = [[theStation valueForKey:(NSString*)DGXDistance] doubleValue]
			- [[theLastStation valueForKey:(NSString*)DGXDistance] doubleValue];
		[theIntervals addObject:[NSNumber numberWithDouble:theInterval]];
#else
		CGFloat	theInterval = [[theStation valueForKey:(NSString*)DGXDistance] floatValue]
			- [[theLastStation valueForKey:(NSString*)DGXDistance] floatValue];
		[theIntervals addObject:[NSNumber numberWithFloat:theInterval]];
#endif
		theLastStation = theStation;
	}
	intervals_ = [[NSArray arrayWithArray:theIntervals] retain];

	// create minRuntime
	[self setRuntime:downTrains_ byDirection:YES smoothness:smoothness_];
	[self setRuntime:upTrains_ byDirection:NO smoothness:smoothness_];
	return self;
}
// =============================================================================
//	dealloc
// =============================================================================
- (void)dealloc
{
	[stations_ release], stations_ = nil;
	[downTrains_ release], downTrains_ = nil;
	[upTrains_ release], upTrains_ = nil;
	[minRuntime_ release], minRuntime_ = nil;
	[intervals_ release], intervals_ = nil;
	[stationGrid_ release], stationGrid_ = nil;

	[super dealloc];
}
#pragma mark -
#pragma mark private methods
// -----------------------------------------------------------------------------
//	makeKeyFrom:to:withClassificationID:
// -----------------------------------------------------------------------------
- (NSString*)makeKeyFrom:(NSInteger)start to:(NSInteger)end withClassificationID:(NSInteger)index
{
//	NSLog (@"theKey = %03X%03X%02X", start, end, index);
	return [NSString stringWithFormat:DGXRuntimeFormat, start, end, index];
}
// -----------------------------------------------------------------------------
//	setMinRuntime:from:to:withClassificationID:
// -----------------------------------------------------------------------------
- (void)setMinRuntime:(NSInteger)aRuntime from:(NSInteger)start to:(NSInteger)end withClassificationID:(unsigned)index
{
	if( aRuntime < 0 )	// irregular data -> do nothing
		return;
	NSString*	theKey = [self makeKeyFrom:start to:end withClassificationID:index];
	id	theValue = [minRuntime_ valueForKey:theKey];
	if( !theValue || aRuntime < [theValue integerValue])
		[minRuntime_ setObject:[NSNumber numberWithInteger:aRuntime] forKey:theKey];
}
#pragma mark private method for initialization
// -----------------------------------------------------------------------------
//	setRuntime:byDirection:smoothness:
// -----------------------------------------------------------------------------
- (void)setRuntime:(NSArray*)trains byDirection:(BOOL)aDirection smoothness:(NSInteger)smoothness
{
	for( id theTrain in trains )
	{
		NSSortDescriptor*	theDescriptor = [[NSSortDescriptor alloc] initWithKey:@"station.index" ascending:aDirection];
		NSArray*	theTimetable = [[[theTrain valueForKey:@"tables"] allObjects] sortedArrayUsingDescriptors:[NSArray arrayWithObject:theDescriptor]];
		[theDescriptor release];

		NSUInteger i, j, count = [theTimetable count];
		for (i = 0; i < count - 1; i++)
		{
			id	theFrom = [theTimetable objectAtIndex:i];
			NSInteger	theArr, theDep;
			id	theArrValue = [theFrom valueForKey:@"arrival"];
			id	theDepValue = [theFrom valueForKey:@"departure"];
			if ((!theDepValue && !theArrValue) 
			|| ([theDepValue isEqualToString:@"-"] || [theArrValue isEqualToString:@"-"]))
				continue;
			theDep = hhmm2min((theDepValue) ? [theDepValue integerValue] : [theArrValue integerValue]);
			for (j = i + 1; j < count; j++)
			{
				id	theTo = [theTimetable objectAtIndex:j];
				theArrValue = [theTo valueForKey:@"arrival"];
				theDepValue = [theTo valueForKey:@"departure"];
				id	theStateValue = [theTo valueForKeyPath:@"station.state"];
				if ((!theDepValue && !theArrValue) 
				|| ([theDepValue isEqualToString:@"-"] || [theArrValue isEqualToString:@"-"])
				|| (smoothness && 0 == [theStateValue integerValue]))
					continue;
				theArr = hhmm2min((theArrValue) ? [theArrValue integerValue]: [theDepValue integerValue]);
				NSInteger	theClassificationID = [[theTrain valueForKeyPath:@"classification.index"] integerValue];
				[self setMinRuntime:submin(theArr, theDep) 
					from:[[theFrom valueForKeyPath:@"station.index"] integerValue] 
					to:[[theTo valueForKeyPath:@"station.index"] integerValue]
					withClassificationID:theClassificationID];
				break;
			}
			i = j - 1;
		}
	}
}
#pragma mark private methods for stationGrid_
// -----------------------------------------------------------------------------
//	setStationGrid:byDirection:
// -----------------------------------------------------------------------------
- (void)setStationGrid:(NSArray*)trains byDirection:(BOOL)aDirection
{
	for( id theTrain in trains )
	{
		NSInteger	theClassificationID = [[theTrain valueForKeyPath:@"classification.index"] integerValue];
		if(0 != theClassificationID)
			continue;
		NSSortDescriptor*	theDescriptor = [[NSSortDescriptor alloc] initWithKey:@"station.index" ascending:aDirection];
		NSArray*	theTimetable = [[[theTrain valueForKey:@"tables"] allObjects] sortedArrayUsingDescriptors:[NSArray arrayWithObject:theDescriptor]];
		[theDescriptor release];

		NSUInteger i, j, count = [theTimetable count];
		for (i = 0; i < count - 1; i++)
		{
			id	theFrom = [theTimetable objectAtIndex:i];
			NSInteger	theArr, theDep;
			id	theArrValue = [theFrom valueForKey:@"arrival"];
			id	theDepValue = [theFrom valueForKey:@"departure"];
			if ((!theDepValue && !theArrValue) 
			|| ([theDepValue isEqualToString:@"-"] || [theArrValue isEqualToString:@"-"]))
				continue;
			theDep = hhmm2min((theDepValue) ? [theDepValue integerValue] : [theArrValue integerValue]);
			for (j = i + 1; j < count; j++)
			{
				id	theTo = [theTimetable objectAtIndex:j];
				theArrValue = [theTo valueForKey:@"arrival"];
				theDepValue = [theTo valueForKey:@"departure"];
				if ((!theDepValue && !theArrValue) 
				|| ([theDepValue isEqualToString:@"-"] || [theArrValue isEqualToString:@"-"]))
					continue;
				theArr = hhmm2min((theArrValue) ? [theArrValue integerValue]: [theDepValue integerValue]);
				[self setMinRuntime:submin(theArr, theDep) 
					from:[[theFrom valueForKeyPath:@"station.index"] integerValue] 
					to:[[theTo valueForKeyPath:@"station.index"] integerValue]
					withClassificationID:DGXStationGridID];
				break;
			}
			i = j - 1;
		}
	}
}
// -----------------------------------------------------------------------------
//	setStationGrid
// -----------------------------------------------------------------------------
- (void)setStationGrid
{
	NSInteger	i, j, k = [stations_ count];
	CGFloat	theDistance;
	NSMutableIndexSet*	theIndexes = [NSMutableIndexSet indexSet];
	for( i = 1; i < k; i++ )
	{
#ifdef __LP64__
		theDistance = [[[stations_ objectAtIndex:i] valueForKey:(NSString*)DGXDistance] doubleValue];
#else
		theDistance = [[[stations_ objectAtIndex:i] valueForKey:(NSString*)DGXDistance] floatValue];
#endif
		if( 0 == theDistance )
			[theIndexes addIndex:i];
	}
	for( i = 0, j = 1; i < k - 1; i++, j++ )
	{
		if(( 0 == i && [theIndexes containsIndex:j])
		|| ( 0 != i && ([theIndexes containsIndex:i] || [theIndexes containsIndex:j])))
			continue;
#ifdef __LP64__
		theDistance = [[intervals_ objectAtIndex:j] doubleValue];
#else
		theDistance = [[intervals_ objectAtIndex:j] floatValue];
#endif
//		NSLog(@"theDistance=%f",theDistance);
		[self setMinRuntime:(NSInteger)theDistance from:i to:j withClassificationID:DGXStationGridID];
	}
//	NSLog( @"stationGrid = %@", [_runtime stationGrid]);
}
#pragma mark -
#pragma mark public methods
// =============================================================================
//	totalRuntime
// =============================================================================
- (NSInteger)totalRuntime
{
	if (totalRuntime_)
		return totalRuntime_;

	// if 0 == totalRuntime then create and return
	for( id loopItem in [self stationGrid])
		totalRuntime_ += [loopItem integerValue];
	return totalRuntime_;
}
// =============================================================================
//	stationGrid
// =============================================================================
- (NSArray*)stationGrid
{
	if (stationGrid_)
		return stationGrid_;

	// if nil == stationGrid_ then create and return
	if (grid_)
		[self setStationGrid];
	else
	{
		[self setStationGrid:downTrains_ byDirection:YES];
		[self setStationGrid:upTrains_ byDirection:NO];
	}
	NSMutableArray*	theArray = [NSMutableArray arrayWithObject:[NSNumber numberWithInteger:0]];
	NSInteger	i, j, k = [stations_ count] - 1;
	NSInteger	theTotal = 0, theNumber = 0, theAverage;
	id	theValue;
	// get average from available data
	for( i = 0, j = 1; i < k; i++, j++ )
	{
		theValue = [minRuntime_ valueForKey:[self makeKeyFrom:i to:j withClassificationID:DGXStationGridID]];
		if( theValue )
		{
			theTotal += [theValue integerValue];
			theNumber++;
		}
	}
	theAverage = ( 0 == theNumber ) ? 1 : ( theTotal / theNumber );
	for( i = 0, j = 1; i < k; i++, j++ )
	{
		theValue = [minRuntime_ valueForKey:[self makeKeyFrom:i to:j withClassificationID:DGXStationGridID]];
		// substitute data from upRuntime
		if( !theValue )
			theValue = [minRuntime_ valueForKey:[self makeKeyFrom:j to:i withClassificationID:DGXStationGridID]];
		// set average data for last resort
		if( !theValue )
			theValue = [NSNumber numberWithInteger:theAverage];
		[theArray addObject:theValue];
	}
	stationGrid_ = [[NSArray arrayWithArray:theArray] retain];
	return stationGrid_;
}
// =============================================================================
//	minRuntimeFrom:to:withClassificationID:
// =============================================================================
- (NSInteger)minRuntimeFrom:(NSInteger)start to:(NSInteger)end withClassificationID:(unsigned)index
{
	id	theValue = [minRuntime_ valueForKey:[self makeKeyFrom:start to:end withClassificationID:index]];
	return ( theValue ) ? [theValue integerValue] : DGXMaxRuntime;
}

@end
