//
//  TrainLine.h
//  CocoDia
//
//  Created by KITAHARA Motohiko on 06-05-21.
//  Copyright 2006 KITAHARA Motohiko. Distributed under GPL.
//

#import <Cocoa/Cocoa.h>


extern const NSString*	DGXLineData;

typedef	struct	_DGXLineDatum
{
	NSInteger	deps;
	NSInteger	arrs;
	NSInteger	dept;
	NSInteger	arrt;
}	DGXLineDatum;

/*
// TrainLine class is substituted with NSDictionary class
// and acessesor methods are as such as [ theTrainLine objectForKey : DGX... ];
@interface TrainLine : NSObject
{
	NSString*	trainID;
	NSDictionary*	classification;
	NSArray*	lineData;	// a lineData is NSMutableDictionary, 
							// keys are @"dep/arrStation" -> index for station from/to in the array
							// and @"dep/arrTime" -> departure and arrival time in NSString*
}

- (id)initWithTrainID:(NSString*)aTrainID
	classification:(NSDictionary*)aClassification
	andLineData:(NSArray*)aLineData;

- (NSString*)trainID;
- (NSDictionary*)classification;
- (NSArray*)lineData;

@end
*/
