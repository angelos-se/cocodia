//
//  TrainLine.m
//  CocoDia
//
//  Created by KITAHARA Motohiko on 06-05-21.
//  Copyright 2006 KITAHARA Motohiko. Distributed under GPL.
//

#import "TrainLine.h"


// key for line data
const NSString*	DGXLineData = @"lineData";
/*
@implementation TrainLine
// =============================================================================
//	initWithTrainID:classification:andLineData:
// =============================================================================
- (id)initWithTrainID:(NSString*)aTrainID
	classification:(NSDictionary*)aClassification
	andLineData:(NSArray*)aLineData
{
	self = [ super init ];
	if( self )
	{
		trainID = [[ NSString alloc ] initWithString : aTrainID ];
		classification = [[ NSDictionary alloc ] initWithDictionary : aClassification ];
		lineData = [[ NSArray alloc ] initWithArray : aLineData ];
	}
	return self;
}
// =============================================================================
//	dealloc
// =============================================================================
- (void)dealloc
{
	[ trainID release ], trainID = nil;
	[ classification release ], classification = nil;
	[ lineData release ], lineData = nil;

	[ super dealloc ];
}
#pragma mark -
// =============================================================================
//	trainID
// =============================================================================
- (NSString*)trainID
{
	return trainID;
}
// =============================================================================
//	Classification
// =============================================================================
- (NSDictionary*)classification
{
	return classification;
}
// =============================================================================
//	lineData
// =============================================================================
- (NSArray*)lineData
{
	return lineData;
}

@end
*/
