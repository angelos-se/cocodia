//
//  MyDocument.m
//  CocoDia
//
//  Created by KITAHARA Motohiko on 07-11-25.
//  Copyright horazaka.net 2007-10. All rights reserved.
//

#import "MyDocument.h"
#import "MyDocument_Accessor.h"

#import "DiagramView.h"
#import "Runtime.h"
#import "TrainLine.h"

#import "DGXKeys.h"
#import	"DGXUpdater.h"
#import "DGXUtilities.h"
#import "FileInfoWC.h"
#import	"OuDiaConverter.h"
#import	"WinDIAConverter.h"

static NSInteger	DGXTagHeightBase = 3000;
static NSInteger	DGXTagHeightAuto = 3000;
//static NSInteger	DGXTagHeightMin = 3001;
static NSInteger	DGXTagHeightMed = 3002;
//static NSInteger	DGXTagHeightMax = 3003;


@interface MyDocument( privateMethods )
- (void)drawDiagram:(NSInteger)height;
- (NSArray*)createYPositions:(Runtime*)runtime;
- (NSArray*)createLineData:(NSDictionary*)aTrain byDirection:(BOOL)aDirection;
- (NSArray*)createTrainLines:(NSArray*)trains byDirection:(BOOL)aDirection;
@end

@implementation MyDocument
// =============================================================================
//	dealloc
// =============================================================================
- (void)dealloc
{
	[_runtime release], _runtime = nil;
	[_yPositions release], _yPositions = nil;
	[_downTrainLines release], _downTrainLines = nil;
	[_upTrainLines release], _upTrainLines = nil;

	[super dealloc];
}
// =============================================================================
//	willPresentError:
// =============================================================================
- (NSError*)willPresentError:(NSError *)error
{
	if ([error code] == NSValidationMultipleErrorsError)
	{
		NSArray*	theErrors = [[error userInfo] objectForKey:NSDetailedErrorsKey];
		NSLog(@"errors=%@",theErrors);
	}
	else
		NSLog(@"error=%@",error);
	return [super willPresentError:error];
}
#pragma mark -
// =============================================================================
//	windowNibName
// =============================================================================
- (NSString *)windowNibName 
{
    return @"MyDocument";
}
// -----------------------------------------------------------------------------
//	SHOWTRAINDATA(macro for debug)
// -----------------------------------------------------------------------------
#define	SHOWTRAINDATA( array )	\
do	\
{	\
	for( id theLine in array )	\
	{	\
		NSLog( @"TrainID = %@", [theLine valueForKey:@"trainID"]);	\
		for( id theObj in [theLine valueForKey:@"lineData"])	\
		{	\
			DGXLineDatum	theDatum;	\
			[theObj getValue:&theDatum];	\
			NSLog(@"%@(%ld) ~ %@(%ld) ( %ld ~ %ld )",[[theStations objectAtIndex:theDatum.deps] valueForKey:DGXName],theDatum.deps,[[theStations objectAtIndex:theDatum.arrs] valueForKey:DGXName],theDatum.arrs,theDatum.dept,theDatum.arrt);	\
		}	\
	}	\
} while(0)
// =============================================================================
//	windowControllerDidLoadNib:
// =============================================================================
- (void)windowControllerDidLoadNib:(NSWindowController *)windowController 
{
    [super windowControllerDidLoadNib:windowController];

	// set param for drawing diagram
	id	theValues = [[NSUserDefaultsController sharedUserDefaultsController] values];
	_smoothness = [[theValues valueForKey:@"DGXPriority"] integerValue];
	_stationGrid = [[theValues valueForKey:@"DGXStationGrid"] integerValue];

	// set heightPUB's index (runtime -> auto, interval -> med)
	[heightPUB selectItemWithTag:( _stationGrid ) ? DGXTagHeightMed : DGXTagHeightAuto];

	// fetch necessary data
	NSManagedObjectContext*	theMOC = [self managedObjectContext];
	NSFetchRequest*	theRequest = [[[NSFetchRequest alloc] init] autorelease];
	NSEntityDescription*	theEntity = [NSEntityDescription entityForName:@"Station" inManagedObjectContext:theMOC];
	[theRequest setEntity:theEntity];

	NSSortDescriptor*	theDescriptor = [[NSSortDescriptor alloc] initWithKey:@"index" ascending:YES];
	[theRequest setSortDescriptors:[NSArray arrayWithObject:theDescriptor]];
	[theDescriptor release];

	NSError*	theError = nil;
	NSArray*	theStations = [[self managedObjectContext] executeFetchRequest:theRequest error:&theError];
	if (theError)
		goto FAIL;

	theEntity = [NSEntityDescription entityForName:@"Train" inManagedObjectContext:theMOC];
	[theRequest setEntity:theEntity];

	NSPredicate*	thePredicate = [NSPredicate predicateWithFormat:@"direction != 0"];
	[theRequest setPredicate:thePredicate];
	theError = nil;
	NSArray*	theDownTrains = [theMOC executeFetchRequest:theRequest error:&theError];
	if (theError)
		goto FAIL;

	[theRequest setEntity:theEntity];
	thePredicate = [NSPredicate predicateWithFormat:@"direction == 0"];
	[theRequest setPredicate:thePredicate];
	theError = nil;
	NSArray*	theUpTrains = [theMOC executeFetchRequest:theRequest error:&theError];
	if (theError)
		goto FAIL;

	// set up _runtime
	_runtime = [[Runtime alloc] initWithStations:theStations downTrains:theDownTrains upTrains:theUpTrains smoothness:_smoothness grid:_stationGrid];
//	NSInteger	i, j, k = [theStations count];
//	for( i = 0, j = 1; i < k - 1; i++, j++ )
//		NSLog( @"minRuntime(%d~%d) = %d, %d", i, j, [_runtime minRuntimeFrom:i to:j withClassificationID:0], [_runtime minRuntimeFrom:j to:i withClassificationID:0]);
//	NSLog( @"totalRuntime = %d", [_runtime totalRuntime]);

	// create TrainLine
	_downTrainLines = [[self createTrainLines:theDownTrains byDirection:YES] retain];
	_upTrainLines = [[self createTrainLines:theUpTrains byDirection:NO] retain];
//	SHOWTRAINDATA( _downTrainLines );
//	SHOWTRAINDATA( _upTrainLines );
	// dinamic scroll -> enable
	[scrollView setScrollsDynamically:YES];

	// draw diagram
	[self drawDiagram:[heightPUB selectedTag] - DGXTagHeightBase];
	[diagramView scrollRectToVisible:NSMakeRect( 0, 1599, 1, 1 )];	// scroll to upper-left

	// reset undo
	NSManagedObjectContext*	theContext = [self managedObjectContext];
	[theContext processPendingChanges];
	[[theContext undoManager] removeAllActions];
	[self updateChangeCount:NSChangeCleared];
	return;

FAIL:
	return;
}
#undef SHOWTRAINDATA
#pragma mark -
// =============================================================================
//	readFromURL:ofType:error:
// =============================================================================
- (BOOL)readFromURL:(NSURL *)absoluteURL ofType:(NSString *)typeName error:(NSError **)outError
{
	NSDictionary*	theProperties = nil;
	id	theConverter = nil;
	if ([typeName isEqualToString:@"SQLite"])
	{
		[self setFileType:typeName];
		return [super readFromURL:absoluteURL ofType:typeName error:outError];
	}
	else if ([typeName isEqualToString:@"CocoDiaFile"])
	{
		theConverter = [[[DGXUpdater alloc] init] autorelease];
		theProperties = [theConverter convert:absoluteURL];
	}
	else if ([typeName isEqualToString:@"OuDiaFile"])
	{
		theConverter = [[[OuDiaConverter alloc] init] autorelease];
		theProperties = [theConverter convert:absoluteURL];
	}
	else	// if ([typeName isEqualToString:@"WinDIAFile"])
	{
		theConverter = [[[WinDIAConverter alloc] init] autorelease];
		theProperties = [theConverter convert:absoluteURL];
	}

	// check result
	if( !theProperties )
		return NO;

	// if file's version > app's version -> error
	if([[theProperties valueForKey:(NSString*)DGXVersion] integerValue] > [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"DGXDocumentVersion"] integerValue])
		return NO;

	// create new store in temporary directory
#if TARGET_VERSION == 1060
	NSString*	theFilename = [[[absoluteURL lastPathComponent] stringByDeletingPathExtension] stringByAppendingPathExtension:@"sqlite"];
#else	// TARGET_VERSION == 1050
	NSString*	theFilename = [[[[absoluteURL path] lastPathComponent] stringByDeletingPathExtension] stringByAppendingPathExtension:@"sqlite"];
#endif
	NSString*	thePath = [NSTemporaryDirectory() stringByAppendingPathComponent:theFilename];
	NSURL*	theURL = [NSURL fileURLWithPath:thePath];
	NSFileManager*	theFM = [NSFileManager defaultManager];
	if ([theFM fileExistsAtPath:thePath])
	{
#if TARGET_VERSION == 1060
		BOOL	theResult = [theFM removeItemAtURL:theURL error:NULL];
#else	// TARGET_VERSION == 1050
		BOOL	theResult = [theFM removeItemAtPath:[theURL path] error:NULL];
#endif
		if (!theResult)
			return NO;
	}

//	NSLog(@"theURL=%@",theURL);
	NSManagedObjectContext*	theMOC = [self managedObjectContext];
	NSPersistentStoreCoordinator*	thePSC = [theMOC persistentStoreCoordinator];
	NSError*	theError = nil;
	[thePSC addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:theURL options:nil error:&theError];
//	NSPersistentStore*	theStore = [thePSC addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:theURL options:nil error:&theError];
//	NSLog(@"theStore=%@,error=%@",theStore,theError);
	if (theError)
		return NO;

	// set entities
	[self setInfo:[theProperties objectForKey:DGXInfo]];
	[self setClassification:[theProperties objectForKey:DGXClassification]];
	[self setStation:[theProperties objectForKey:DGXStation]];
	[self setTrainWithDown:[theProperties objectForKey:DGXDownTrain]
		andUp:[theProperties objectForKey:DGXUpTrain]];	// must be after setClassification: and setStation:
	return YES;
}
#pragma mark -
// -----------------------------------------------------------------------------
//	drawDiagram:
// -----------------------------------------------------------------------------
- (void)drawDiagram:(NSInteger)height
{
//	NSLog(@"_downTrainLines=%@",_downTrainLines);
	// draw grids and lines
	[diagramView drawDiagramDown:_downTrainLines andUp:_upTrainLines withRuntime:_runtime  height:height];

	// order view to display
	[diagramView setNeedsDisplay:YES];
}
#pragma mark -
// -----------------------------------------------------------------------------
//	createLineData:byDirection:
// -----------------------------------------------------------------------------
- (NSArray*)createLineData:(NSDictionary*)aTrain byDirection:(BOOL)aDirection
{
	NSMutableArray*	theLineData = [ NSMutableArray array ];
	NSSortDescriptor*	theDescriptor = [[NSSortDescriptor alloc] initWithKey:@"station.index" ascending:aDirection];
	NSArray*	theTables = [[[aTrain valueForKey:(NSString*)DGXTables] allObjects] sortedArrayUsingDescriptors:[NSArray arrayWithObject:theDescriptor]];
	[theDescriptor release];
	NSUInteger i, j, count = [theTables count];
	for (i = 0; i < count - 1; i++)
	{
		id	theFrom = [theTables objectAtIndex:i];
		NSInteger	theArr, theDep, theRuntime;
		DGXLineDatum	theLineDatum;
		id	theArrValue = [theFrom valueForKey:(NSString*)DGXArrival];
		id	theDepValue = [theFrom valueForKey:(NSString*)DGXDeparture];
		if (!theDepValue || [theDepValue isEqualToString:@"-"])
			continue;
		theDep = hhmm2min((theDepValue) ? [theDepValue integerValue] : [theArrValue integerValue]);
		for (j = i + 1; j < count; j++)
		{
			id	theTo = [theTables objectAtIndex:j];
			theArrValue = [theTo valueForKey:(NSString*)DGXArrival];
			theDepValue = [theTo valueForKey:(NSString*)DGXDeparture];
			id	theStateValue = [theTo valueForKeyPath:@"station.state"];
			if ([theDepValue isEqualToString:@"-"]
			|| (!theDepValue && (!theArrValue || [theArrValue isEqualToString:@"-"]))
			|| (_smoothness && 0 == [theStateValue integerValue]))
				continue;
			if ([theArrValue isEqualToString:@"-"] 
			&& (theDepValue && ![theDepValue isEqualToString:@"-"]))	// ex. -/519
			{
				theArr = hhmm2min([theDepValue integerValue]);
//				NSLog(@"theArr=%ld",theArr);
			}
			else	// ex. 518/519, 518/, /519
			{
				theArr = hhmm2min((theArrValue) ? [theArrValue integerValue] : [theDepValue integerValue]);
				theRuntime = [_runtime minRuntimeFrom:[[theFrom valueForKeyPath:@"station.index"] integerValue] 
					to:[[theTo valueForKeyPath:@"station.index"] integerValue]  
					withClassificationID:[[aTrain valueForKeyPath:@"classification.index"] integerValue]];
				if ((theRuntime < submin(theArr, theDep)) 
				&& (!theArrValue || [theArrValue isEqualToString:@"-"]))
				{
					theArr = addmin(theDep, theRuntime);
				}
			}
			theLineDatum.deps = [[theFrom valueForKeyPath:@"station.index"] integerValue];
			theLineDatum.arrs = [[theTo valueForKeyPath:@"station.index"] integerValue];
			if( theDep <= theArr )
			{
				theLineDatum.dept = theDep, theLineDatum.arrt = theArr;
				[ theLineData addObject:[ NSValue value:&theLineDatum withObjCType:@encode( DGXLineDatum )]];
			}
			else	// over midnight
			{
				theLineDatum.dept = theDep, theLineDatum.arrt = theArr + 1440;
				[ theLineData addObject:[ NSValue value:&theLineDatum withObjCType:@encode( DGXLineDatum )]];
				theLineDatum.dept = theDep - 1440, theLineDatum.arrt = theArr;
				[ theLineData addObject:[ NSValue value:&theLineDatum withObjCType:@encode( DGXLineDatum )]];
			}
			break;
		}
		i = j - 1;
	}
	return [ NSArray arrayWithArray:theLineData ];
}
// -----------------------------------------------------------------------------
//	createTrainLine:
// -----------------------------------------------------------------------------
- (NSArray*)createTrainLines:(NSArray*)trains byDirection:(BOOL)aDirection
{
	NSMutableArray*	theTrainLines = [NSMutableArray array];
	for( id theTrain in trains )
	{
		if( ![[theTrain valueForKey:(NSString*)DGXVisible] boolValue])	// if train unnecessary drawn -> skip
			continue;
		NSArray*	theLineData = [self createLineData:theTrain byDirection:aDirection];
//		NSLog( @"theLineData = %@", theLineData );
		if( 0 == [theLineData count])	// if line data are empty -> skip
			continue;

		// create classification
		[theTrainLines addObject:[NSDictionary dictionaryWithObjectsAndKeys: 
			[theTrain valueForKey:(NSString*)DGXTrainID], DGXTrainID, 
			[theTrain valueForKeyPath:@"classification.lineColor"], DGXLineColor, 
			[theTrain valueForKeyPath:@"classification.lineStyle"], DGXLineStyle, 
			[theTrain valueForKeyPath:@"classification.lineThickness"], DGXLineThickness, 
			theLineData, DGXLineData, nil]];
	}
	return [NSArray arrayWithArray:theTrainLines];
}
#pragma mark -
// =============================================================================
//	selectDirectionByMenu:
// =============================================================================
- (IBAction)selectDirectionByMenu:(id)sender
{
	[directionPUB selectItemAtIndex:[sender tag]];	// change PUB compulsory
	[self selectDirection:directionPUB];	// and [self selectDirection]
}
// =============================================================================
//	selectTrainIDStateByMenu:
// =============================================================================
- (IBAction)selectTrainIDStateByMenu:(id)sender
{
	[trainIDPUB selectItemAtIndex:([trainIDPUB indexOfSelectedItem]) ? 0 : 1];	// change PUB compulsory
	[self selectTrainIDState:trainIDPUB];	// and [self selectDirection]
}
// =============================================================================
//	selectHeightByMenu:
// =============================================================================
- (IBAction)selectHeightByMenu:(id)sender
{
	[heightPUB selectItemWithTag:[sender tag]];	// change PUB compulsory
	[self selectHeight:heightPUB];	// and [sef selectHeight]
}
// =============================================================================
//	selectDirection:
// =============================================================================
- (IBAction)selectDirection:(id)sender
{
//	NSLog( @"index = %d", [sender indexOfSelectedItem]);
	[diagramView setDirection:[sender indexOfSelectedItem]];
	[diagramView setNeedsDisplay:YES];
	return;
}
// =============================================================================
//	selectTrainIDState:
// =============================================================================
- (IBAction)selectTrainIDState:(id)sender
{
//	NSLog( @"index = %d", [sender indexOfSelectedItem]);
	[diagramView setTrainIDOn:![sender indexOfSelectedItem]];
	[diagramView setNeedsDisplay:YES];
	return;
}
// =============================================================================
//	selectHeight:
// =============================================================================
- (IBAction)selectHeight:(id)sender
{
//	NSLog(@"currentHeight=%ld,tag=%ld",[[diagramView valueForKey:@"height"] integerValue],[sender selectedTag]);
	NSInteger	theIndex = [sender selectedTag] - DGXTagHeightBase;
	if([diagramView height] != theIndex )
		[self drawDiagram:theIndex];
	return;
}
#pragma mark -
// =============================================================================
//	printOperationWithSettings:error:
//		- (IBAction)printDocument:(id)sender will invoke this method
// =============================================================================
- (NSPrintOperation *)printOperationWithSettings:(NSDictionary *)printSettings error:(NSError **)outError
{
	NSPrintInfo*	thePI = [self printInfo];
	[[thePI dictionary] setObject:[NSNumber numberWithBool:YES] forKey:NSPrintHeaderAndFooter];
	[thePI setOrientation:NSLandscapeOrientation];
	[thePI setHorizontalPagination:NSAutoPagination];
	[thePI setVerticalPagination:NSFitPagination];
	[thePI setVerticallyCentered:NO];

	return [NSPrintOperation printOperationWithView:diagramView printInfo:thePI];
}
#pragma mark -
#pragma mark methods for NSMenuValidation Protocol
// =============================================================================
//	validateMenuItem:
// =============================================================================
- (BOOL)validateMenuItem:(NSMenuItem *)menuItem
{
	SEL	theAction = [menuItem action];
	if( theAction == @selector( selectTrainIDStateByMenu: ))
	{
		[menuItem setTitle:(![trainIDPUB indexOfSelectedItem]) ? NSLocalizedString( @"Hide TrainID",@"Hide TrainID" ) : NSLocalizedString( @"Show TrainID", @"Show TrainID" )];
		return YES;
	}
	else if( theAction == @selector( selectDirectionByMenu: ))
	{
		return [menuItem tag] != [directionPUB indexOfSelectedItem];
	}
	else if( theAction == @selector( selectHeightByMenu: ))
	{
		if([menuItem tag] == DGXTagHeightAuto && _stationGrid )
			return NO;
		else
			return [menuItem tag] != [heightPUB selectedTag];
	}
	else
		return [super validateMenuItem:menuItem];
}
#pragma mark -
#pragma mark methods for NSWindowDelegate Protocol
// =============================================================================
//	windowDidBecomeMain:
// =============================================================================
- (void)windowDidBecomeMain:(NSNotification *)notification
{
	[[NSNotificationCenter defaultCenter] postNotificationName:DGXMainWindowDidChangeNotification 
		object:self];
}
// =============================================================================
//	windowWillClose:
// =============================================================================
- (void)windowWillClose:(NSNotification *)notification
{
	[[NSNotificationCenter defaultCenter] postNotificationName:DGXDocumentWindowWillCloseNotification 
		object:self];
}

@end
