//
//  DiagramView.h
//  CocoDia
//
//  Created by KITAHARA Motohiko on 06-05-13.
//  Copyright 2006-10 horazaka.net. Distributed under GPL.
//

#import <Cocoa/Cocoa.h>
@class Runtime;

typedef enum	_DGXHeightState
{
	DGXHeightAuto,
	DGXHeightMinimum,
	DGXHeightMedium,
	DGXHeightMaximum,
}	DGXHeightState;

@interface DiagramView : NSView
{
	NSInteger	_direction;	// 0..down/up, 1..down, 2..up
	BOOL	_trainIDOn;	// YES...TrainID on, NO...TrainID off
	DGXHeightState	_height;	// 0..Auto, 1..Min, 2..Med, 3..Max

	Runtime*	_runtime;
	CGFloat*	_yPositions;

	NSImage*	_stationImage;
	NSImage*	_timetableImage;
	NSImage*	_downTrainLineImage;
	NSImage*	_upTrainLineImage;
	NSImage*	_downTrainIDImage;
	NSImage*	_upTrainIDImage;
}
@property (nonatomic, retain) Runtime*	runtime;
@property (nonatomic, assign) DGXHeightState	height;

- (void)drawDiagramDown:(NSArray*)downTrainLines andUp:(NSArray*)upTrainLines withRuntime:(Runtime*)runtime height:(DGXHeightState)height;
- (void)setDirection:(NSInteger)aDirection;
- (void)setTrainIDOn:(BOOL)yn;

@end
