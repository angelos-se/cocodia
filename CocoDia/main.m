//
//  main.m
//  CocoDia
//
//  Created by 北原 基彦 on 10-10-03.
//  Copyright horazaka.net 2010 . All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **) argv);
}
