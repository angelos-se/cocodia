//
//  CocoDia_AppDelegate.h
//  CocoDia
//
//  Created by KITAHARA Motohiko on 07-11-25.
//  Copyright 2007-10 horazaka.net. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class FileInfoWC;
@class PreferenceWC;


@interface CocoDia_AppDelegate : NSObject
{
	FileInfoWC*	_fileInfoWC;
	PreferenceWC*	_preferenceWC;
}
- (IBAction)showInfoPanel:(id)sender;
- (IBAction)showPreferencePanel:(id)sender;

@end
