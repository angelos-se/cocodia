//
//  DGXKeys.m
//  CoreDia
//
//  Created by KITAHARA Motohiko on 07-12-01.
//  Copyright 2007 horazaka.net. All rights reserved.
//

#import "DGXKeys.h"

// =============================================================================
//	index(attributes)
// =============================================================================
const NSString*	DGXIndex = @"index";
// =============================================================================
//	info(attributes, keys)
// =============================================================================
const NSString*	DGXInfo = @"info";	// key
const NSString*	DGXLine = @"line";
const NSString*	DGXDate = @"date";
const NSString*	DGXCreator = @"creator";
const NSString*	DGXComment = @"comment";
// =============================================================================
//	version(keys)
// =============================================================================
const NSString*	DGXVersion = @"version";	// key
// =============================================================================
//	station(attributes,keys)
// =============================================================================
const NSString*	DGXStation = @"station";	// key, also relationship in Table Entity
const NSString*	DGXName = @"name";
const NSString*	DGXState = @"state";
const NSString*	DGXDistance = @"distance";
// =============================================================================
//	classification(relationship,attributes,keys)
// =============================================================================
const NSString*	DGXClassification = @"classification";	// key, also relationship in Train Entity
const NSString*	DGXLongName = @"longName";
const NSString*	DGXShortName = @"shortName";
const NSString*	DGXLineStyle = @"lineStyle";
const NSString*	DGXLineColor = @"lineColor";
const NSString*	DGXLineThickness = @"lineThickness";
const NSString*	DGXLineImage = @"lineImage";
const NSString*	DGXTrains = @"trains";	// relationship
// =============================================================================
//	train(relationship,attributes,keys)
// =============================================================================
const NSString*	DGXTrain = @"train";	// key, also relationship in Table Entity
const NSString*	DGXTrainID = @"trainID";
const NSString*	DGXIsShown = @"isShown";	// new for 51
const NSString*	DGXVisible = @"visible";	// new for 60
const NSString*	DGXDirection = @"direction";
const NSString*	DGXNickname = @"nickname";
const NSString*	DGXNumber = @"number";
const NSString*	DGXNote = @"note";
const NSString*	DGXTables = @"tables";	// relationship, also in Station Entity

const NSString*	DGXDownTrain = @"downTrain";	// following 4 are keys
const NSString*	DGXUpTrain = @"upTrain";
const NSString*	DGXClassificationID = @"classificationID";
const NSString*	DGXTimetable = @"timetable";
// =============================================================================
//	table(attributes,keys)
// =============================================================================
const NSString*	DGXArrTime = @"arrTime";	// ver < 60
const NSString*	DGXDepTime = @"depTime";	// ver < 60
const NSString*	DGXArrival = @"arrival";	// new for 60
const NSString*	DGXDeparture = @"departure";	// new for 60
//const NSString*	DGXTime = @"time";

