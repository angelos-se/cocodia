//
//  DGXUtilities.h
//  CoreDia
//
//  Created by KITAHARA Motohiko on 07-12-02.
//  Copyright 2007 horazaka.net. All rights reserved.
//

#import <Cocoa/Cocoa.h>


// =============================================================================
//	notification
// =============================================================================
extern NSString*	DGXTrainACDidInsertObjectNotification;
extern NSString*	DGXStationACDidInsertObjectNotification;

// =============================================================================
//	dash line style for Classification.m, DiagramView.m
// =============================================================================
extern const CGFloat	dash1[];	// dash line
extern const CGFloat	dash2[];	// dotted line
extern const CGFloat	dash3[];	// dot-and-dashed line

// =============================================================================
//	function and macro for time calculation
// =============================================================================
NSInteger	submin( NSInteger arg1, NSInteger arg2 );
NSInteger	addmin( NSInteger arg1, NSInteger arg2 );

extern const NSInteger	_hhmm2min[];
#define	hhmm2min( hhmm )	_hhmm2min[( hhmm )]
//#define	hhmm2min( hhmm )	(( hhmm / 100 ) * 60 ) + (( hhmm ) % 100 )

extern const NSInteger	_min2hhmm[];
#define min2hhmm( min )	_min2hhmm[( min )]
//#define min2hhmm( min )	(( min / 60 ) * 100 ) + (( min ) % 60 )

#define	time2x( time )	(( time ) * 4 )

#define	ishhmm( time )	((( time ) < 2401 ) && ( -1 != _hhmm2min[( time )]))
