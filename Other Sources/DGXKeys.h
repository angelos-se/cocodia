//
//  DGXKeys.h
//  CoreDia
//
//  Created by KITAHARA Motohiko on 07-12-01.
//  Copyright 2007 horazaka.net. All rights reserved.
//

#import <Cocoa/Cocoa.h>

// =============================================================================
//	index(attributes)
// =============================================================================
extern const NSString*	DGXIndex;
// =============================================================================
//	info(attributes, keys)
// =============================================================================
extern const NSString*	DGXInfo;	// key
extern const NSString*	DGXLine;
extern const NSString*	DGXDate;
extern const NSString*	DGXCreator;
extern const NSString*	DGXComment;
// =============================================================================
//	version(keys)
// =============================================================================
extern const NSString*	DGXVersion;	// key
// =============================================================================
//	station(attributes,keys)
// =============================================================================
extern const NSString*	DGXStation;	// key, also relationship in Table Entity
extern const NSString*	DGXName;
extern const NSString*	DGXState;
extern const NSString*	DGXDistance;
// =============================================================================
//	classification(relationship,attributes,keys)
// =============================================================================
extern const NSString*	DGXClassification;	// key, also relationship in Train Entity
extern const NSString*	DGXLongName;
extern const NSString*	DGXShortName;
extern const NSString*	DGXLineStyle;
extern const NSString*	DGXLineColor;
extern const NSString*	DGXLineThickness;
extern const NSString*	DGXLineImage;
extern const NSString*	DGXTrains;	// relationship
// =============================================================================
//	train(relationships,attributes,keys)
// =============================================================================
extern const NSString*	DGXTrain;	//key,  also relationship in Table Entity
extern const NSString*	DGXTrainID;
extern const NSString*	DGXIsShown;	// new for 51
extern const NSString*	DGXVisible;	// ner for 60
extern const NSString*	DGXDirection;
extern const NSString*	DGXNickname;
extern const NSString*	DGXNumber;
extern const NSString*	DGXNote;
extern const NSString*	DGXTables;	// relationship, also in Station Entity

extern const NSString*	DGXDownTrain;	// following 4 are keys
extern const NSString*	DGXUpTrain;
extern const NSString*	DGXClassificationID;
extern const NSString*	DGXTimetable;
// =============================================================================
//	table(attributes,keys)
// =============================================================================
extern const NSString*	DGXArrTime;	// ver < 60
extern const NSString*	DGXDepTime;	// ver < 60
extern const NSString*	DGXArrival;	// new for 60
extern const NSString*	DGXDeparture;	// new for 60
//extern const NSString*	DGXTime;	// ver < 70

