//
//  DGXVerticalText.h
//  CocoATSUI
//
//  Created by KITAHARA Motohiko on 07-04-18.
//  Copyright 2007-09 horazaka.net. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface DGXVerticalText : NSObject
{
}
+ (OSStatus)draw:(NSString*)aString inRect:(NSRect)aRect;

@end
