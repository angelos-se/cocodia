//
//  MyDocument.h
//  CocoTable
//
//  Created by 北原 基彦 on 09-08-09.
//  Copyright 2009-10 Motohiko KITAHARA . All rights reserved.
//

#import <Cocoa/Cocoa.h>

enum _DGXTimetableTitleColumn {
	DGXTimetableTitleColumnDistance = 0,
	DGXTimetableTitleColumnStation,
	DGXTimetableTitleColumnDepArr,
	DGXTimetableTitleColumnCount,
};

@interface MyDocument : NSPersistentDocument
{
    IBOutlet NSPopUpButton*	directionPUB;
	IBOutlet NSTableView*	tableView;

	NSInteger	_stationCount;
	NSMutableIndexSet*	_majorStationIndex;
	NSMutableArray*	_downTimetable;
	NSMutableArray*	_upTimetable;
}
- (IBAction)selectDirectionByMenu:(id)sender;
- (IBAction)selectDirection:(id)sender;

@end
