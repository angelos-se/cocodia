//
//  DGXTimeTableColumn.m
//  CocoDia
//
//  Created by KITAHARA Motohiko on 07-04-22.
//  Copyright 2007 horazaka.net. All rights reserved.
//

#import "DGXTimeTableColumn.h"


@implementation DGXTimeTableColumn
- (id)dataCellForRow:(NSInteger)row
{
	NSInteger		theRow = [[ self tableView ] numberOfRows ];
	NSCell*	theCell;
	if( row == 1 || row == theRow - 1 )
	{
		theCell = [[ NSImageCell alloc ] init ];
	}
	else
	{
		theCell = [[ NSTextFieldCell alloc ] init ];
	}
	return [ theCell autorelease ];
}
@end
