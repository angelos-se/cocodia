//
//  DGXVerticalText.m
//  CocoTable
//
//  Created by KITAHARA Motohiko on 07-04-18.
//  Copyright 2007-09 horazaka.net. All rights reserved.
//

#import "DGXVerticalText.h"


@implementation DGXVerticalText
// =============================================================================
//	halfToFull:
//		change half width chars( 0x21 ~ 0x7E ) to full width chars( 0xFF01 ~ 0xFF5E )
//		so that they are readable in vertical text
// =============================================================================
+ (NSString*)halfToFull:(NSString*)aString
{
	NSMutableString*	theResult = [ NSMutableString string ];
	NSInteger	i, j = [ aString length ];
	for( i = 0; i < j; i++ )
	{
		UniChar	theChar = [ aString characterAtIndex:i ];
		if( isgraph( theChar ))
			theChar += 0xFEE0;
		[ theResult appendFormat:@"%C", theChar ];
	}
	return [ NSString stringWithString:theResult ]; 
}
// =============================================================================
//	draw:inRect:
// =============================================================================
+ (OSStatus)draw:(NSString*)aString inRect:(NSRect)aRect
{
	if([ aString length ] == 0 )
		return noErr;

	OSStatus	theStatus = noErr;
	CTFontRef	theFont = NULL;

	// init a graphics context and set the text matrix to a known value
	CGContextRef	theContext = (CGContextRef)[[ NSGraphicsContext currentContext ] graphicsPort ];
	CGContextSetTextMatrix( theContext, CGAffineTransformIdentity );

	// init attributed string
	// first try "Hiragino" font, suitable for vertical text 
	theFont = CTFontCreateWithName((CFStringRef)@"HiraKakuPro-W4", 13.0, NULL);
	if( NULL == theFont )
	{
		// get substitute font
//		theFont = CTFontCreateUIFontForLanguage( kCTFontSystemFontType, 0.0, (CFStringRef)NULL );
		theFont = CTFontCreateForString( 
			CTFontCreateUIFontForLanguage( kCTFontSystemFontType, 0.0, (CFStringRef)NULL ), 
			(CFStringRef)aString, 
			CFRangeMake(0, [aString length]));
		if( NULL == theFont )
			goto EXIT;
	}

	NSDictionary*	theStringAttributes = [ NSDictionary dictionaryWithObjectsAndKeys: 
		(NSFont*)theFont, (NSString*)kCTFontAttributeName, 
//		theParagraphStyle, NSParagraphStyleAttributeName, 
		[ NSNumber numberWithBool:YES ], (NSString*)kCTVerticalFormsAttributeName, nil ];
	NSAttributedString*	theString = [[[ NSAttributedString alloc ] initWithString:[ self halfToFull:aString ] attributes:theStringAttributes ] autorelease ];

#if TARGET_VERSION == 1050
	// draw glyph one by one at ( x, y )
	// this does not work well in SnoL, each glyph does not rotate properly
	CGFloat	theLeading = CTFontGetSize( theFont ) + (CGFloat)2.0;
	NSInteger	theCharsInLine = NSMaxY( aRect ) / theLeading;
	NSInteger	theLineNumber = ([ aString length ] - 1 ) / theCharsInLine + 1;
	CGFloat	x, y, theLineWidth = NSMaxX( aRect ) / theLineNumber;
	NSInteger	i, j, k, n = [ theString length ];	// i:for line, j:for char in line, k:total char
	CFRange theGlyphRange = CFRangeMake( 0, 1 );
	for( i = 0, k = 0, x = NSMaxX( aRect ) - theLineWidth / 2; i < theLineNumber; i++, x-= theLineWidth )
	{
		for( j = 0, y = NSMaxY( aRect ) - (CGFloat)3.0; j < theCharsInLine && k < n; j++, y -= theLeading, k++ )
		{
//			NSLog(@"k=%ld,x=%f,y=%f",k,x,y);
			NSAttributedString*	theChar = [ theString attributedSubstringFromRange:NSMakeRange( k, 1 )];
//			NSLog(@"theChar=%@",[theChar string]);
			CTLineRef	theLine = CTLineCreateWithAttributedString( (CFAttributedStringRef)theChar);
			CFArrayRef	theRuns = CTLineGetGlyphRuns( theLine );
			CTRunRef	theRun = (CTRunRef)CFArrayGetValueAtIndex( theRuns, 0 );
			CGContextSetTextPosition( theContext, x, y ); 
			CTRunDraw( theRun, theContext, theGlyphRange );
			CFRelease( theLine );
		}
	}

#else	// TARGET_VERSION == 1060
	// first draw horizontally with vertical gryph, then rotete by -90 degree
	CTFramesetterRef	theFramesetter = CTFramesetterCreateWithAttributedString((CFAttributedStringRef)theString);
	NSDictionary*	theFrameAttributes = [NSDictionary dictionaryWithObjectsAndKeys: 
		[NSNumber numberWithInteger:kCTFrameProgressionTopToBottom], (NSString*)kCTFrameProgressionAttributeName, nil];
//		[NSNumber numberWithInteger:kCTFrameProgressionRightToLeft], (NSString*)kCTFrameProgressionAttributeName, nil];
	CGMutablePathRef	thePath = CGPathCreateMutable();
//	CGPathAddRect(thePath, NULL, aRect);
	CGRect	theRect = CGRectMake(aRect.origin.x, aRect.origin.y, aRect.size.height, aRect.size.width);
	CGPathAddRect(thePath, NULL, theRect);
	CFRange	theRange = CFRangeMake(0, 0);
	CTFrameRef	theFrame = CTFramesetterCreateFrame(theFramesetter, theRange, thePath, (CFDictionaryRef)theFrameAttributes);

	[[NSGraphicsContext currentContext] saveGraphicsState];
	NSAffineTransform*	theTransform = [NSAffineTransform transform];
	[theTransform translateXBy:0 yBy:aRect.size.height];
	[theTransform rotateByDegrees:-90.0f];
	[theTransform concat];

//	CTFrameDraw(theFrame, theContext);

	// grab lines and draw line by line
	NSArray*	theLines = (NSArray*)CTFrameGetLines( theFrame );
	NSInteger	i, j = [ theLines count ];
//	NSUInteger	i;
//	CGFloat	theLeading = CTFontGetSize(theFont) + 3.0;
//	NSInteger	theCharsInLine = NSMaxY( aRect ) / theLeading;
//	NSUInteger	theLength = [theString length];
//	NSInteger	j = (theLength - 1) / theCharsInLine + 1;
	CGFloat	theLineWidth = aRect.size.width / j;
	CGFloat	x = 0, y;
	for( i = 0, y = theLineWidth * ( j - 0.5 ); i < j; i++, y -= theLineWidth )
	{
		CGContextSetTextPosition( theContext, x, y );
//		NSLog(@"x=%f,y=%f",x,y);
//		NSRange	theRange = ( i == j - 1 ) ? NSMakeRange( i * theCharsInLine, theLength - i * theCharsInLine) : NSMakeRange(i * theCharsInLine, theCharsInLine);
//		CTLineRef	theLine = CTLineCreateWithAttributedString((CFAttributedStringRef)[theString attributedSubstringFromRange:theRange]);
//		CFArrayRef	theRuns = CTLineGetGlyphRuns(theLine);
//		CTRunRef	theRun = (CTRunRef)CFArrayGetValueAtIndex(theRuns, 0);
//		CTRunDraw(theRun, theContext, CFRangeMake(theRange.location, theRange.length));
		CTLineDraw( (CTLineRef)[ theLines objectAtIndex:i ], theContext );
	}
	[[NSGraphicsContext currentContext] restoreGraphicsState];

	CFRelease(theFrame);
	CFRelease(thePath);
#endif

EXIT:
	if( NULL != theFont )
		CFRelease( theFont );
	return theStatus;
}

@end
