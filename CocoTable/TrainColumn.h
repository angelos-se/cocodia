//
//  TrainColumn.h
//  CocoDia
//
//  Created by KITAHARA Motohiko on 06-06-10.
//  Copyright 2006 KITAHARA Motohiko. Distributed under GPL.
//

#import <Cocoa/Cocoa.h>


extern const NSString*	DGXIdentifier;
extern const NSString*	DGXColumnHeader;
extern const NSString*	DGXColumnData;
/*
// TrainColumn class is substituted with NSDictionary class
// and acessesor methods are as such as [ theTrainColumn objectForKey : DGX... ];
@interface TrainColumn : NSObject
{
	NSString*	identifier;	// 0, 1, 2,...
	NSString*	columnHeader;	// 4501D, 525M,...
	NSArray*	columnData;
}

- (id)initWithIdentifier:(NSInteger)anIdentifier
		columnHeader:(NSString*)aHeader
		columnData:(NSArray*)aData;

- (NSString*)identifier;
- (NSString*)columnHeader;
- (NSArray*)columnData;

@end
*/
