//
//  CocoTable_AppDelegate.h
//  CocoTable
//
//  Created by KITAHARA Motohiko on 07-11-10.
//  Copyright 2007 horazaka.net. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class FileInfoWC;


@interface CocoTable_AppDelegate : NSObject
{
	FileInfoWC*	_fileInfoWC;
}
- (IBAction)showInfoPanel:(id)sender;

@end
