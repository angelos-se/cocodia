//
//  MyDocument.m
//  CocoTable
//
//  Created by 北原 基彦 on 09-08-09.
//  Copyright 2009-10 Motohiko KITAHARA . All rights reserved.
//

#import "MyDocument.h"
#import "MyDocument_Accessor.h"

#import "DGXTimeTableColumn.h"
#import "DGXVerticalText.h"
#import "TrainColumn.h"

#import "DGXKeys.h"
#import "DGXUtilities.h"
#import "DGXUpdater.h"
#import "FileInfoWC.h"
#import	"OuDiaConverter.h"
#import	"WinDIAConverter.h"


static CGFloat	DGXTimetableDataWidth = 40.0;
static CGFloat	DGXTimetableStationWidth = 150.0;
static CGFloat	DGXTimetableDistanceWidth = 50.0;
static CGFloat	DGXTimetableNameHeight = 136.0;
static CGFloat	DGXTimetableNoteHeight = 272.0;


@interface MyDocument( privateMethods )
- (NSMutableIndexSet*)createIndexSet;
- (NSAttributedString*)createHeader:(NSString*)aHeader;
- (NSInteger)createTitleColumns:(NSInteger)aDirection;
- (NSImage*)createTrainnameFromNickname:(NSString*)aName andNumber:(NSString*)aNumber;
- (NSImage*)createNote:(NSString*)aNote;
- (NSAttributedString*)createTime:(NSString*)anArrDep;
- (void)createTimetableColumns:(NSInteger)aDirection;
- (void)writeTimetableByDirection:(NSInteger)aDirection;
@end

@implementation MyDocument
// =============================================================================
//	dealloc
// =============================================================================
- (void)dealloc
{
	[_majorStationIndex release], _majorStationIndex = nil;
	[_downTimetable release], _downTimetable = nil;
	[_upTimetable release], _upTimetable = nil;

	[super dealloc];
}
// =============================================================================
//	willPresentError:
// =============================================================================
- (NSError*)willPresentError:(NSError *)error
{
	if ([error code] == NSValidationMultipleErrorsError)
	{
		NSArray*	theErrors = [[error userInfo] objectForKey:NSDetailedErrorsKey];
		NSLog(@"errors=%@",theErrors);
	}
	else
		NSLog(@"error=%@",error);
	return [super willPresentError:error];
}
#pragma mark -
// =============================================================================
//	windowNibName
// =============================================================================
- (NSString *)windowNibName 
{
    return @"MyDocument";
}
// =============================================================================
//	windowControllerDidLoadNib:
// =============================================================================
- (void)windowControllerDidLoadNib:(NSWindowController *)windowController 
{
    [super windowControllerDidLoadNib:windowController];

	// initialize _majorStationIndex
	_majorStationIndex = [[self createIndexSet] retain];
	_downTimetable = [[NSMutableArray array] retain];
	_upTimetable = [[NSMutableArray array] retain];
	// create down/up titles
	_stationCount = [self createTitleColumns:0];	// create down, and initialize _stationCount
	[self createTitleColumns:1];	// create up
	// create down/up timetable
	[self createTimetableColumns:0];	// create down
	[self createTimetableColumns:1];	// create up
	// at last show timetable
	[self writeTimetableByDirection:[directionPUB indexOfSelectedItem]];

	// reset undo
	NSManagedObjectContext*	theContext = [self managedObjectContext];
	[theContext processPendingChanges];
	[[theContext undoManager] removeAllActions];
	[self updateChangeCount:NSChangeCleared];
}
// =============================================================================
//	readFromURL:ofType:error:
// =============================================================================
- (BOOL)readFromURL:(NSURL *)absoluteURL ofType:(NSString *)typeName error:(NSError **)outError
{
	NSDictionary*	theProperties = nil;
	id	theConverter = nil;
	if ([typeName isEqualToString:@"SQLite"])
	{
		[self setFileType:typeName];
		return [super readFromURL:absoluteURL ofType:typeName error:outError];
	}
	else if ([typeName isEqualToString:@"CocoDiaFile"])
	{
		theConverter = [[[DGXUpdater alloc] init] autorelease];
		theProperties = [theConverter convert:absoluteURL];
	}
	else if ([typeName isEqualToString:@"OuDiaFile"])
	{
		theConverter = [[[OuDiaConverter alloc] init] autorelease];
		theProperties = [theConverter convert:absoluteURL];
	}
	else	// if ([typeName isEqualToString:@"WinDIAFile"])
	{
		theConverter = [[[WinDIAConverter alloc] init] autorelease];
		theProperties = [theConverter convert:absoluteURL];
	}

	// check result
	if( !theProperties )
		return NO;

	// if file's version > app's version -> error
	if([[theProperties valueForKey:(NSString*)DGXVersion] integerValue] > [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"DGXDocumentVersion"] integerValue])
		return NO;

	// create new store in temporary directory
	NSString*	theFilename = [[[absoluteURL lastPathComponent] stringByDeletingPathExtension] stringByAppendingPathExtension:@"sqlite"];
	NSString*	thePath = [NSTemporaryDirectory() stringByAppendingPathComponent:theFilename];
	NSURL*	theURL = [NSURL fileURLWithPath:thePath];
	NSFileManager*	theFM = [NSFileManager defaultManager];
	if ([theFM fileExistsAtPath:thePath])
	{
		BOOL	theResult = [theFM removeItemAtURL:theURL error:NULL];
		if (!theResult)
			return NO;
	}

//	NSLog(@"theURL=%@",theURL);
	NSManagedObjectContext*	theMOC = [self managedObjectContext];
	NSPersistentStoreCoordinator*	thePSC = [theMOC persistentStoreCoordinator];
	NSError*	theError = nil;
	[thePSC addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:theURL options:nil error:&theError];
//	NSPersistentStore*	theStore = [thePSC addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:theURL options:nil error:&theError];
//	NSLog(@"theStore=%@,error=%@",theStore,theError);
	if (theError)
		return NO;

	// set entities
	[self setInfo:[theProperties objectForKey:DGXInfo]];
	[self setClassification:[theProperties objectForKey:DGXClassification]];
	[self setStation:[theProperties objectForKey:DGXStation]];
	[self setTrainWithDown:[theProperties objectForKey:DGXDownTrain]
		andUp:[theProperties objectForKey:DGXUpTrain]];	// must be after setClassification: and setStation:
	return YES;
}
#pragma mark -
// -----------------------------------------------------------------------------
//	createIndexSet
// -----------------------------------------------------------------------------
- (NSMutableIndexSet*)createIndexSet
{
	NSManagedObjectContext*	theMOC = [self managedObjectContext];
	NSFetchRequest*	theRequest = [[[NSFetchRequest alloc] init] autorelease];
	NSEntityDescription*	theEntity = [NSEntityDescription entityForName:@"Station" inManagedObjectContext:theMOC];
	[theRequest setEntity:theEntity];
	NSSortDescriptor*	theDescriptor = [[[NSSortDescriptor alloc] initWithKey:(NSString*)DGXIndex ascending:YES] autorelease];
	[theRequest setSortDescriptors:[NSArray arrayWithObject:theDescriptor]];
	NSArray*	theStations = [theMOC executeFetchRequest:theRequest error:NULL];

	NSMutableIndexSet*	theMajorStationIndex = [NSMutableIndexSet indexSet];
	NSInteger	i, j = [theStations count] - 1;
	for( i = 1; i < j; i++ )
	{
		if( 2 == [[[theStations objectAtIndex:i] valueForKey:(NSString*)DGXState] integerValue])
			[theMajorStationIndex addIndex:i];
	}
	return theMajorStationIndex;
}
// -----------------------------------------------------------------------------
//	createHeader:
// -----------------------------------------------------------------------------
- (NSAttributedString*)createHeader:(NSString*)aHeader
{
	static NSDictionary* theAttributes = nil;
	if( nil == theAttributes )
	{
		NSMutableParagraphStyle *theStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
		[theStyle setLineBreakMode:NSLineBreakByTruncatingTail];
		[theStyle setTighteningFactorForTruncation:0.5];
		[theStyle setAlignment:NSCenterTextAlignment];
		theAttributes = [[NSDictionary alloc] initWithObjectsAndKeys: 
			theStyle, NSParagraphStyleAttributeName, nil];
		[theStyle release];
	}
	return [[[NSAttributedString alloc] initWithString:aHeader attributes:theAttributes] autorelease];
}
#pragma mark -
// -----------------------------------------------------------------------------
//	createTitleColumns
//		aDirection	0...Down, 1...Up
//		return number of rows
// -----------------------------------------------------------------------------
- (NSInteger)createTitleColumns:(NSInteger)aDirection
{
	NSManagedObjectContext*	theMOC = [self managedObjectContext];
	NSFetchRequest*	theRequest = [[[NSFetchRequest alloc] init] autorelease];
	NSEntityDescription*	theEntity = [NSEntityDescription entityForName:@"Station" inManagedObjectContext:theMOC];
	[theRequest setEntity:theEntity];
	NSSortDescriptor*	theDescriptor = [[[NSSortDescriptor alloc] initWithKey:(NSString*)DGXIndex ascending:YES] autorelease];
	[theRequest setSortDescriptors:[NSArray arrayWithObject:theDescriptor]];
	NSArray*	theStations = [theMOC executeFetchRequest:theRequest error:NULL];

	NSInteger	i, j, k = [theStations count];
	// 2 zero string for 1st and 2nd row
	NSMutableArray*	theDistanceData = [NSMutableArray arrayWithObjects:@"", @"", nil];
	NSMutableArray*	theStationsData = [NSMutableArray arrayWithObjects:@"", @"", nil];
	NSMutableArray*	theArrDepData = [NSMutableArray arrayWithObjects:@"", @"", nil];
	for( i = 0; i < k; i++ )
	{
		j = ( 0 == aDirection ) ? i:k - i - 1;
		id	theStation = [theStations objectAtIndex:j];
#ifdef __LP64__
		[theDistanceData addObject:[NSString stringWithFormat:@"%4.1f", [[theStation valueForKey:(NSString*)DGXDistance] doubleValue]]];
#else
		[theDistanceData addObject:[NSString stringWithFormat:@"%4.1f", [[theStation valueForKey:(NSString*)DGXDistance] floatValue]]];
#endif
		[theStationsData addObject:[theStation valueForKey:(NSString*)DGXName]];
		if([_majorStationIndex containsIndex:j])
		{
			[theDistanceData addObject:@"-"];
			[theStationsData addObject:@"-"];
			[theArrDepData addObject:NSLocalizedString( @"arr", nil )];
			[theArrDepData addObject:NSLocalizedString( @"dep", nil )];
		}
		else
		{
			[theArrDepData addObject:NSLocalizedString(( i == k - 1 ) ? @"arr":@"dep", nil )];
		}
	}
	// one more zero string for the last row
	[theDistanceData addObject:@""];
	[theStationsData addObject:@""];
	[theArrDepData addObject:@""];

	NSMutableArray*	theTimetable = ( 0 == aDirection ) ? _downTimetable:_upTimetable;
	[theTimetable addObject:[NSDictionary dictionaryWithObjectsAndKeys: 
		[NSNumber numberWithInteger:[theTimetable count]], DGXIdentifier, 
		[self createHeader:NSLocalizedString( @"distance", nil )], DGXColumnHeader, 
		theDistanceData, DGXColumnData, nil]];
	[theTimetable addObject:[NSDictionary dictionaryWithObjectsAndKeys: 
		[NSNumber numberWithInteger:[theTimetable count]], DGXIdentifier, 
		[self createHeader:NSLocalizedString( @"station", nil )], DGXColumnHeader, 
		theStationsData, DGXColumnData, nil]];
	[theTimetable addObject:[NSDictionary dictionaryWithObjectsAndKeys: 
		[NSNumber numberWithInteger:[theTimetable count]], DGXIdentifier, 
		[self createHeader:NSLocalizedString( @"arrdep", nil )], DGXColumnHeader, 
		theArrDepData, DGXColumnData, nil]];
	return [theStationsData count];
}
#pragma mark -
// -----------------------------------------------------------------------------
//	createTrainnameFromNickname:andNumber:
// -----------------------------------------------------------------------------
- (NSImage*)createTrainnameFromNickname:(NSString*)aName andNumber:(NSString*)aNumber
{
	NSImage*	theImage = [[[NSImage alloc] initWithSize:NSMakeSize( DGXTimetableDataWidth, DGXTimetableNameHeight )] autorelease];
	[theImage lockFocus];
	if( [aName length] && [aNumber length])
		[DGXVerticalText draw:[NSString stringWithFormat:NSLocalizedString( @"format", nil ), aName, aNumber] 
			inRect:NSMakeRect( 0.0, 0.0, [theImage size].width, [theImage size].height )];
	else if( ![aNumber length])
		[DGXVerticalText draw:aName 
			inRect:NSMakeRect( 0.0, 0.0, [theImage size].width, [theImage size].height )];
	else
		;	// do nothing
	[theImage unlockFocus];
	return theImage;
}
// -----------------------------------------------------------------------------
//	createNote:
// -----------------------------------------------------------------------------
- (NSImage*)createNote:(NSString*)aNote
{
	NSImage*	theImage = [[[NSImage alloc] initWithSize:NSMakeSize( DGXTimetableDataWidth, DGXTimetableNoteHeight )] autorelease];
	[theImage lockFocus];
	if([aNote length])
		[DGXVerticalText draw:aNote 
			inRect:NSMakeRect( 0.0, 0.0, [theImage size].width, [theImage size].height )];
	else
		;	// do nothing
	[theImage unlockFocus];
	return theImage;
}
// -----------------------------------------------------------------------------
//	createTime:
// -----------------------------------------------------------------------------
- (NSAttributedString*)createTime:(NSString*)anArrDep
{
	NSString*	theTime;
	static NSMutableDictionary* theAttributes = nil;
	if( nil == theAttributes )
	{
		NSMutableParagraphStyle *theStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
		[theStyle setLineBreakMode:NSLineBreakByClipping];
		[theStyle setAlignment:NSRightTextAlignment];
		theAttributes = [[NSMutableDictionary alloc] initWithObjectsAndKeys: 
			theStyle, NSParagraphStyleAttributeName, nil];
		[theStyle release];
	}
	NSFont*	theFont = [[NSFontManager sharedFontManager] convertFont:[NSFont userFontOfSize:[NSFont systemFontSize]] toHaveTrait:NSUnitalicFontMask];
//	NSFont*	theFont = theFont = [[NSFontManager sharedFontManager] convertFont:[NSFont controlContentFontOfSize:[NSFont systemFontSize]] toHaveTrait:NSUnitalicFontMask];
	if( ![anArrDep length] || [anArrDep hasPrefix:@"-"])
	{
		theTime = anArrDep;
	}
	else
	{
		NSInteger	i = [anArrDep integerValue];
		theTime = [NSString stringWithFormat:( 59 < i ) ? @"%d":@"%03d", i];
		if([anArrDep hasSuffix:@"?"])
			theFont = [[NSFontManager sharedFontManager] convertFont:theFont toHaveTrait:NSItalicFontMask];
	}
	[theAttributes setValue:theFont forKey:NSFontAttributeName];
	return [[[NSAttributedString alloc] initWithString:theTime attributes:theAttributes] autorelease];
}
// -----------------------------------------------------------------------------
//	createTimetableColumns:
//		aDirection	0...Down, 1...Up
// -----------------------------------------------------------------------------
- (void)createTimetableColumns:(NSInteger)aDirection
{
	NSManagedObjectContext*	theMOC = [self managedObjectContext];
	NSFetchRequest*	theRequest = [[[NSFetchRequest alloc] init] autorelease];
	NSEntityDescription*	theEntity = [NSEntityDescription entityForName:@"Train" inManagedObjectContext:theMOC];
	[theRequest setEntity:theEntity];
	NSSortDescriptor*	theDescriptor = [[[NSSortDescriptor alloc] initWithKey:(NSString*)DGXIndex ascending:YES] autorelease];
	[theRequest setSortDescriptors:[NSArray arrayWithObject:theDescriptor]];
	NSPredicate*	thePredicate = [NSPredicate predicateWithFormat:@"direction == %ld", (0 == aDirection) ? 1 : 0];
	[theRequest setPredicate:thePredicate];
	NSArray*	theTrains = [theMOC executeFetchRequest:theRequest error:NULL];

	NSMutableArray*	theTimetable = ( 0 == aDirection ) ? _downTimetable:_upTimetable;
	for( id theTrain in theTrains )
	{
		if( ![[theTrain valueForKey:(NSString*)DGXVisible] boolValue])	// if train must not be drawn -> skip
			continue;
		// classification
		NSMutableArray*	theColumnData = [NSMutableArray arrayWithObject:[theTrain valueForKeyPath:@"classification.shortName"]];

		// nickname and number
		NSString*	theNickname = [theTrain valueForKey:(NSString*)DGXNickname];
		NSNumber*	theNumber = [theTrain valueForKey:(NSString*)DGXNumber];
//		NSString*	theNumber = [[theTrain valueForKey:(NSString*)DGXNumber] stringValue];
		[theColumnData addObject:[self createTrainnameFromNickname:theNickname andNumber:(0 == [theNumber integerValue]) ? @"" : [theNumber stringValue]]];

		// timetable
		NSSortDescriptor*	theDescriptor = [[NSSortDescriptor alloc] initWithKey:@"station.index" ascending:YES];
		NSArray*	theTables = [[[theTrain valueForKey:(NSString*)DGXTables] allObjects] sortedArrayUsingDescriptors:[NSArray arrayWithObject:theDescriptor]];
		[theDescriptor release];
		NSInteger	i, j, k = [theTables count];
		for( i = 0; i < k; i++ )
		{
			j = (( 0 == aDirection ) ? i:k - i - 1 );
			NSString*	theArrival = [[theTables objectAtIndex:j] valueForKey:(NSString*)DGXArrival];
			NSString*	theDeparture = [[theTables objectAtIndex:j] valueForKey:(NSString*)DGXDeparture];
			if([_majorStationIndex containsIndex:j])
			{
				[theColumnData addObject:[self createTime:(!theArrival) ? @"" : theArrival]];
				[theColumnData addObject:[self createTime:(!theDeparture) ? @"" : theDeparture]];
			}
			else
			{
				if (!theDeparture && !theArrival)
					[theColumnData addObject:@""];
				else
					[theColumnData addObject:[self createTime:(!theDeparture) ? theArrival : theDeparture]];
			}
		}
		// note
		[theColumnData addObject:[self createNote:[theTrain valueForKey:(NSString*)DGXNote]]];

		[theTimetable addObject:[NSDictionary dictionaryWithObjectsAndKeys:
				[NSNumber numberWithInteger:[theTimetable count]], DGXIdentifier, 
				[self createHeader:[theTrain valueForKey:(NSString*)DGXTrainID]], DGXColumnHeader, 
				theColumnData, DGXColumnData, nil]];
	}
	return;
}
// -----------------------------------------------------------------------------
//	writeTimetableByDirection:
//		aDirection	0...Down, 1...Up
// -----------------------------------------------------------------------------
- (void)writeTimetableByDirection:(NSInteger)aDirection;
{
	// delete columns for train timetable
	while( 0 <  [tableView numberOfColumns])
		[tableView removeTableColumn:[[tableView tableColumns] objectAtIndex:0]];
	// then re-write columns for train timetable
	NSEnumerator*	e = [(( 0 == aDirection ) ? _downTimetable:_upTimetable ) objectEnumerator];
	id	theTrain;
	NSInteger	i;
	for( i = 0; theTrain = [e nextObject]; i++ )
	{
		NSString*	theIdentifier = [theTrain objectForKey:DGXIdentifier];
		NSAttributedString*	theHeader = [theTrain  objectForKey:DGXColumnHeader];
		NSTableColumn*	theColumn = [[[NSTableColumn alloc] initWithIdentifier:theIdentifier] autorelease];
		[[theColumn headerCell] setAttributedStringValue:theHeader];
		[theColumn setEditable:NO];
		CGFloat	theWidth;
		switch (i) {
			case DGXTimetableTitleColumnDistance:	// col#0 ... distance
				theWidth = DGXTimetableDistanceWidth;
				break;
			case DGXTimetableTitleColumnStation:	// col#1 ... station
				theWidth = DGXTimetableStationWidth;
				break;
			case DGXTimetableTitleColumnDepArr:	// col#2 ... arr/dep
			default:	// others ... timedata
				theWidth = DGXTimetableDataWidth;
				break;
		}
		[theColumn setWidth:theWidth];
		[tableView addTableColumn:theColumn];
	}
	[tableView setColumnAutoresizingStyle:NSTableViewNoColumnAutoresizing];
	[tableView reloadData];
}
#pragma mark -
// =============================================================================
//	selectDirectionByMenu:
// =============================================================================
- (IBAction)selectDirectionByMenu:(id)sender
{
	[directionPUB selectItemAtIndex:[sender tag]];	// change PUB compulsory, and [self selectDirection]
	[tableView setNeedsDisplay:YES];	// do not skip this
}
// =============================================================================
//	selectDirection:
// =============================================================================
- (IBAction)selectDirection:(id)sender
{
	[self writeTimetableByDirection:[sender indexOfSelectedItem]];
}
// =============================================================================
//	printOperationWithSettings:error:
//		- (IBAction)printDocument:(id)sender will invoke this method
// =============================================================================
- (NSPrintOperation *)printOperationWithSettings:(NSDictionary *)printSettings error:(NSError **)outError
{
	NSPrintInfo*	thePI = [self printInfo];
	[[thePI dictionary] setObject:[NSNumber numberWithBool:YES] forKey:NSPrintHeaderAndFooter];
	[thePI setOrientation:NSLandscapeOrientation];
	[thePI setHorizontalPagination:NSAutoPagination];
	[thePI setVerticalPagination:NSFitPagination];
	[thePI setVerticallyCentered:NO];

	return [NSPrintOperation printOperationWithView:tableView printInfo:thePI];
}
#pragma mark -
#pragma mark methods for NSTableViewDataSource Protocol
// -----------------------------------------------------------------------------
//	numberOfRowsInTableView:
// -----------------------------------------------------------------------------
- (NSInteger)numberOfRowsInTableView:(NSTableView*)aTableView
{
	return _stationCount;
}
// -----------------------------------------------------------------------------
//	tableView:objectValueForTableColumn:row:
// -----------------------------------------------------------------------------
- (id)tableView:(NSTableView*)aTableView
	objectValueForTableColumn:(NSTableColumn*)aTableColumn
	row:(NSInteger)rowIndex
{
	NSMutableArray*	theTrain = (( 0 == [directionPUB indexOfSelectedItem]) ? _downTimetable:_upTimetable );
	return [[[theTrain objectAtIndex:[[aTableColumn identifier] integerValue]] objectForKey:DGXColumnData] objectAtIndex:rowIndex];
}
#pragma mark -
#pragma mark methods for NSTableViewDelegate Protocol
// -----------------------------------------------------------------------------
//	tableView:heightOfRow:
// -----------------------------------------------------------------------------
- (CGFloat)tableView:(NSTableView*)aTableView heightOfRow:(NSInteger)rowIndex
{
	CGFloat	theHeight = [aTableView rowHeight];
	return (( rowIndex == 1 ) ? DGXTimetableNameHeight:(( rowIndex == _stationCount - 1 ) ? DGXTimetableNoteHeight:theHeight ));
}
// -----------------------------------------------------------------------------
//	tableView:willDisplayCell:forTableColumn:row:
// -----------------------------------------------------------------------------
- (void)tableView:(NSTableView*)aTableView
	willDisplayCell:(id)aCell 
	forTableColumn:(NSTableColumn*)aTableColumn
	row:(NSInteger)rowIndex
{
	[aCell setAlignment:(( rowIndex == 0 ) ? NSCenterTextAlignment:NSRightTextAlignment )];
}
// -----------------------------------------------------------------------------
//	tableView:dataCellForTableColumn:forTableColumn:row:
//		thanks to this delegate method which is avalable from 10.5
//		no need to subclass NSTableView in order to have each col have defferent cell
// -----------------------------------------------------------------------------
- (NSCell *)tableView:(NSTableView *)aTableView dataCellForTableColumn:(NSTableColumn *)aTableColumn row:(NSInteger)row
{
	NSInteger	theCol = [[aTableColumn identifier] integerValue];
	NSInteger	theRow = [aTableView numberOfRows];
	if( DGXTimetableTitleColumnDepArr < theCol && (row == 1 || row == theRow - 1 ))
	{
		return [[[NSImageCell alloc] init] autorelease];
	}
	return nil;	// use default cell ie NSTextFieldCell
}
#pragma mark -
#pragma mark methods for NSMenuValidation Protocol
// =============================================================================
//	validateMenuItem:
// =============================================================================
- (BOOL)validateMenuItem:(NSMenuItem *)menuItem
{
	SEL	theAction = [menuItem action];
	if( theAction == @selector( selectDirectionByMenu: ))
	{
		return [menuItem tag] != [directionPUB indexOfSelectedItem];
	}
	else
		return [super validateMenuItem:menuItem];
}
#pragma mark -
#pragma mark methods for NSWindowDelegate Protocol
// =============================================================================
//	windowDidBecomeMain:
// =============================================================================
- (void)windowDidBecomeMain:(NSNotification *)notification
{
	[[NSNotificationCenter defaultCenter] postNotificationName:DGXMainWindowDidChangeNotification 
		object:self];
}
// =============================================================================
//	windowWillClose:
//		note:[NSApp windows] returns a log of rubbish no title windows(why?), so filtered them out
// =============================================================================
- (void)windowWillClose:(NSNotification *)notification
{
	[[NSNotificationCenter defaultCenter] postNotificationName:DGXDocumentWindowWillCloseNotification 
		object:self];
}

@end
