//
//  TrainColumn.m
//  CocoDia
//
//  Created by KITAHARA Motohiko on 06-06-10.
//  Copyright 2006 KITAHARA Motohiko. Distributed under GPL.
//

#import "TrainColumn.h"


const NSString*	DGXIdentifier = @"identifier";
const NSString*	DGXColumnHeader = @"DGXColumnHeader";
const NSString*	DGXColumnData = @"DGXColumnData";
/*
@implementation TrainColumn
// =============================================================================
//	initWithIdentifier:columnHeader:columnData:
// =============================================================================
- (id)initWithIdentifier:(NSInteger)anIdentifier
		columnHeader:(NSString*)aHeader
		columnData:(NSArray*)aData
{
	self = [ super init ];
	if( self )
	{
		identifier = [[ NSString alloc ] initWithFormat : @"%04d", anIdentifier ];
		columnHeader = [[ NSString alloc ] initWithString : aHeader ];
		columnData = [[ NSArray alloc ] initWithArray : aData ];
	}
	return self;
}
// =============================================================================
//	dealloc
// =============================================================================
- (void)dealloc
{
	[ identifier release ], identifier = nil;
	[ columnHeader release ], columnHeader = nil;
	[ columnData release ], columnData = nil;

	[ super dealloc ];
}
// =============================================================================
//	identifier
// =============================================================================
- (NSString*)identifier
{
	return identifier;
}
// =============================================================================
//	columnHeader
// =============================================================================
- (NSString*)columnHeader
{
	return columnHeader;
}
// =============================================================================
//	columnData:
// =============================================================================
- (NSArray*)columnData
{
	return columnData;
}

@end
*/